#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string.h>
#include <ctype.h>
#include "error.h"

#define REQUESTMESSAGE 2048

/*
Error controller is a function made to controll the calling of
the function errors
*/
void error_controller(int argc, char** argv) {
    GameDetails* gameDetails = malloc(sizeof(GameDetails));
    if(argc != 5) {
        fprintf(stderr, "Incorrect number of args\n");
        exit(1);
    }
    keyfile_check(argv, gameDetails);
    port_check(argv, gameDetails);
}

//Need to check game name reconnect playername and recon_id !!!!!!!!!

/*
This is a function to simply check if the port is valid.
*/
void port_check(char** argv, GameDetails* gameDetails) {
    for (int i = 0; i < strlen(argv[2]); i++) {
        if(isdigit(argv[2][i]) == 0) {
            fprintf(stderr, "Connect failed\n");
            exit(3);
        }
    }
    gameDetails->portNum = atoi(argv[2]);
    if(gameDetails->portNum < 1024 || gameDetails->portNum > 65535) {
        fprintf(stderr, "Connect failed\n");
        exit(3);
    }
}

/*
This is a function to simply check the keyfile is valid
*/
void keyfile_check(char** argv, GameDetails* gameDetails) {
    FILE* fileFinder = fopen(argv[1], "r");
    if(fileFinder == NULL) {
        fprintf(stderr, "Bad get passchar\n");
        exit(2);
    }
    char getCharacter;
    char* requestString = (char*)malloc(REQUESTMESSAGE);
    int i = 0;
    while(getCharacter != '\n' && getCharacter != EOF) {
        getCharacter = fgetc(fileFinder);
        requestString[i] = getCharacter;
        i++;
    }
    if(sscanf(requestString, "%cplay", &gameDetails->serverKey) != 1) {
        fprintf(stderr, "Bad get passchar\n");
        exit(2);
    }
}