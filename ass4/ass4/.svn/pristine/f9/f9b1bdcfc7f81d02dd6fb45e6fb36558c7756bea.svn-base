
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <string.h>


#define MAXHOSTNAMELEN 128
#define CLIENTMESSAGE 2048
#define YES 4
#define NO 3

typedef enum {
    YESVALUE,
    NOVALUE
} HubMessage;


typedef enum {
	passChar
} ReadMessage;


typedef struct {
	char passChar;
	int* ports;
	int* fdServers;
	int totalPorts;
	char** playerNames;
	int totalPlayers;
} GameDetails;

void* client_thread(void* arg);
void error_controller(int argc, char** argv, GameDetails* gameDetails);
void keyfile_check(GameDetails* gameDetails, char** argv);
void port_check(GameDetails* gameDetails, char** argv);
GameDetails* game_details_init(int argc);


/*
This method opens the server to listening at a particular port
*/
int open_listen(int port) {
    int fd;
    struct sockaddr_in serverAddr;
    int optVal;
    // Create socket - IPv4 TCP socket
    fd = socket(AF_INET, SOCK_STREAM, 0);
    if(fd < 0) {
        perror("Failed listen");
        exit(6);
    }
    // Allow address (port number) to be reused immediately
    optVal = 1;
    if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &optVal, sizeof(int)) < 0) {
        perror("Failed listen");
        exit(6);
    }
    // Populate server address structure to indicate the local address(es)
    // that our server is going to listen on
    serverAddr.sin_family = AF_INET; // IPv4
    serverAddr.sin_port = htons(port); // port number - in network byte order
    serverAddr.sin_addr.s_addr = htonl(INADDR_ANY); // any IP address of this host

    // Bind our socket to this address (IP addresses + port number)
    if(bind(fd, (struct sockaddr*)&serverAddr, sizeof(struct sockaddr_in)) < 0) {
        perror("Failed listen");
        exit(6);
    }
    // Indicate our willingness to accept connections. Queue length is SOMAXCONN
    // (128 by default) - max length of queue of connections waiting to be accepted
    if(listen(fd, SOMAXCONN) < 0) {
        perror("Failed listen");
        exit(6);
    }
    return fd;
}

/*
This is a function for the server that handles all connections and
decides whether to accept or reject a client
*/
void process_connections(int fdServer) {
    int fd;
    struct sockaddr_in fromAddr;
    socklen_t fromAddrSize;
    int error;
    char hostname[MAXHOSTNAMELEN];
    // Repeatedly accept connections and process data from clients (capitalise)
    while(1) {
        fromAddrSize = sizeof(struct sockaddr_in);
	// Block, waiting for a new connection (fromAddr will be populated
	// with address of the client
        fd = accept(fdServer, (struct sockaddr*)&fromAddr, &fromAddrSize);
        if(fd < 0) {
            perror("Failed listen");
            exit(1);
        }
	// Turn our client address into a hostname - print out details to 
	// stdout of server
        error = getnameinfo((struct sockaddr*)&fromAddr, fromAddrSize, hostname,
                MAXHOSTNAMELEN, NULL, 0, 0);
        if(error) {
            fprintf(stderr, "Error getting hostname: %s\n", 
                    gai_strerror(error));
        } else {
            printf("Accepted connection from %s (%s), port %d\n", 
                    inet_ntoa(fromAddr.sin_addr), hostname,
                    ntohs(fromAddr.sin_port));
        }
	// Send a welcome message back to the client
	write(fd, "Welcome\n", 8);
	// Start a new thread to deal with communication to the client
	int* fdPtr = malloc(sizeof(int));
	*fdPtr = fd;
	pthread_t threadId;
	pthread_create(&threadId, NULL, client_thread, fdPtr);
	pthread_detach(threadId);
    }
}

/*
The client thread method is useful as it creates a thread for 
a particular client.
*/
void* client_thread(void* arg) {
    char buffer[1024];
    ssize_t numBytesRead;
    int fd = *(int*)arg;
    free(arg);
    // Repeatedly read data from client, turn it into uppercase, send it back
    while((numBytesRead = read(fd, buffer, 1024)) > 0) {
		write(fd, buffer, numBytesRead);
    }
    if(numBytesRead < 0) {
		fprintf(stderr, "Error reading from socket");
    }
    // Print a message to server's stdout
    printf("Done\n");
    fflush(stdout);
    // Close client socket
    close(fd);
    return NULL;	// could have called pthread_exit(NULL);
}

/*
This function writes to a child based off whether they are
accepted or rejected to the server.
*/
void write_to_child(int fd, HubMessage hubMessage) {
	switch(hubMessage) {
		case (YESVALUE):
			write(fd, "yes\n", YES);
			break;
		case (NOVALUE):
			write(fd, "no\n", NO);
			break;
	}
}

/*
This is the main method for the train-job, it opens all ports for
listening and then will proccess these connections accrodingly.
*/
int main(int argc, char** argv) {
    int i;
    GameDetails* gameDetails = game_details_init(argc);
    error_controller(argc, argv, gameDetails);
    for(i = 0; i < gameDetails-> totalPorts; i++) {
    	gameDetails->fdServers[i] = open_listen(gameDetails->ports[i]);
	}
	for(i = 0; i < gameDetails->totalPorts; i++) {
		process_connections(gameDetails->fdServers[i]);
    }
    return 0;
}

/*
Error controller is a function which will check all args, seeds, timeouts
etc to ensure they are all valid.
*/
void error_controller(int argc, char** argv, GameDetails* gameDetails) {
    if(argc < 5) {
        fprintf(stderr, "Usage: train-job keyfile seed timeout port {port}\n");
        exit(1);
    }
    int i;
    keyfile_check(gameDetails, argv);
    //Check if the seed is valid
    for(i = 0; i < strlen(argv[2]); i++) {
        if(isdigit(argv[2][i]) == 0) {
            fprintf(stderr, "Bad seed\n");
            exit(3);
        }
    }
    //Check the value of the time out seconds
    for(i = 0; i < strlen(argv[3]); i++) {
        if(isdigit(argv[3][i]) == 0) {
            fprintf(stderr, "Bad timeout\n");
            exit(4);
        }
    }
    port_check(gameDetails, argv);
    
}

/*
Port check is a utility function for error controller that will analyse 
all ports and ensure they are valid
*/
void port_check(GameDetails* gameDetails, char** argv) {
	int i, j;
	for(i = 0; i < gameDetails->totalPorts; i++) {
		//fprintf(stderr, "%d\n", gameDetails->totalPorts);
		if((atoi(argv[4 + i]) < 1024 || atoi(argv[4 + i]) > 65535) 
				&& argv[4 + i][0] != '-') {
        	fprintf(stderr,  "Bad port\n");
        	exit(5);
    	}
    }
    for(i = 0; i < gameDetails->totalPorts; i++) {
		// Check port number is valid
     	for (j = 0; j < strlen(argv[4 + i]); j++) {
     		//fprintf(stderr, "%c\n", argv[4][j]);
      		if (isdigit(argv[4][j]) == 0) {
           		fprintf(stderr, "Bad port\n");
           	 	exit(5);
         	}
     	}
    if(argv[4 + i][0] == '-' && strlen(argv[4 + i]) == 1) {
    	gameDetails->ports[i] = 0;
    } else {
    	gameDetails->ports[i] = atoi(argv[4 + i]);
    }
	}
}


/*
Key file check, ensures that all keyfiles are valid and the 
pass char's are correct.
*/
void keyfile_check(GameDetails* gameDetails, char** argv) {
	//Should there be an feof(stdin) check here
    if(strlen(argv[1]) == 0) {
        fprintf(stderr, "Can't get pass-char\n");
        exit(2);
    }
    FILE* filePointer = fopen(argv[1], "r");
    if(filePointer == NULL) {
        fprintf(stderr, "Can't get pass-char\n");
        exit(2);
    }
    char getCharacter;
    char* requestString = (char*)malloc(CLIENTMESSAGE);
    int i = 0;
    while(getCharacter != '\n' && getCharacter != EOF) {
        getCharacter = fgetc(filePointer);
        requestString[i] = getCharacter;
        i++;
    }
    if(sscanf(requestString, "%cscore", &gameDetails->passChar) != 1 ||
    	sscanf(requestString, "%cplay", &gameDetails->passChar) != 1) {
        fprintf(stderr, "Can't get pass-char\n");
        exit(2);
    }
}

/*
This function initialises the GameDetails struct and everything involved.
*/
GameDetails* game_details_init(int argc) {
	//char** playerNames;
	//int i;
	GameDetails* gameDetails = malloc(sizeof(GameDetails));
    gameDetails->totalPorts = argc - 4;
    gameDetails->ports = (int*)malloc(sizeof(int) * gameDetails->totalPorts);
    gameDetails->fdServers = (int*)malloc(sizeof(int) * gameDetails->totalPorts);
    //gameDetails->playerNames = (char**)malloc(sizeof(char*) * 
     //   gameDetails->totalPlayers);
    //playerNames = gameDetails->playerNames;
    //for(i = 0; i < gameDetails->totalPlayers; i++) {
    //    playerNames[i] = (char*)malloc(sizeof(char) * gameDetails->totalPlayers);
    //}
    return gameDetails;
}
