/* 
** CSSE2310/7231 - sample client - code to be commented in class
** Send a request for the top level web page (/) on some webserver and
** print out the response - including HTTP headers.
*/
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h> 
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <ctype.h>
#include "error.h"

#define REQUESTMESSAGE 2048
#define BUFFERVALUE 1024
#define YESVALUE 3
#define PLAYVALUE 6
#define YOURTURNVALUE 8
#define QUESTIONVALUE 2
#define SOCKETVALUE 0

typedef enum {
    YES,
    NO,
    ORDERED,
    HMOVE,
    VMOVE,
    LOOTED,
    SHORTED,
    LONGED,
    DRIEDOUT,
    YOURTURN,
    HORRIZONTAL,
    SHORTORLONG
} ReadMessage;


typedef struct {
    char* playerName;
    char* gameName;
} PlayerDetails;


void write_to_server(ReadMessage readMessage, char initialMove, char secondMove,
        int fd, PlayerDetails* playerDetails);
void get_sever_accept_response(int fd, PlayerDetails* playerDetails);
void prompt_handler(int switchCase, int fd);

/*
This function converts a host name to an IP address
*/
struct in_addr* name_to_ip_addr(char* hostname) {
    int error;
    struct addrinfo* addressInfo;

    error = getaddrinfo(hostname, NULL, NULL, &addressInfo);
    if(error) {
        return NULL;
    }
    // Extract the IP address and return it
    return &(((struct sockaddr_in*)(addressInfo->ai_addr))->sin_addr);
}


int connect_to(struct in_addr* ipAddress, int port) {
    struct sockaddr_in socketAddr;
    int fd;
    // Create socket - TCP socket IPv4
    fd = socket(AF_INET, SOCK_STREAM, SOCKETVALUE);
    if(fd < 0) {
        perror("Error creating socket");
        exit(1);
    }

    // Populate server address structure with IP address and port number of
    // the server
    socketAddr.sin_family = AF_INET;    // IPv4
    socketAddr.sin_port = htons(port);  // convert port num to network byte order
    socketAddr.sin_addr.s_addr = ipAddress->s_addr; // IP address - already in
                                // network byte order
    // Attempt to connect to the server
    if(connect(fd, (struct sockaddr*)&socketAddr, sizeof(socketAddr)) < 0) {
        perror("Error connecting");
        exit(1);
    }

    // Now have connected socket
    return fd;
}

/*
This function will send a play request to 
*/
void send_play_request(int fd, char* file, char* host) {
    char getCharacter;
    FILE* fileFinder = fopen(file, "r");
    getCharacter = fgetc(fileFinder);
    char buffer[9];
    memset(buffer, '\0', PLAYVALUE);
    snprintf(buffer, PLAYVALUE, "%cplay\n", getCharacter);
    // Send request to the server
    if(write(fd, buffer, strlen(buffer)) < 1) {
        perror("Write error");
        exit(1);
    }
}

/*
This is a function to constantly read the server messages and
output the correct message to stdout. Also analyses a successful
and unsucessful connection.
*/
void get_sever_accept_response(int fd, PlayerDetails* playerDetails) {
    char buffer[BUFFERVALUE], initialMove, secondMove;
    int eof = 0, readBytes;
    int validated = 0;
    while(!eof) {
        memset(buffer, '\0', BUFFERVALUE);
        readBytes = read(fd, buffer, BUFFERVALUE);
        printf("Buffer is: <%s>\n", buffer);
        if(readBytes == 0) {
            fprintf(stderr, "Server disconnect\n");
            close(fd);
            exit(7);
        }
        if (buffer[readBytes - 1] == '\n') {
            buffer[readBytes - 1] = '\0';
            readBytes--;
        }
        if (!validated) {
            if (strncmp(buffer, "yes", YESVALUE) == 0) {
                fprintf(stderr, "Handling yes code\n");
                validated = 1;
                write_to_server(YES, initialMove, secondMove, fd, playerDetails);
            } else {
                write_to_server(NO, initialMove, secondMove, fd, playerDetails);
            }
        }
        if (strncmp(buffer, "yourturn", YOURTURNVALUE) == 0) {
            write_to_server(YOURTURN, initialMove, secondMove, fd, playerDetails);
        } else if (strncmp(buffer, "h?", QUESTIONVALUE) == 0) {
            write_to_server(HORRIZONTAL, initialMove, secondMove, fd, 
                    playerDetails);
        } else if (strncmp(buffer, "s?", QUESTIONVALUE) == 0 
                || strncmp(buffer, "l?", QUESTIONVALUE) == 0) {
            write_to_server(SHORTORLONG, initialMove, secondMove, fd, 
                    playerDetails);
        } else if(sscanf(buffer, "ordered%c%c\n", &initialMove, 
                &secondMove) == 2) {
            write_to_server(ORDERED, initialMove, secondMove, fd, playerDetails);
        } else if (sscanf(buffer, "hmove%c%c", &initialMove, &secondMove) == 2) {
            write_to_server(HMOVE, initialMove, secondMove, fd, playerDetails);
        } else if(sscanf(buffer, "vmove%c%c", &initialMove, &secondMove) == 2) {
        	write_to_server(VMOVE, initialMove, secondMove, fd, playerDetails);
    	} else if(sscanf(buffer, "looted%c", &initialMove) == 1) {
            write_to_server(LOOTED, initialMove, secondMove, fd, playerDetails);
        } else if(sscanf(buffer, "short%c%c", &initialMove, &secondMove) == 2) {
            write_to_server(SHORTED, initialMove, secondMove, fd, playerDetails);
        } else if(sscanf(buffer, "long%c%c", &initialMove, &secondMove) == 2) {
            write_to_server(LONGED, initialMove, secondMove, fd, playerDetails);
        } else if(sscanf(buffer, "driedout%c", &initialMove) == 1) {
            write_to_server(DRIEDOUT, initialMove, secondMove, fd, playerDetails);
        }
     printf("End of EOF while\n");
    }
}

/*
This function is used as a utility function to write messages to the server.
*/
void write_to_server(ReadMessage readMessage, char initialMove, char secondMove,
            int fd, PlayerDetails* playerDetails) {
    switch(readMessage) {
        case (YES):
            fprintf(stderr, "%s\n", playerDetails->playerName);
            break;
        case (NO):
            fprintf(stderr, "Auth failed\n");
            close(fd);
            exit(4);
            break;
        case (ORDERED):
            fprintf(stderr, "%sOrdered%c\n", 
            	playerDetails->playerName, secondMove);
            break;
        case (HMOVE):
            fprintf(stderr, "%s moved to %c/%c\n", playerDetails->playerName,
                    initialMove, secondMove);
            break;
        case (VMOVE):
            fprintf(stderr, "%s moved to %c/%c\n", 
            	playerDetails->playerName, initialMove, secondMove);
            //Same as hmove, get one correct first
            break;
        case (LOOTED):
            if(initialMove == '-') {
                fprintf(stderr, "%s failed to loot\n", 
                    playerDetails->playerName);
                fflush(stdout);
            } else {
                fprintf(stderr, "%s looted\n", playerDetails->playerName);
            }
            break;
        case (SHORTED):
            if(secondMove == '-') {
                fprintf(stderr, "%s missed\n", playerDetails->playerName);
            } else {
                fprintf(stderr, "%s shorted %c\n", 
                	playerDetails->playerName, secondMove);
            }
            break;
        case (LONGED):
            if(secondMove == '-') {
                fprintf(stderr, 
                	"%s missed\n", playerDetails->playerName);
            } else {
                fprintf(stderr, "%s shorted %c\n", 
                	playerDetails->playerName, initialMove);
            }
        case (DRIEDOUT):
            fprintf(stderr, "%s dried out\n", playerDetails->playerName);
            break;
        case (YOURTURN):
            prompt_handler(0, fd);
            break;

        case (HORRIZONTAL):

            break;

        case (SHORTORLONG):

            break;
        default:
            break;
    }
}

/*
This is the main function for the train mal, it handles errors, reading
and writing. Additionally, handles the connection procedure.
*/
int main(int argc, char* argv[]) {
    int fd;
    struct in_addr* ipAddress;
    char* hostname;
    error_controller(argc, argv);
    PlayerDetails* playerDetails = malloc(sizeof(PlayerDetails));
    hostname = "localhost";
    // Convert hostname to IP address
    ipAddress = name_to_ip_addr(hostname);
    if(!ipAddress) {
        fprintf(stderr, "%s is not a valid hostname\n", hostname);
        exit(1);
    }
    playerDetails->playerName = argv[3];
    // Establish a connection to port 80 on given IP address
    fd = connect_to(ipAddress, atoi(argv[2]));
    // Send request to server for the top level web page (/)
    send_play_request(fd, argv[1], hostname);
    // Retrieve the response and print it out
    while(1) {
        get_sever_accept_response(fd, playerDetails);
    }
    // Close socket
    return 0;
}

/*
Prompt handler is a utility function to help the prompting to users.
It is controlled by a switch statement for varying conditions.
*/
void prompt_handler(int switchCase, int fd) {
    char charReply;
    char buffer[1024];
    int readBytes, validated = 0;
    switch(switchCase) {
        case 0:
            while(!validated) {
                fprintf(stderr, "move:");
                scanf("%c", &charReply);
                memset(buffer, '\0', 1024);
                snprintf(buffer, 2, "%c\n", charReply);
                write(fd, buffer, strlen(buffer));
                memset(buffer, '\0', 1024);
                readBytes = read(fd, buffer, 1024);
                if(readBytes == 0) {
                    fprintf(stderr, "Server disconnect\n");
                    close(fd);
                    exit(7);
                }
                if (strncmp(buffer, "yes", 3) == 0) {
                    validated = 1;
                } else {
                    printf("\n");
                }
            }
            break;
        case 1:
            while(!validated) {
                printf("hmove:");
                fflush(stdout);
                scanf("%c", &charReply);
                memset(buffer, '\0', 1024);
                snprintf(buffer, 2, "%c\n", charReply);
                write(fd, buffer, strlen(buffer));
                memset(buffer, '\0', 1024);
                readBytes = read(fd, buffer, 1024);
                if(readBytes == 0) {
                    fprintf(stderr, "Server disconnect\n");
                    close(fd);
                    exit(7);
                }
                if (strncmp(buffer, "yes", 3) == 0) {
                    validated = 1;
                } else {
                    printf("\n");
                }
            }
            break;
        case 2:
            while(!validated) {
                printf("target:");
                fflush(stdout);
                scanf("%c", &charReply);
                memset(buffer, '\0', 1024);
                snprintf(buffer, 2, "%c\n", charReply);
                write(fd, buffer, strlen(buffer));
                memset(buffer, '\0', 1024);
                readBytes = read(fd, buffer, 1024);
                if(readBytes == 0) {
                    fprintf(stderr, "Server disconnect\n");
                    close(fd);
                    exit(7);
                }
                if (strncmp(buffer, "yes", 3) == 0) {
                    validated = 1;
                } else {
                    printf("\n");
                }
            }
        default:
            break;
    }
}