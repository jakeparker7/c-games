/* 
** CSSE2310/7231 - sample client - code to be commented in class
** Send a request for the top level web page (/) on some webserver and
** print out the response - including HTTP headers.
*/
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h> 
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <ctype.h>
#include "error.h"

#define REQUESTMESSAGE 2048
#define BUFFERVALUE 1024
#define YESVALUE 3
#define PLAYVALUE 10
#define YOURTURNVALUE 8
#define QUESTIONVALUE 2
#define SOCKETVALUE 0

typedef enum {
    YES,
    NO,
    ORDERED,
    HMOVE,
    VMOVE,
    LOOTED,
    SHORTED,
    LONGED,
    DRIEDOUT,
    YOURTURN,
    HORRIZONTAL,
    SHORTORLONG
} ReadMessage;


typedef struct {
    char* playerName;
    char* gameName;
    char* gameString;
    int totalCarriages;
    int totalPlayers;
} PlayerDetails;


void write_to_server(ReadMessage readMessage, char initialMove, 
        char secondMove, int fd, PlayerDetails* playerDetails);
void get_sever_accept_response(int fd, PlayerDetails* playerDetails);
void prompt_handler(int switchCase, int fd);
void prompt_user(char* promptMessage, int fd);
void server_reader(char* buffer, int fd, PlayerDetails* playerDetails);
void write_to_server_two(ReadMessage readMessage, 
        char initialMove, char secondMove,
        int fd, PlayerDetails* playerDetails);

/*
This function converts a host name to an IP address
*/
struct in_addr* name_to_ip_addr(char* hostname) {
    int error;
    struct addrinfo* addressInfo;

    error = getaddrinfo(hostname, NULL, NULL, &addressInfo);
    if(error) {
        return NULL;
    }
    // Extract the IP address and return it
    return &(((struct sockaddr_in*)(addressInfo->ai_addr))->sin_addr);
}


/*
The connect to function is to handle a connection of the client to
the server, insuring a valid socket.
*/
int connect_to(struct in_addr* ipAddress, int port) {
    struct sockaddr_in socketAddr;
    int fd;
    // Create socket - TCP socket IPv4
    fd = socket(AF_INET, SOCK_STREAM, SOCKETVALUE);
    if(fd < 0) {
        perror("Error creating socket");
        exit(1);
    }

    // Populate server address structure with IP address and port number of
    // the server
    socketAddr.sin_family = AF_INET;    // IPv4
    socketAddr.sin_port = htons(port);  // convert port num to network byte o
    socketAddr.sin_addr.s_addr = ipAddress->s_addr; // IP address - already in
                                // network byte order
    // Attempt to connect to the server
    if(connect(fd, (struct sockaddr*)&socketAddr, sizeof(socketAddr)) < 0) {
        perror("Error connecting");
        exit(1);
    }

    // Now have connected socket
    return fd;
}

/*
This function will send a play request to 
*/
void send_play_request(int fd, char* file, char* host) {
    char getCharacter;
    FILE* fileFinder = fopen(file, "r");
    getCharacter = fgetc(fileFinder);
    char buffer[PLAYVALUE];
    memset(buffer, '\0', PLAYVALUE);
    snprintf(buffer, PLAYVALUE, "%cplay\n", getCharacter);
    // Send request to the server
    if(write(fd, buffer, strlen(buffer)) < 1) {
        perror("Write error");
        exit(1);
    }
}

/*
This is a function to constantly read the server messages and
output the correct message to stdout. Also analyses a successful
and unsucessful connection.
*/
void get_sever_accept_response(int fd, PlayerDetails* playerDetails) {
    char buffer[BUFFERVALUE], initialMove = NULL, secondMove = NULL;
    int eof = 0, readBytes;
    int validated = 0;
    while(!eof) {
        memset(buffer, '\0', BUFFERVALUE);
        readBytes = read(fd, buffer, BUFFERVALUE);
        printf("Buffer is: <%s>\n", buffer);
        if(readBytes == 0) {
            fprintf(stderr, "Server disconnect\n");
            close(fd);
            exit(7);
        }
        if (buffer[readBytes - 1] == '\n') {
            buffer[readBytes - 1] = '\0';
            readBytes--;
        }
        if (!validated) {
            if (strncmp(buffer, "yes", YESVALUE) == 0) {
                write_to_server(YES, initialMove, secondMove, 
                        fd, playerDetails);
                validated = 1;
            } else {
                write_to_server(NO, initialMove, secondMove, 
                        fd, playerDetails);
            }
        }
        server_reader(buffer, fd, playerDetails);
    }
}

/*
server reader is a function that will handle strings read in from the server
*/
void server_reader(char* buffer, int fd, PlayerDetails* playerDetails) {
    char initialMove, secondMove, additional;
    if (strncmp(buffer, "yourturn", YOURTURNVALUE) == 0) {
        write_to_server_two(YOURTURN, initialMove, 
                secondMove, fd, playerDetails);
    } else if (strncmp(buffer, "h?", QUESTIONVALUE) == 0) {
        write_to_server_two(HORRIZONTAL, initialMove, secondMove, fd, 
                playerDetails);
    } else if (strncmp(buffer, "s?", QUESTIONVALUE) == 0 
            || strncmp(buffer, "l?", QUESTIONVALUE) == 0) {
        write_to_server_two(SHORTORLONG, initialMove, secondMove, fd, 
                playerDetails);
    } else if(sscanf(buffer, "ordered%c%c%c", &initialMove, 
            &secondMove, &additional) == 2) {
        write_to_server(ORDERED, initialMove, secondMove, fd, playerDetails);
    } else if (sscanf(buffer, "hmove%c%c%c", &initialMove, 
            &secondMove, &additional) == 2) {
        write_to_server(HMOVE, initialMove, secondMove, fd, playerDetails);
    } else if(sscanf(buffer, "vmove%c%c%c", &initialMove, 
            &secondMove, &additional) == 2) {
        write_to_server(VMOVE, initialMove, secondMove, fd, playerDetails);
    } else if(sscanf(buffer, "looted%c%c", &initialMove, 
            &additional) == 1) {
        write_to_server(LOOTED, initialMove, secondMove, fd, playerDetails);
    } else if(sscanf(buffer, "short%c%c%c", &initialMove, 
            &secondMove, &additional) == 2) {
        write_to_server_two(SHORTED, initialMove, secondMove, 
                fd, playerDetails);
    } else if(sscanf(buffer, "long%c%c%c", &initialMove, 
            &secondMove, &additional) == 2) {
        write_to_server_two(LONGED, initialMove, secondMove, 
                fd, playerDetails);
    } else if(sscanf(buffer, "driedout%c%c", 
            &initialMove, &additional) == 1) {
        write_to_server_two(DRIEDOUT, initialMove, secondMove, 
                fd, playerDetails);
    }
}

/*
This function is used as a utility function to write messages to the server.
*/
void write_to_server(ReadMessage readMessage, char initialMove, 
        char secondMove, int fd, PlayerDetails* playerDetails) {
    char buffer[BUFFERVALUE];
    switch(readMessage) {
        case (YES):
            memset(buffer, '\0', BUFFERVALUE);
            snprintf(buffer, BUFFERVALUE, "%s\n", playerDetails->playerName);
            write(fd, buffer, strlen(buffer));
            memset(buffer, '\0', BUFFERVALUE);
            snprintf(buffer, BUFFERVALUE, "%s\n", playerDetails->gameString);
            write(fd, buffer, strlen(buffer));
            break;
        case (NO):
            fprintf(stderr, "Auth failed\n");
            close(fd);
            exit(4);
            break;
        case (ORDERED):
            fprintf(stderr, "%sOrdered%c\n", 
            	    playerDetails->playerName, secondMove);
            break;
        case (HMOVE):
            fprintf(stderr, "%s moved to %c/%c\n", playerDetails->playerName,
                    initialMove, secondMove);
            break;
        case (VMOVE):
            fprintf(stderr, "%s moved to %c/%c\n", 
            	    playerDetails->playerName, initialMove, secondMove);
            break;
        case (LOOTED):
            if(initialMove == '-') {
                fprintf(stderr, "%s failed to loot\n", 
                        playerDetails->playerName);
            } else {
                fprintf(stderr, "%s looted\n", playerDetails->playerName);
            }
            break;
        default:
            break;
    }
}

/*
This function is an extention of write to server, it handles more switch
arguments. This function was created to be able to handle more statements
within the function line limit.
*/
void write_to_server_two(ReadMessage readMessage, char initialMove, 
        char secondMove, int fd, PlayerDetails* playerDetails) {
    switch(readMessage) {
        case (SHORTED):
            if(secondMove == '-') {
                fprintf(stderr, "%s missed\n", playerDetails->playerName);
            } else {
                fprintf(stderr, "%s shorted %c\n", 
                        playerDetails->playerName, secondMove);
            }
            break;
        case (LONGED):
            if(secondMove == '-') {
                fprintf(stderr, 
                        "%s missed\n", playerDetails->playerName);
            } else {
                fprintf(stderr, "%s shorted %c\n", 
                        playerDetails->playerName, initialMove);
            }
            break;
        case (DRIEDOUT):
            fprintf(stderr, "%s dried out\n", playerDetails->playerName);
            break;
        case (YOURTURN):
            prompt_handler(0, fd);
            break;
        case (HORRIZONTAL):
            prompt_handler(1, fd);
            break;
        case (SHORTORLONG):
            prompt_handler(2, fd);
            break;
        default:
            break;
    }
}

/*
This is the main function for the train mal, it handles errors, reading
and writing. Additionally, handles the connection procedure.
*/
int main(int argc, char* argv[]) {
    int fd;
    struct in_addr* ipAddress;
    char* hostname;
    error_controller(argc, argv);
    PlayerDetails* playerDetails = malloc(sizeof(PlayerDetails));
    playerDetails->playerName = (char*)malloc(BUFFERVALUE);
    playerDetails->gameString = (char*)malloc(BUFFERVALUE);
    playerDetails->gameName = (char*)malloc(BUFFERVALUE);
    hostname = "localhost";
    // Convert hostname to IP address
    ipAddress = name_to_ip_addr(hostname);
    if(!ipAddress) {
        fprintf(stderr, "%s is not a valid hostname\n", hostname);
        exit(1);
    }
    playerDetails->playerName = argv[3];
    playerDetails->gameString = argv[4];

    // Establish a connection to port 80 on given IP address
    fd = connect_to(ipAddress, atoi(argv[2]));
    // Send request to server for the top level web page (/)
    send_play_request(fd, argv[1], hostname);
    // Retrieve the response and print it out
    while(1) {
        get_sever_accept_response(fd, playerDetails);
    }
    return 0;
}

/*
Prompt handler is a utility function to help the prompting to users.
It is controlled by a switch statement for varying conditions.
*/
void prompt_handler(int switchCase, int fd) {
    switch(switchCase) {
        case 0:
            prompt_user("move:", fd);
        case 1:
            prompt_user("hmove:", fd);
            break;
        case 2:
            prompt_user("target:", fd);
            break;
    }
}

/*
Prompt user is a utility function that takes in a string
and an fd. From this it will give the user a valid prompt
and wait for the serer to reply.
*/
void prompt_user(char* promptMessage, int fd) {
    char charReply;
    char buffer[BUFFERVALUE];
    int readBytes, validated = 0;
    while(!validated) {
        fprintf(stderr, "%s", promptMessage);
        scanf("%c", &charReply);
        memset(buffer, '\0', BUFFERVALUE);
        snprintf(buffer, QUESTIONVALUE, "%c\n", charReply);
        write(fd, buffer, strlen(buffer));
        memset(buffer, '\0', BUFFERVALUE);
        readBytes = read(fd, buffer, BUFFERVALUE);
        if(readBytes == 0) {
            fprintf(stderr, "Server disconnect\n");
            close(fd);
            exit(7);
        }
        if (strncmp(buffer, "yes", 3) == 0) {
            printf("\n");
            validated = 1;
        } else {
            printf("\n");
        }
    }
}

