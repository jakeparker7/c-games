/* 
** CSSE2310/7231 - sample client - code to be commented in class
** Send a request for the top level web page (/) on some webserver and
** print out the response - including HTTP headers.
*/
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h> 
#include <unistd.h>
#include <netdb.h>
#include <string.h>
#include <ctype.h>
#include "error.h"
#include "utils.h"
#include "player.h"

#define REQUESTMESSAGE 2048
#define BUFFERVALUE 1024
#define PLAYVALUE 10
#define YOURTURNVALUE 8
#define QUESTIONVALUE 2
#define SOCKETVALUE 0

typedef struct {
    char* playerName;
    char* gameName;
    char* gameString;
    int totalCarriages;
    int totalPlayers;
} PlayerDetails;

typedef enum {
    PLAYERORDERED,
    HMOVE,
    VMOVE,
    PLAYERLOOTED,
    SHORTED,
    LONGED,
    DRIEDOUT
} ReadMessage;

void player_update(ReadMessage readMessage, char initialMove, char secondMove,
        int fd);
void player_decision(int switchCase, PlayerDetails* playerDetails);
void server_reader(char* buffer, int fd, PlayerDetails* playerDetails);

/*
This function is to change the name of the hostname
to an IP address
*/
struct in_addr* name_to_ip_addr(char* hostname) {
    int error;
    struct addrinfo* addressInfo;
    error = getaddrinfo(hostname, NULL, NULL, &addressInfo);
    if(error) {
        return NULL;
    }
    // Extract the IP address and return it
    return &(((struct sockaddr_in*)(addressInfo->ai_addr))->sin_addr);
}

/*
This function is to help connect to a particular port
*/
int connect_to(struct in_addr* ipAddress, int port) {
    struct sockaddr_in socketAddr;
    int fd;
    // Create socket - TCP socket IPv4
    fd = socket(AF_INET, SOCK_STREAM, SOCKETVALUE);
    if(fd < 0) {
        perror("Error creating socket");
        exit(1);
    }
    // Populate server address structure with IP address and port number of
    // the server
    socketAddr.sin_family = AF_INET;    // IPv4
    socketAddr.sin_port = htons(port);  // convert port num to network byte ord
    socketAddr.sin_addr.s_addr = ipAddress->s_addr; // IP address - already in
                                // network byte order
    // Attempt to connect to the server
    if(connect(fd, (struct sockaddr*)&socketAddr, sizeof(socketAddr)) < 0) {
        perror("Error connecting");
        exit(1);
    }
    // Now have connected socket
    return fd;
}

/*
This function simply sends the pass char and play to the server
*/
void send_play_message(int fd, char* file, char* host) {
    char getCharacter;
    FILE* fileFinder = fopen(file, "r");
    getCharacter = fgetc(fileFinder);
    char buffer[PLAYVALUE];
    memset(buffer, '\0', PLAYVALUE);
    snprintf(buffer, PLAYVALUE, "%cplay\n", getCharacter);
    // Send request to the server
    if(write(fd, buffer, strlen(buffer)) < 1) {
        perror("Write error");
        exit(1);
    }
}

/*
This function handles the response from the server and 
connects
*/
void get_server_response(int fd, PlayerDetails* playerDetails) {
    char buffer[BUFFERVALUE];
    int eof = 0, readBytes, validated = 0;
    while(!eof) {
        memset(buffer, '\0', BUFFERVALUE);
        readBytes = read(fd, buffer, BUFFERVALUE);
        //printf("Buffer is: <%s>\n", buffer);
        if(readBytes == 0) {
            fprintf(stderr, "Not allowed\n");
            close(fd);
            exit(5);
        }
        if (buffer[readBytes - 1] == '\n') {
            buffer[readBytes - 1] = '\0';
            readBytes--;
        }
        if (!validated) {
            if (strncmp(buffer, "yes", 3) == 0) {
                memset(buffer, '\0', BUFFERVALUE);
                snprintf(buffer, BUFFERVALUE, "%s\n", 
                        playerDetails->playerName);
            	write(fd, buffer, strlen(buffer));
            	memset(buffer, '\0', BUFFERVALUE);
            	snprintf(buffer, BUFFERVALUE, "%s\n", 
                        playerDetails->gameString);
            	write(fd, buffer, strlen(buffer));
                validated = 1;
            } else {
                fprintf(stderr, "Auth failed\n");
                close(fd);
                exit(4);
            }
        }
        server_reader(buffer, fd, playerDetails);
    }
}

/*
This function is created to read the servers messages.
*/
void server_reader(char* buffer, int fd, PlayerDetails* playerDetails) {
    char initialMove, secondMove, additional;
    if (strncmp(buffer, "yourturn", YOURTURNVALUE) == 0) {
        player_decision(0, playerDetails);
    } else if (strncmp(buffer, "h?", QUESTIONVALUE) == 0) {
        player_decision(1, playerDetails);
    } else if (strncmp(buffer, "s?", QUESTIONVALUE) == 0 
            || strncmp(buffer, "l?", QUESTIONVALUE) == 0) {
        player_decision(2, playerDetails);
    } else if(sscanf(buffer, "ordered%c%c%c", &initialMove, 
            &secondMove, &additional) == 2) {
        player_update(ORDERED, initialMove, secondMove, fd);
    } else if (sscanf(buffer, "hmove%c%c%c", &initialMove, 
            &secondMove, &additional) == 2) {
        player_update(HMOVE, initialMove, secondMove, fd);
    } else if(sscanf(buffer, "vmove%c%c%c", &initialMove, 
            &secondMove, &additional) == 2) {
        player_update(VMOVE, initialMove, secondMove, fd);
    } else if(sscanf(buffer, "looted%c%c", &initialMove, 
            &additional) == 1) {
        player_update(LOOTED, initialMove, secondMove, fd);
    } else if(sscanf(buffer, "short%c%c%c", &initialMove, 
            &secondMove, &additional) == 2) {
        player_update(SHORTED, initialMove, secondMove, fd);
    } else if(sscanf(buffer, "long%c%c%c", &initialMove, 
            &secondMove, &additional) == 2) {
        player_update(LONGED, initialMove, secondMove, fd);
    } else if(sscanf(buffer, "driedout%c%c", 
            &initialMove, &additional) == 1) {
        player_update(DRIEDOUT, initialMove, secondMove, fd);
    }
}

/*
this is a utility function for writing to the server.
*/
void player_decision(int switchCase, PlayerDetails* playerDetails) {
    switch(switchCase) {
        case (0):
            //DECIDING AND THEN SENDING
            //send_action(playerDetails->playerName, stdout);
            break;
        case (1):
            //Horrizontal choice
            //send_hchoice(int v, FILE* out);
            break;
        case (2):
            //Short choice
            //Send choice
            break;
        case (3):
            //long choice
        default:
            break;
    }
}

/*
Player update is a function to update the player as each move comes in.
*/
void player_update(ReadMessage readMessage, char initialMove, char secondMove,
        int fd) {
    switch(readMessage) {
        case (PLAYERORDERED):

            break;
        case (HMOVE):

            break;
        case (VMOVE):

            break;
        case (PLAYERLOOTED):

            break;
        case (SHORTED):

            break;
        case (LONGED):

            break;
        case (DRIEDOUT):

            break;
    }
}

/*
This is the main method for train-jayne and handles the majority of
actions.
*/
int main(int argc, char* argv[]) {
    int fd;
    struct in_addr* ipAddress;
    char* hostname;
    error_controller(argc, argv);
    PlayerDetails* playerDetails = malloc(sizeof(PlayerDetails));
    playerDetails->playerName = (char*)malloc(BUFFERVALUE);
    playerDetails->gameString = (char*)malloc(BUFFERVALUE);
    playerDetails->gameName = (char*)malloc(BUFFERVALUE);
    hostname = "localhost";
    // Convert hostname to IP address
    ipAddress = name_to_ip_addr(hostname);
    if(!ipAddress) {
        fprintf(stderr, "%s is not a valid hostname\n", hostname);
        exit(1);
    }
    playerDetails->playerName = argv[3];
    playerDetails->gameString = argv[4];

    // Establish a connection to port 80 on given IP address
    fd = connect_to(ipAddress, atoi(argv[2]));
    // Send request to server for the top level web page (/)
    send_play_message(fd, argv[1], hostname);
    while(1) {
        // Retrieve the response and print it out
        get_server_response(fd, playerDetails);
    }
    // Close socket
    close(fd);
    return 0;
}

