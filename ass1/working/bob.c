#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>

typedef struct {
    int row;
    int collum;
    int moveOCounter;
} PlayerO;

typedef struct {
    int row;
    int collum;
    int moveXCounter;
} PlayerX;

typedef struct {
    int height;
    int width;
    char** board;
    char currentPlayer;
} BoardStr;

typedef struct {
    int row;
    int collum;
    char playerCharacter;
    char** visitedBoard;
} CheckPos;


int max(BoardStr* pboardStruct);
void print_board(BoardStr* pboardStruct);
void computer_player_x(BoardStr* pboardStruct, PlayerX* playerxP);
void computer_player_o(BoardStr* pboardStruct, PlayerO* playeroP);
void my_read_first_line(FILE* fp, BoardStr* pboardStruct, 
        PlayerX* playerxP, PlayerO* playeroP);
void load_game(BoardStr* pboardStruct, PlayerX* playerxP, 
        PlayerO* playeroP, FILE* fp);
void read_board(FILE* fp, BoardStr* pboardStruct, PlayerO* playeroP, 
        PlayerX* playerxP);
void recursive_check_x(BoardStr* pboardStruct, CheckPos* pcheckPos);
void check_for_win(BoardStr* pboardStruct);
void save_game(char* filename, BoardStr* pboardStruct, PlayerX* playerxP, 
        PlayerO* playeroP);
void visited_board(CheckPos* pcheckPos, BoardStr* pboardStruct);
void recursive_check_o(BoardStr* pboardStruct, CheckPos* pcheckPos);
void recursive_check_o_right(BoardStr* pboardStruct, CheckPos* pcheckPos, 
        int height, int width, char** board, char** visitedBoard);
void recursive_check_o_below(BoardStr* pboardStruct, CheckPos* pcheckPos, 
        int height, int width, char** board, char** visitedBoard);
void recursive_check_o_below_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard);
void recursive_check_o_left(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard);
void recursive_check_o_top_left(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard);
void recursive_check_o_top(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard);
void recursive_check_x_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard);
void recursive_check_x_below(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard);
void recursive_check_x_below_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard);
void recursive_check_x_left(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard);
void recursive_check_x_top_left(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard);
void recursive_check_x_top(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard);
void check_for_win_x_below(BoardStr* pboardStruct, CheckPos* pcheckPos, 
        int i, int j, char** board, int height);
void check_for_win_x_below_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int i, int j, char** board, int height);
void check_for_win_o_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int i, int j, char** board, int width);
void check_for_win_o_below_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int i, int j, char** board, int width);
void argc_5_handler(BoardStr* pboardStruct, char** argv, 
        PlayerX* playerXPointer, PlayerO* playeroP);
int error_check_input(int spaceCounter, int row, int height, 
        int width, int collum, char** board);
void manual_change_position_a_m(char** board, int row, int collum, 
        char* currentPlayer);
void manual_change_position_m_a(char** board, int row, int collum, 
        char* currentPlayer);
void manual_change_position_m(char** board, int row, int collum, 
        char* currentPlayer);
void computer_move_x(BoardStr* pboardStruct, PlayerX* playerxP, 
        char* currentPlayer, char** board);
void computer_move_o(BoardStr* pboardStruct, PlayerO* playeroP,
        char* currentPlayer, char** board);
void print_board_check_win(BoardStr* pboardStruct);
int check_for_arg_input(char* fileString);

/*
 *  game_m_m is a function that allows two manual players to play between
 *  each other. Each player enters a row and collum, the function
 *  reads the value and prints to the board. It continues this until
 *  a player has won the game (check_for_win). It also calls a 
 *  (save_game) function, which allows a user to write a game to a 
 *  file which can later be loaded.
 *  @Param
 *  Takes into pointer's to three Structs:
 *      pboardStruct
 *      playerxP
 *      playerOPtr
 *  The function also takes in:
 *      argv
 *  @Return
 *  The function is a void, it continues to loop until a game is won.
 */
void game_m_m(char** argv, BoardStr* pboardStruct, PlayerX* playerxP, 
        PlayerO* playeroP) {
    int row = 0;
    int collum = 0;
    int i;
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    char** board = pboardStruct->board;
    char currentPlayer = pboardStruct->currentPlayer;
    while(1) {
        printf("Player %c] ", currentPlayer);
        int spaceCounter = 0;
        char* fileString = (char*)malloc(71);
        char* filename = (char*)malloc(20);
        char* dummyString = (char*)malloc(10);
        memset(filename, '\0', 20);
        memset(fileString, '\0', 71);
        fgets(fileString, 71, stdin);
        if(check_for_arg_input(fileString)) {
            continue;
        }
        if(fileString[0] == 's') {
            memmove(filename, fileString + 1, strlen(fileString + 1));
            filename[strlen(fileString) - 2] = '\0';
            filename[strlen(fileString) - 1] = '\0';
            pboardStruct->currentPlayer = currentPlayer;
            save_game(filename, pboardStruct, playerxP, playeroP);
        } else {
            sscanf(fileString, "%d %d", &row, &collum);
        }
        if(sscanf(fileString, "%d %d%s", &row, &collum, dummyString) != 2) {
            continue;
        }
        for(i = 0; i < strlen(fileString) - 1; i++) {
            if(fileString[i] == ' ') {
                spaceCounter++;
            }
        }
        if (error_check_input(spaceCounter, row, height, width, 
                collum, board)) {
            continue;
        }
        manual_change_position_m(board, row, collum, &currentPlayer);
        print_board_check_win(pboardStruct);
    }
}

/*
 *  game_m_a allows a manual and automatic player to play between
 *  each other. The manual player enters a row and collum, the function
 *  reads the value and prints to the board, followed by the automatic
 *   players move. It continues this until
 *  a player has won the game (check_for_win). It also calls a 
 *  (save_game) function, which allows a user to write a game to a 
 *  file which can later be loaded.
 *  @Param
 *  Takes into pointer's to three Structs:
 *      pboardStruct
 *      playerxP
 *      playerOPtr
 *  The function also takes in:
 *      argv
 *  @Return
 *  The function is a void, it continues to loop until a game is won.
 */
void game_m_a(char** argv, BoardStr* pboardStruct, PlayerX* playerxP, 
        PlayerO* playeroP) {
    int row = 0;
    int collum = 0;
    int i;
    char currentPlayer = pboardStruct->currentPlayer;
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    char** board = pboardStruct->board;
    while(1) {
        printf("Player %c] ", currentPlayer);
        int spaceCounter = 0;
        char* fileString = (char*)malloc(50);
        char* filename = (char*)malloc(20);
        char* dummyString = (char*)malloc(10);
        memset(filename, '\0', 20);
        fgets(fileString, 70, stdin);
        if(check_for_arg_input(fileString)) {
            continue;
        }
        if(fileString[0] == 's') {
            memmove(filename, fileString + 1, strlen(fileString + 1));
            filename[strlen(fileString) - 2] = '\0';
            filename[strlen(fileString) - 1] = '\0';
            pboardStruct->currentPlayer = currentPlayer;
            save_game(filename, pboardStruct, playerxP, playeroP);
        } else {
            sscanf(fileString, "%d %d", &row, &collum);
        }
        if(sscanf(fileString, "%d %d%s", &row, &collum, dummyString) != 2) {
            continue;
        }
        for(i = 0; i < strlen(fileString) - 1; i++) {
            if(fileString[i] == ' ') {
                spaceCounter++;
            }
        }
        if (error_check_input(spaceCounter, row, height, width, 
                collum, board)) {
            continue;
        }
        manual_change_position_m_a(board, row, collum, &currentPlayer);
        print_board_check_win(pboardStruct);
        if(currentPlayer == 'X') {
            computer_move_x(pboardStruct, playerxP, &currentPlayer, board);
        }
        print_board_check_win(pboardStruct);
    }
}

/*
 *  This function checks the file string input, to ensure it
 *  is viable. It checks the amount of inputs. aswell if an
 *  EOF is found
 *  @Param
 *  Takes in a char*
 *      fileString
 *  @Return
 *  The function is a vint function that continues back through the loop if
 *  the buffer exceeds 70.
 */
int check_for_arg_input(char* fileString) {
    if(feof(stdin) && strlen(fileString) == 0) {
        fprintf(stderr, "EOF from user\n");
        exit(6);
    }
    if(fileString[strlen(fileString) - 1] != '\n' && strlen(fileString) 
            == 70) {
        //Invalid
        char charIn;
        while(1) {
            charIn = fgetc(stdin);
            if (charIn == EOF || charIn == '\n') {
                return 1;
            }
        }
    }
    return 0;
}

/*
 *  This function conducts the computer player X move and 
 *  changes the value on the board. It additionally displays
 *  the computer move
 *  @Param
 *  Takes in the char** grid
 *      board
 *  The function also takes in the struct pointers of BoardStr & PlayerO
 *      pboardStruct
 *      playerxP
 *  Additionally takes in who the currentPlayer is
 *      currentPlayer
 *  @Return
 *  The function is a void, that conducts computer player O's move
 */
void computer_move_x(BoardStr* pboardStruct, PlayerX* playerxP, 
        char* currentPlayer, char** board) {
    computer_player_x(pboardStruct, playerxP);
    while(board[playerxP->row][playerxP->collum] != '.') {
        computer_player_x(pboardStruct, playerxP);
    }
    if(board[playerxP->row][playerxP->collum] == '.') {
        printf("Player %c => %d %d\n", *currentPlayer, 
                playerxP->row, playerxP->collum);
        board[playerxP->row][playerxP->collum] = *currentPlayer;
        *currentPlayer = 'O';
    }
}

/*
 *  This function conducts the computer player O move and 
 *  changes the value on the board. It additionally displays
 *  the computer move
 *  @Param
 *  Takes in the char** grid
 *      board
 *  The function also takes in the struct pointers of BoardStr & PlayerO
 *      pboardStruct
 *      playeroP
 *  Additionally takes in who the currentPlayer is
 *      currentPlayer
 *  @Return
 *  The function is a void, that conducts computer player O's move
 */
void computer_move_o(BoardStr* pboardStruct, PlayerO* playeroP, 
        char* currentPlayer, char** board) {
    computer_player_o(pboardStruct, playeroP);
    while(board[playeroP->row][playeroP->collum] != '.') {
        computer_player_o(pboardStruct, playeroP);
    }
    if(board[playeroP->row][playeroP->collum] == '.') {
        printf("Player %c => %d %d\n", *currentPlayer, 
                playeroP->row, playeroP->collum);
        board[playeroP->row][playeroP->collum] = *currentPlayer;
        *currentPlayer = 'X';
    }
}

/*
 *  This function is simply prints the board and checks for a win
 *  @Param
 *  Takes in the point to the struct boardStr
 *      pboardStruct
 *  @Return
 *  The function is a void that prints the board and checks for a win
 */
void print_board_check_win(BoardStr* pboardStruct) {
    print_board(pboardStruct);
    check_for_win(pboardStruct);
}

/*
 *  This function is to reduce the size of both games m_a m_m
 *  a_m. The function performs if statements that would usually
 *  result in a re prompting of a move. Instead of writing all statement
 *  in the games while loop, it returns a 1 or 0 for the a continue
 *  statement
 *  @Param
 *  Takes in the char** grid
 *      board
 *  The function also takes in the move
 *      row
 *      collum
 *  Additionally takes in a space Counter for the check of spaces
 *      spaceCouunter
 *  @Return
 *  The function is a int, that returns a 1 or 0 for the correct
 *  for varying if statements
 */
int error_check_input(int spaceCounter, int row, int height, 
        int width, int collum, char** board) {
    if(spaceCounter > 1) {
        return 1;
    }
    if(row > (height - 1) || collum > (width - 1)) {
        return 1;
    }
    if(board[row][collum] != '.') {
        return 1;
    }
    return 0;
}

/*
 *  This function is to reduce the size of both games m_a and
 *  The function performs a move on the board if it is
 *  available.
 *  @Param
 *  Takes in the char** grid
 *      board
 *  The function also takes in the move and the player
 *      row
 *      collum
 *      currentPlayer
 *  @Return
 *  The function is a void, that performs the required char
 *  change on the board.
 */
void manual_change_position_m_a(char** board, int row, int collum, 
        char* currentPlayer) {
    if(board[row][collum] == '.') {
        if(*currentPlayer == 'O') {
            board[row][collum] = 'O';
            *currentPlayer = 'X';
        }
    }
}

/*
 *  This function is to reduce the size of both games a_m
 *  The function performs a move on the board if it is
 *  available.
 *  @Param
 *  Takes in the char** grid
 *      board
 *  The function also takes in the move and the player
 *      row
 *      collum
 *      currentPlayer
 *  @Return
 *  The function is a void, that performs the required char
 *  change on the board.
 */
void manual_change_position_a_m(char** board, int row, int collum, 
        char* currentPlayer) {
    if(board[row][collum] == '.') {
        if(*currentPlayer == 'X') {
            board[row][collum] = 'X';
            *currentPlayer = 'O';
        }
    }
}

/*
 *  This function is to reduce the size of game_m_m
 *  The function performs a move on the board if it is
 *  available.
 *  @Param
 *  Takes in the char** grid
 *      board
 *  The function also takes in the move and the player
 *      row
 *      collum
 *      currentPlayer
 *  @Return
 *  The function is a void, that performs the required char
 *  change on the board.
 */
void manual_change_position_m(char** board, int row, int collum, 
        char* currentPlayer) {
    if(board[row][collum] == '.') {
        if(*currentPlayer == 'O') {
            board[row][collum] = 'O';
            *currentPlayer = 'X';
        } else {
            board[row][collum] = 'X';
            *currentPlayer = 'O';
        }
    }
}

/*
 *  game_a_m allows a manual and automatic player to play between
 *  each other.The automatic player plays, then the manual player
 *  enters a row and collum, the function
 *  reads the value and prints to the board. It continues this until
 *  a player has won the game (check_for_win). It also calls a 
 *  (save_game) function, which allows a user to write a game to a 
 *  file which can later be loaded.
 *  @Param
 *  Takes into pointer's to three Structs:
 *      pboardStruct
 *      playerxP
 *      playerOPtr
 *  The function also takes in:
 *      argv
 *  @Return
 *  The function is a void, it continues to loop until a game is won.
 */
void game_a_m(char** argv, BoardStr* pboardStruct, PlayerX* playerxP, 
        PlayerO* playeroP) {
    int row = 0;
    int collum = 0;
    int i;
    char currentPlayer = pboardStruct->currentPlayer;
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    char** board = pboardStruct->board;
    while(1) {
        if(currentPlayer == 'O') {
            computer_move_o(pboardStruct, playeroP, 
                    &currentPlayer, board);
            print_board_check_win(pboardStruct);
        }
        printf("Player %c] ", currentPlayer);
        int spaceCounter = 0;
        char* fileString = (char*)malloc(50);
        char* filename = (char*)malloc(20);
        char* dummyString = (char*)malloc(10);
        memset(filename, '\0', 20);
        fgets(fileString, 50, stdin);
        if(check_for_arg_input(fileString)) {
            continue;
        }
        if(fileString[0] == 's') {
            memmove(filename, fileString + 1, strlen(fileString + 1));
            filename[strlen(fileString) - 2] = '\0';
            filename[strlen(fileString) - 1] = '\0';
            pboardStruct->currentPlayer = currentPlayer;
            save_game(filename, pboardStruct, playerxP, playeroP);
        } else {
            sscanf(fileString, "%d %d", &row, &collum);
        }
        if(sscanf(fileString, "%d %d%s", &row, &collum, dummyString) != 2) {
            continue;
        }
        for(i = 0; i < strlen(fileString) - 1; i++) {
            if(fileString[i] == ' ') {
                spaceCounter++;
            }
        }
        if (error_check_input(spaceCounter, row, height, width, 
                collum, board)) {
            continue;
        }
        manual_change_position_a_m(board, row, collum, &currentPlayer);
        print_board_check_win(pboardStruct);
    }
}

/*
 *  game_a_a is a function that allows two automatic players to play between
 *  each other. The automatic players will both continuosly enter rows 
 *  and collums on the board. It continues this until
 *  a player has won the game (check_for_win). It also calls a 
 *  (save_game) function, which allows a user to write a game to a 
 *  file which can later be loaded.
 *  @Param
 *  Takes into pointer's to three Structs:
 *      pboardStruct
 *      playerxP
 *      playerOPtr
 *  The function also takes in:
 *      argv
 *  @Return
 *  The function is a void, it continues to loop until a game is won.
 */
void game_a_a(char** argv, BoardStr* pboardStruct, 
        PlayerX* playerxP, PlayerO* playeroP) {
    //Setting Game Details
    char** board = pboardStruct->board;
    char currentPlayer;
    currentPlayer = pboardStruct->currentPlayer;
    while(1) {
        if(currentPlayer == 'O') {
            computer_move_o(pboardStruct, playeroP, 
                    &currentPlayer, board);
        }
        print_board(pboardStruct);
        if(currentPlayer == 'X') {
            computer_move_x(pboardStruct, playerxP, &currentPlayer, board);
        }
        print_board_check_win(pboardStruct);
    }
}

/*
 *  board_display uses both the calcualted height and width. Which
 *  are assigned with in BoardStr. It uses the height and width to
 *  allocate memory to char** board in BoardStr
 *  @Param
 *  Takes in the pointer to BoardStr:
 *      pboardStruct
 *  @Return
 *  The function is a void, and is purely for allocating memory
 */
void board_display(BoardStr* pboardStruct) {
    int i;
    int j;
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    char** board;
    pboardStruct->board = (char**)malloc(sizeof(char*) * height);
    board = pboardStruct->board;
    for(i = 0; i < height; i++) {
        board[i] = (char*)malloc(sizeof(char) * (width));
    }
    for(i = 0; i < height; i++) {
        for(j = 0; j < width; j++) {
            board[i][j] = '.';
        }
    }
}

/*
 *  print_board uses the board, height and width from BoardStr
 *  to print the board accurately. The board is a grid with BoardStr
 *  print_board makes sure it is skewly print as required
 *  @Param
 *  Takes in the pointer to BoardStr:
 *      pboardStruct
 *  @Return
 *  The function is a void, and is for correctly printing the board
 */
void print_board(BoardStr* pboardStruct) {
    int i;
    int j;
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    char** board = pboardStruct->board;
    for(i = 0; i < height; i++) {
        int spaceCounter = (height - 1) - i;
        while(spaceCounter > 0) {
            printf(" ");
            spaceCounter--;
        }
        for(j = 0; j < width; j++) {
            printf("%c", board[i][j]);
            if (j < width - 1) {
                printf(" ");
            }
        }
        printf("\n");
    }
}

/*
 *  error_controller handles the command line input originally called
 *  by the user. Insuring there are no invalid player types, the 
 *  correct nummber of arguments are used and the correct board dimensions
 *  are inputed. If an error is caught the function will print a message
 *  to stderr followed by a allocated exit status.
 *  @Param
 *  Takes in the pointer to BoardStr:
 *      pboardStruct
 *  Also takes in both:
 *      argc
 *      argv
 *  For reading command line args
 *  @Return
 *  The function is a void, and is for ensuring command line args are correct
 */
void error_controller(int argc, char** argv, BoardStr* pboardStruct, 
        char** filename) {
    int i;
    if(!(argc == 4 || argc == 5)) {
        fprintf(stderr, "Usage: bob p1type p2type "
                "[height width | filename]\n");
        exit(1);
    }
    if(argv[1][0] != 'm' && argv[1][0] != 'a') {
        fprintf(stderr, "Invalid type\n");
        exit(2);
    }
    if(argv[2][0] != 'm' && argv[2][0] != 'a') {
        fprintf(stderr, "Invalid type\n");
        exit(2);
    }
    if(strlen(argv[1]) > 1) {
        fprintf(stderr, "Invlaid type\n");
        exit(2);
    }
    if(strlen(argv[2]) > 1) {
        fprintf(stderr, "Invalid type\n");
        exit(2);
    }
    if(argc == 5) {
        for(i = 0; i < strlen(argv[3]); i++) {
            if(isdigit(argv[3][i]) == 0) {
                fprintf(stderr, "Sensible board dimensions please!\n");
                exit(3); 
            }
        }
        for(i = 0; i < strlen(argv[4]); i++) {
            if(isdigit(argv[4][i]) == 0) {
                fprintf(stderr, "Sensible board dimensions please!\n");
                exit(3);
            }
        }
        if(atoi(argv[3]) > 1000 || atoi(argv[3]) < 1) {
            fprintf(stderr, "Sensible board dimensions please!\n");
            exit(3);
        }
        if(atoi(argv[4]) > 1000 || atoi(argv[4]) < 1) {
            fprintf(stderr, "Sensible board dimensions please!\n");
            exit(3);
        }
    }
}

/*
 *  Main allocates memory to all three Structs. It then ensures the command
 *  line does not contain errors and handles the arguments accordingly.
 *  By either detecting a load_game situation where (argc == 4) or detecting
 *  the start of a new, inputed game (argc == 5)
 *  @Param
 *  Main only requires both:
 *      argc
 *      argv
 *  For reading command line args
 *  @Return
 *  The function is a void, and is for ensuring command line args are correct
 */
int main(int argc, char** argv) {
    char* filename;
    BoardStr* pboardStruct = malloc(sizeof(BoardStr));
    PlayerX* playerxP = malloc(sizeof(PlayerX));
    PlayerO* playeroP = malloc(sizeof(PlayerO));
    playeroP->moveOCounter = 0;
    playerxP->moveXCounter = 0;
    error_controller(argc, argv, pboardStruct, &filename);
    pboardStruct->currentPlayer = 'O';
    if(argc == 4) {
        FILE* fp;
        char* filename = argv[3];
        fp = fopen(filename, "r");
        if(fp == NULL) {
            fprintf(stderr, "Could not start reading from savefile\n");
            exit(4);
        }
        my_read_first_line(fp, pboardStruct, playerxP, playeroP);
        board_display(pboardStruct);
        read_board(fp, pboardStruct, playeroP, playerxP);
        if(argv[1][0] == 'm' && argv[2][0] == 'm') {
            print_board(pboardStruct);
            game_m_m(argv, pboardStruct, playerxP, playeroP);
        }
        if(argv[1][0] == 'm' && argv[2][0] == 'a') {
            print_board(pboardStruct);
            game_m_a(argv, pboardStruct, playerxP, playeroP);
        }
        if(argv[1][0] == 'a' && argv[2][0] == 'a') {
            print_board(pboardStruct);
            game_a_a(argv, pboardStruct, playerxP, playeroP);
        }
        if(argv[1][0] == 'a' && argv[2][0] == 'm') {
            print_board(pboardStruct);
            game_a_m(argv, pboardStruct, playerxP, playeroP);
        }
    }
    if(argc == 5) {
        pboardStruct->height = atoi(argv[3]);
        pboardStruct->width = atoi(argv[4]);
        board_display(pboardStruct);
    }
    argc_5_handler(pboardStruct, argv, playerxP, playeroP);
}

/*
 *  argc_5_hanler is a function that calls the correct game pending
 *  on the command line arguments. Error's for args have already beeen
 *  checked in the calling of the function.
 *  @Param
 *  Takes in the pointers to BoardStr, PlayerO & PlayerX:
 *      pboardStruct
 ^      playerxP
 *      playerOPtr
 *  The function also takes in char** from the command line
 *      argv
 *  @Return
 *  The function is a void, and is for altering the selecting the correct game
 */
void argc_5_handler(BoardStr* pboardStruct, char** argv, PlayerX* playerxP, 
        PlayerO* playeroP) {
    if(argv[1][0] == 'm' && argv[2][0] == 'm') {
        print_board(pboardStruct);
        game_m_m(argv, pboardStruct, playerxP, playeroP);
    }
    if(argv[1][0] == 'm' && argv[2][0] == 'a') {
        print_board(pboardStruct);
        game_m_a(argv, pboardStruct, playerxP, playeroP);
    }
    if(argv[1][0] == 'a' && argv[2][0] == 'a') {
        print_board(pboardStruct);
        game_a_a(argv, pboardStruct, playerxP, playeroP);
    }
    if(argv[1][0] == 'a' && argv[2][0] == 'm') {
        print_board(pboardStruct);
        game_a_m(argv, pboardStruct, playerxP, playeroP);
    }
}

/*
 *  computer_player_x simply computes the next move of computer player
 *  X. It calculates the next row and collum value based off the move
 *  counter, height, width and the relevant algorithim. It changes the
 *  value of both the row and collum within the struct PlayerX to be
 *  later called
 *  @Param
 *  Takes in the pointers to BoardStr & PlayerX:
 *      pboardStruct
 ^      playerxP

 *  @Return
 *  The function is a void, and is for altering the computers next move
 */
void computer_player_x(BoardStr* pboardStruct, PlayerX* playerxP) {

    //Setting Board Variables
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    int maxValue = max(pboardStruct);
    int tValue = ((playerxP->moveXCounter * 7 % 1000213) + 81);
    playerxP->row = (tValue / maxValue) % height;
    playerxP->collum = tValue % width;
    playerxP->moveXCounter++;
}


/*
 *  computer_player_o simply computes the next move of computer player
 *  O. It calculates the next row and collum value based off the move
 *  counter, height, width and the relevant algorithim. It changes the
 *  value of both the row and collum within the struct PlayerO to be
 *  later called
 *  @Param
 *  Takes in the pointers to BoardStr & PlayerO:
 *      pboardStruct
 ^      playerOPtr
 *  @Return
 *  The function is a void, and is for altering the computers next move
 */
void computer_player_o(BoardStr* pboardStruct, PlayerO* playeroP) {
    //Setting Board Variables
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    int maxValue = max(pboardStruct);
    int tValue = (((playeroP->moveOCounter * 9) % 1000037) + 17);
    playeroP->row = (tValue / maxValue) % height;
    playeroP->collum = tValue % width;
    playeroP->moveOCounter++;
}

/*
 *  The function max is to simply find the maximum between the height
 *  and width for computer move calculations
 *  @Param
 *  Takes in the pointer to BoardStr:
 *      pboardStruct
 *  @Return
 *  The function is a void, and is for calculating tha max of height and width
 */
int max(BoardStr* pboardStruct) {
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    if(height > width) {
        return height;
    } else {
        return width;
    }
}

/*
 *  my_read_first_line is called when a game is being loaded.
 *  This function simply reads the first line and make sure it follows
 *  the correct requiremnts. These include, containing five digits seperated
 *  by ',' followed by a '\n' after the fifth digit. Additionally a check that
 *  the first digit is 0 or 1 in order to assign it to the correct player.
 *  The function then assigns all five digits to their corresponding values
 *  of (Current Player,Height,Width,CompOmoveCount,CompXmoveCount)
 *  @Param
 *  Takes in a file pointer (FILE*):
 *      fp
 *  Takes in the pointers to BoardStr, PlayerX & PlayerO:
 *      pboardStruct
 *      playerxP
 ^      playerOPtr
 *  @Return
 *  The function is a void, and is for handling the first line of a loaded game
 */
void my_read_first_line(FILE* fp, BoardStr* pboardStruct, PlayerX* playerxP, 
        PlayerO* playeroP) {
    char* s = (char*)malloc(2048);
    int playerValue, heightValue, widthValue, playerOCounter, playerXCounter;
    char newLine;

    if (fgets(s, 1024, fp) == NULL) {
        fprintf(stderr, "Incorrect file contents\n");
        exit(5);
    }
    if(sscanf(s, "%d,%d,%d,%d,%d%c", &playerValue, &heightValue, 
            &widthValue, &playerOCounter, &playerXCounter, &newLine) != 6) {
        fprintf(stderr, "Incorrect file contents\n");
        exit(5);
    }
    if(newLine != '\n') {
        fprintf(stderr, "Incorrect file contents\n");
        exit(5);
    }
    if(playerValue != 0 && playerValue != 1) {
        fprintf(stderr, "Incorrect file contents\n");
        exit(5);
    }
    if(playerValue == 0) {
        pboardStruct->currentPlayer = 'O';
    } else {
        pboardStruct->currentPlayer = 'X';
    }
    pboardStruct->height = heightValue;
    pboardStruct->width = widthValue;
    playeroP->moveOCounter = playerOCounter;
    playerxP->moveXCounter = playerXCounter;
}


/*
 *  read_board is called when a game is being loaded.
 *  This function simply reads all of the lines after the first ensuring
 *  the correct requiremnts. These include, the board height and width
 *  are the same as the values loaded on the first line. Additionally that
 *  the moveCounter's for each computer players are accurately represented by
 *  the amount of pieces on the board. Finally, ensuring that there are no
 *  other characters except '.', 'X' & 'O' represented on the board. Once
 *  checked the function will allocated the board to the board within BoardStr
 *  @Param
 *  Takes in a file pointer (FILE*):
 *      fp
 *  Takes in the pointers to BoardStr, PlayerX & PlayerO:
 *      pboardStruct
 *      playerxP
 ^      playerOPtr
 *  @Return
 *  The function is a void, and is for handling lines after the first
 */
void read_board(FILE* fileP, BoardStr* pboardStruct, 
        PlayerO* playeroP, PlayerX* playerxP) {
    int i;
    int j;
    int k = 0;
    char* fileString = (char*)malloc(2048);
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    while(fgets(fileString, 2048, fileP)) {
        if(strlen(fileString) - 1 != width) {
            fprintf(stderr, "Incorrect file contents\n");
            exit(5);
        }
        strncpy(pboardStruct->board[k], fileString, strlen(fileString) - 1);
        k++;
    }
    if(height != k) {
        fprintf(stderr, "Incorrect file contents\n");
        exit(5);
    }
    char** board = pboardStruct->board;
    int countX = 0;
    int countO = 0;
    for(i = 0; i < height; i++) {
        for(j = 0; j < width; j++) {
            if(board[i][j] != '.' && board[i][j] != 'X' && board[i][j] 
                    != 'O') {
                fprintf(stderr, "Incorrect file contents\n");
                exit(5);
            }
            if(board[i][j] == 'X') {
                countX++;
            }
            if(board[i][j] == 'O') {
                countO++;
            }
        }
    }
    if(playeroP->moveOCounter > 0 && playeroP->moveOCounter 
            != countO) {
        fprintf(stderr, "Incorrect file contents\n");
        exit(5);
    }
    if(playerxP->moveXCounter > 0 && playerxP->moveXCounter != countX) {
        fprintf(stderr, "Incorrect file contents\n");
        exit(5);
    }
}

/*
 *  The check_for_win function itterates through the top checking 
 *  for the character 'X'. It also itterates through the left hand
 *  side of the board checking for the character 'O'. Once found in
 *  either situation it will check the next two possible positions towards 
 *  the centre of the board. If a similar character is found it will then
 *  call a recursive check. Some safety statements are called before hane.
 *  E.g. if the with==1 player O will win and if the inner section is found
 *  and the height or width is only 2, the corresponding player will win.
 *  @Param
 *  Takes in the pointer to BoardStr:
 *      pboardStruct
 *  @Return
 *  The function is a void, and is for checking if a win situation is possible
 */
void check_for_win(BoardStr* pboardStruct) {
    int i;
    int j;
    CheckPos* pcheckPos = malloc(sizeof(CheckPos));
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    char** board = pboardStruct->board;
    for(i = 0; i < height; i++) {
        for(j = 0; j < width; j++) {
            if(board[i][j] == '.') {
                continue;
            }
            if(width == 1) {
                printf("Player O wins\n");
                exit(0);
            }
            if(board[0][j] == 'X') {
                visited_board(pcheckPos, pboardStruct);
                pcheckPos->visitedBoard[0][j] = '1';
                pcheckPos->playerCharacter = 'X';
                //Check bellow
                check_for_win_x_below(pboardStruct, pcheckPos, i, 
                        j, board, height);
                if(j + 1 > width - 1) {
                    continue;
                }
                //Check below right
                check_for_win_x_below_right(pboardStruct, pcheckPos, i, 
                        j, board, height);
            }        
            if(board[i][0] == 'O') {
                visited_board(pcheckPos, pboardStruct);
                pcheckPos->visitedBoard[i][0] = '1';
                pcheckPos->playerCharacter = 'O';
                //Check right of point found on LHS
                check_for_win_o_right(pboardStruct, pcheckPos, i, j, 
                        board, width);
                if(i + 1 > height - 1) {
                    continue;
                }
                //Check below right of point found on LHS
                check_for_win_o_below_right(pboardStruct, pcheckPos, i, 
                        j, board, width);
            }
        }
    }
}

/*
 *  check_for_win_x_below is a small function which will check
 *  if the value below the previous selected top row and collum
 *  is an X. It will reallocate CheckPos row and collum and begin recursion
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void check_for_win_x_below(BoardStr* pboardStruct, CheckPos* pcheckPos, 
        int i, int j, char** board, int height) {
    if(board[1][j] == pcheckPos->playerCharacter) {
        if(height == 2) {
            printf("Player X wins\n");
            exit(0);
        }
        pcheckPos->visitedBoard[1][j] = '1';
        pcheckPos->row = 1;
        pcheckPos->collum = j;
        recursive_check_x(pboardStruct, pcheckPos);
    }
}

/*
 *  check_for_win_x_below_right is a small function which will check
 *  if the value below right the previous selected top row and collum
 *  is an X. It will reallocate CheckPos row and collum and begin recursion
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void check_for_win_x_below_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int i, int j, char** board, int height) {
    if(board[1][j + 1] == pcheckPos->playerCharacter) {
        if(height == 2) {
            printf("Player X wins\n");
            exit(0);
        }
        pcheckPos->visitedBoard[1][j + 1] = '1';
        pcheckPos->row = 1;
        pcheckPos->collum = j + 1;
        recursive_check_x(pboardStruct, pcheckPos);
    }
}

/*
 *  check_for_win_o_right is a small function which will check
 *  if the value to the right the previous selected row and left collum
 *  is an O. It will reallocate CheckPos row and collum and begin recursion
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void check_for_win_o_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int i, int j, char** board, int width) {
    if(board[i][1] == pcheckPos->playerCharacter) {
        if(width == 2) {
            printf("Player O wins\n");
            exit(0);
        }
        pcheckPos->visitedBoard[i][1] = '1';
        pcheckPos->row = i;
        pcheckPos->collum = 1;
        recursive_check_o(pboardStruct, pcheckPos);
    }
}

/*
 *  check_for_win_o_below_right is a small function which will check
 *  if the value to the right below the previous selected row and left collum
 *  is an O. It will reallocate CheckPos row and collum and begin recursion
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void check_for_win_o_below_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int i, int j, char** board, int width) {
    if(board[i + 1][1] == pcheckPos->playerCharacter) {
        if(width == 2) {
            printf("Player O wins\n");
            exit(0);
        }
        pcheckPos->visitedBoard[i + 1][1] = '1';
        pcheckPos->row = i + 1;
        pcheckPos->collum = 1;
        recursive_check_o(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_x will check all possible 6 situations 
 *  (Top, Top Left, Left, Bottom, Bottom Right, Right)
 *  if a spot is found and it is equal to 'X' the function will change
 *  the values of CheckPosStr row and collum to equal the new position
 *  and mark the value as '1' on the visited_board, to know where we 
 *  have been. It will continue this until a position on the bottom of 
 *  the board is found.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_x(BoardStr* pboardStruct, CheckPos* pcheckPos) {
    char** board = pboardStruct->board;
    char** visitedBoard = pcheckPos->visitedBoard;
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    //Checking below
    recursive_check_x_below(pboardStruct, pcheckPos, height, 
            width, board, visitedBoard);
    //Checking below right
    recursive_check_x_below_right(pboardStruct, pcheckPos, 
            height, width, board, visitedBoard);
    //Checking LHS
    recursive_check_x_left(pboardStruct, pcheckPos, height, 
            width, board, visitedBoard);
    //Checking top left
    recursive_check_x_top_left(pboardStruct, pcheckPos, height, 
            width, board, visitedBoard);
    //Check Top
    recursive_check_x_top(pboardStruct, pcheckPos, height, 
            width, board, visitedBoard);
    //Check RHS
    recursive_check_x_right(pboardStruct, pcheckPos, height, 
            width, board, visitedBoard);
}

/*
 *  recursive_check_x_below is a small function which will check
 *  if the value below the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_x_below(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard) {
    if(pcheckPos->row + 1 < height && 
            board[pcheckPos->row + 1][pcheckPos->collum] 
            == pcheckPos->playerCharacter && 
            visitedBoard[pcheckPos->row + 1][pcheckPos->collum] 
            == '0') {
        if(pcheckPos->row + 1 == height - 1) {
            printf("Player X wins\n");
            exit(0);
        }
        visitedBoard[pcheckPos->row + 1][pcheckPos->collum]
                = '1';
        pcheckPos->row = pcheckPos->row + 1;
        recursive_check_x(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_x_below_right is a small function which will check
 *  if the value below right the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_x_below_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard) {
    if(pcheckPos->collum + 1 < width && 
            pcheckPos->row + 1 < height && 
            board[pcheckPos->row + 1][pcheckPos->collum + 1] 
            == pcheckPos->playerCharacter && 
            visitedBoard[pcheckPos->row + 1][pcheckPos->collum + 1] 
            == '0') {
        if(pcheckPos->row + 1 == height - 1) {
            printf("Player X wins\n");
            exit(0);
        }
        visitedBoard[pcheckPos->row + 1][pcheckPos->collum + 1] 
                = '1';
        pcheckPos->row = pcheckPos->row + 1;
        pcheckPos->collum = pcheckPos->collum + 1;
        recursive_check_x(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_x_left is a small function which will check
 *  if the value left of the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_x_left(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard) {
    if(pcheckPos->collum - 1 >= 0 && 
            board[pcheckPos->row][pcheckPos->collum - 1] 
            == pcheckPos->playerCharacter && 
            visitedBoard[pcheckPos->row][pcheckPos->collum - 1] 
            == '0') {
        visitedBoard[pcheckPos->row][pcheckPos->collum - 1] = '1';
        pcheckPos->collum = pcheckPos->collum - 1;
        recursive_check_x(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_x_top_left is a small function which will check
 *  if the value toop left of the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_x_top_left(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard) {
    if(pcheckPos->collum - 1 >= 0 && pcheckPos->row - 1 >= 0 && 
            board[pcheckPos->row - 1][pcheckPos->collum - 1] 
            == pcheckPos->playerCharacter && 
            visitedBoard[pcheckPos->row - 1][pcheckPos->collum - 1]
            == '0') {
        visitedBoard[pcheckPos->row - 1][pcheckPos->collum - 1]
                = '1';
        pcheckPos->row = pcheckPos->row - 1;
        pcheckPos->collum = pcheckPos->collum - 1;
        recursive_check_x(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_x_top is a small function which will check
 *  if the value on top of the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_x_top(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard) {
    if(pcheckPos->row - 1 >= 0 && 
            board[pcheckPos->row - 1][pcheckPos->collum] 
            == pcheckPos->playerCharacter
            && visitedBoard[pcheckPos->row - 1][pcheckPos->collum] 
            == '0') {
        visitedBoard[pcheckPos->row - 1][pcheckPos->collum] = '1';
        pcheckPos->row = pcheckPos->row - 1;
        recursive_check_x(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_x_right is a small function which will check
 *  if the value to the right of the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_x_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard) {
    if(pcheckPos->collum + 1 < width && 
            board[pcheckPos->row][pcheckPos->collum + 1] 
            == pcheckPos->playerCharacter
            && visitedBoard[pcheckPos->row][pcheckPos->collum + 1] 
            == '0') {
        visitedBoard[pcheckPos->row][pcheckPos->collum + 1] = '1';
        pcheckPos->collum = pcheckPos->collum + 1;
        recursive_check_x(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_o will check all possible 6 situations 
 *  (Top, Top Left, Left, Bottom, Bottom Right, Right)
 *  if a spot is found and it is equal to 'O' the function will change
 *  the values of CheckPos row and collum to equal the new position
 *  and mark the value as '1' on the visited_board, to know where we 
 *  have been. It will continue this until a position on the bottom of 
 *  the board is found.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_o(BoardStr* pboardStruct, CheckPos* pcheckPos) {
    char** board = pboardStruct->board;
    char** visitedBoard = pcheckPos->visitedBoard;
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    //Checking below
    recursive_check_o_below(pboardStruct, pcheckPos, height, width, 
            board, visitedBoard);
    //Checking below right
    recursive_check_o_below_right(pboardStruct, pcheckPos, height, width, 
            board, visitedBoard);
    //Checking LHS
    recursive_check_o_left(pboardStruct, pcheckPos, height, width, board, 
            visitedBoard);
    //Checking top left
    recursive_check_o_top_left(pboardStruct, pcheckPos, height, width, 
            board, visitedBoard);
    //Check Top
    recursive_check_o_top(pboardStruct, pcheckPos, height, width, board, 
            visitedBoard);
    //Check RHS
    recursive_check_o_right(pboardStruct, pcheckPos, height, width, 
            board, visitedBoard);
}

/*
 *  recursive_check_o_below is a small function which will check
 *  if the value below the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_o_below(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard) {
    if(pcheckPos->row + 1 < height && 
            board[pcheckPos->row + 1][pcheckPos->collum] 
            == pcheckPos->playerCharacter
            && visitedBoard[pcheckPos->row + 1][pcheckPos->collum] 
            == '0') {
        visitedBoard[pcheckPos->row + 1][pcheckPos->collum] = '1';
        pcheckPos->row = pcheckPos->row + 1;
        recursive_check_o(pboardStruct, pcheckPos);
    } 
}

/*
 *  recursive_check_o_below_right is a small function which will check
 *  if the value below right the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_o_below_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard) {
    if(pcheckPos->row + 1 < height && pcheckPos->collum + 1 < width
            && board[pcheckPos->row + 1][pcheckPos->collum + 1] 
            == pcheckPos->playerCharacter && 
            visitedBoard[pcheckPos->row + 1][pcheckPos->collum + 1] 
            == '0') {
        if(pcheckPos->collum + 1 == width - 1) {
            printf("Player O wins\n");
            exit(0);
        }
        visitedBoard[pcheckPos->row + 1][pcheckPos->collum + 1] 
                = '1';
        pcheckPos->row = pcheckPos->row + 1;
        pcheckPos->collum = pcheckPos->collum + 1;
        recursive_check_o(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_o_left is a small function which will check
 *  if the value left of the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_o_left(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard) {
    if(pcheckPos->collum - 1 >= 0 && 
            board[pcheckPos->row][pcheckPos->collum - 1] 
            == pcheckPos->playerCharacter
            && visitedBoard[pcheckPos->row][pcheckPos->collum - 1] 
            == '0') {
        visitedBoard[pcheckPos->row][pcheckPos->collum - 1] = '1';
        pcheckPos->collum = pcheckPos->collum - 1;
        recursive_check_o(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_o_top_left is a small function which will check
 *  if the value toop left of the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_o_top_left(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, char** board, 
        char** visitedBoard) {
    if(pcheckPos->collum - 1 >= 0 && pcheckPos->row - 1 >= 0 && 
            board[pcheckPos->row - 1][pcheckPos->collum - 1] 
            == pcheckPos->playerCharacter && 
            visitedBoard[pcheckPos->row - 1][pcheckPos->collum - 1]
            == '0') {
        visitedBoard[pcheckPos->row - 1][pcheckPos->collum - 1] 
                = '1';
        pcheckPos->row = pcheckPos->row - 1;
        pcheckPos->collum = pcheckPos->collum - 1;
        recursive_check_o(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_x_top is a small function which will check
 *  if the value on top of the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_o_top(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, 
        char** board, char** visitedBoard) {
    if(pcheckPos->row - 1 >= 0 && 
            board[pcheckPos->row - 1][pcheckPos->collum] 
            == pcheckPos->playerCharacter
            && visitedBoard[pcheckPos->row - 1][pcheckPos->collum] 
            == '0') {
        visitedBoard[pcheckPos->row - 1][pcheckPos->collum] = '1';
        pcheckPos->row = pcheckPos->row - 1;
        recursive_check_o(pboardStruct, pcheckPos);
    }
}

/*
 *  recursive_check_x_right is a small function which will check
 *  if the value to the right of the previous selected CheckPos row and collum
 *  is an X and has not been visited. If so, continue recursing.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  Also takes in direct variables of:
 *      height
 *      width
 *      board
 *      visitedBoard
 *  @Return
 *  The function is a void, and is for recursively looking for a win
 */
void recursive_check_o_right(BoardStr* pboardStruct, 
        CheckPos* pcheckPos, int height, int width, 
        char** board, char** visitedBoard) {
    if(pcheckPos->collum + 1 < width && 
            board[pcheckPos->row][pcheckPos->collum + 1] 
            == pcheckPos->playerCharacter
            && visitedBoard[pcheckPos->row][pcheckPos->collum + 1] 
            == '0') {
        if(pcheckPos->collum + 1 == width - 1) {
            printf("Player O wins\n");
            exit(0);
        }
        visitedBoard[pcheckPos->row][pcheckPos->collum + 1] = '1';
        pcheckPos->collum = pcheckPos->collum + 1;
        recursive_check_o(pboardStruct, pcheckPos);
    }
}

/*
 *  visisted_board is a hypothetical board which creates a new list of arrays
 *  similar to the board, except these values are filled with '0'. 
 *  This board is created in order to map the path, when the check for win
 *  is occuring.
 *  @Param
 *  Takes in the pointers to BoardStr, CheckPos:
 *      pboardStruct
 *      pcheckPos
 *  @Return
 *  The function is a void, and creates a board for mapping
 */
void visited_board(CheckPos* pcheckPos, BoardStr* pboardStruct) {
    int i;
    int j;
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    char** board;
    pcheckPos->visitedBoard = (char**)malloc(sizeof(char*) * height);
    board = pcheckPos->visitedBoard;
    for(i = 0; i < height; i++) {
        board[i] = (char*)malloc(sizeof(char) * (width));
    }
    for(i = 0; i < height; i++) {
        for(j = 0; j < width; j++) {
            board[i][j] = '0';
        }
    }
}

/*
 *  save_game is a function that opens a new file, which is allocated
 *  by the user. It then truncates all of the file to 0, and writes
 *  the correct first line, followed by the board for the rest of the file.
 *  @Param
 *  Takes in a string (char*)
 *      filename
 *  Takes in the pointers to BoardStr, PlayerX, PlayerO:
 *      pboardStruct
 *      playerxP
 *      playerOPtr
 *  @Return
 *  The function is a void, and is for writing a saved game to a new file
 */
void save_game(char* filename, BoardStr* pboardStruct, 
        PlayerX* playerxP, PlayerO* playeroP) {
    int i;
    int playerInt;
    FILE* fileP;
    int height = pboardStruct->height;
    int width = pboardStruct->width;
    char** board = pboardStruct->board;
    char currentPlayer = pboardStruct->currentPlayer;
    int moveOCounter = playeroP->moveOCounter;
    int moveXCounter = playerxP->moveXCounter;
    if(currentPlayer == 'X') {
        playerInt = 1;
    } else {
        playerInt = 0;
    }
    fileP = fopen(filename, "w");
    fprintf(fileP, "%d,%d,%d,%d,%d\n", playerInt, height, width, 
            moveOCounter, moveXCounter);
    for(i = 0; i < height; i++) {
        fprintf(fileP, "%s\n", board[i]);
    }
    fclose(fileP);
}


