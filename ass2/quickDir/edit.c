#include <stdio.h>                                                    /* QS: */
#include <stdbool.h>                                                  /* QS: */
#include <string.h>                                                   /* QS: */
#include <stdlib.h>                                                   /* QS: */
#include "extra.h"                                                    /* QS: */
                                                                      /* QS: */
                                                                      /* QS: */
                                                                      /* QS: */
                                                                      /* QS: */
#define BLANKCHAR '.'                                                 /* QS: */
                                                                      /* QS: */
typedef struct {                                                      /* QS: */
    int mcount; // number of the next move to generate                /* QS: */
    int mult;   // factors used in generation                         /* QS: */
    int mod;    // see spec for individual meanings                   /* QS: */
    int add;                                                          /* QS: */
    int height; // grid dimensions                                    /* QS: */
    int width;                                                        /* QS: */
} Automove;                                                           /* QS: */
                                                                      /* QS: */
typedef struct Stack {                                                /* QS: */
    int* values;                                                      /* QS: */
    unsigned int cap;   // values which can be stored without resizing/* QS: */
    unsigned int size;  // values currently stored                    /* QS: */
} Stack;                                                              /* QS: */
                                                                      /* QS: */
typedef struct {                                                      /* QS: */
    int height; // grid dimensions                                    /* QS: */
    int width;                                                        /* QS: */
    char** cells;   // contents of cells                              /* QS: */
    Stack* stack;   // stack for searching                            /* QS: */
    Stack* markstack;   // stack of cells to be fixed later           /* QS: */
} Grid;                                                               /* QS: */
                                                                      /* QS: */
typedef struct {                                                      /* QS: */
    Grid board;                                                       /* QS: */
    Automove movers[2]; // auto-players                               /* QS: */
    bool playerType[2]; // true for manual player                     /* QS: */
    char playSym[2];  // symbol used to show player on board          /* QS: */
    int cPlayer;    // current player                                 /* QS: */
    char winner;    // character of winning player                    /* QS: */
} Game;                                                               /* QS: */
                                                                      /* QS: */
// Initialise Game struct for a new game                              /* QS: */
void init_game(Game* g, bool pomanual, bool pxmanual,                 /* QS: */
    int height, int width);                                           /* QS: */
                                                                      /* QS: */
// Release memory held by Game (does not free game itself)            /* QS: */
void clean_game(Game* g);                                             /* QS: */
                                                                      /* QS: */
// Get the next move from the Automove, does not check for validity   /* QS: */
void read_move(Automove* m, int* row, int* column);                   /* QS: */
                                                                      /* QS: */
                                                                      /* QS: */
                                                                      /* QS: */
                                                                      /* QS: */
// initialise empty grid with specified dimensions                    /* QS: */
void init_grid(Grid* g, int height, int width);                       /* QS: */
                                                                      /* QS: */
// cleanup memory held by g (does not free g itself)                  /* QS: */
void clean_grid(Grid* g);                                             /* QS: */
                                                                      /* QS: */
// Output grid to file.                                               /* QS: */
// If indent is true, cells will be spaced for player display.        /* QS: */
// If indent is false, do not indent display (used for saving).       /* QS: */
void print_grid(Grid* g, FILE* f, bool indent);                       /* QS: */
                                                                      /* QS: */
// r and c indicate the cell to start checking                        /* QS: */
// leftright is true checking for O left to right and                 /* QS: */
// false to check X top to bottom                                     /* QS: */
bool sides_connected(Grid* g, bool leftright, int r, int c);          /* QS: */
                                                                      /* QS: */
typedef enum {                                                        /* QS: */
    OK = 0,                                                      /* QS:a1b1a */
    BADARGC = 1,                                                 /* QS:a1i3v */
    BADPTYPE = 2,                                                 /* QS:a2p5q */
    BADDIM = 3,                                                  /* QS:a3w7l */
    BADREADOPEN = 4,                                             /* QS:a4d0h */
    BADREADCONT = 5,                                             /* QS:a5k2c */
    USEREOF = 6,                                                 /* QS:a5r4x */
    GAMEOVER = 20                                                /* QS:a6y6s */
} Result;                                                             /* QS: */
                                                                      /* QS: */
// Get and apply the next valid move                                  /* QS: */
// will return GAMEOVER, USEREOF or OK (if no other issues)           /* QS: */
Result next_move(Game* g);                                            /* QS: */
                                                                      /* QS: */
// Overall driver for game. Return OK if game finished normally       /* QS: */
Result play_game(Game* g);                                            /* QS: */
                                                                      /* QS: */
// Reads previously saved game state into g.                          /* QS: */
// The boolean parameters indicate whether player O/X will be         /* QS: */
// manually controlled this time                                      /* QS: */
Result init_game_from_file(Game* g, const char* name,                 /* QS: */
        bool pomanual, bool pxmanual);                                /* QS: */
// output human readable message for the result code                  /* QS: */
// return the passed in value to allow chaining of ops                /* QS: */
Result print_res_message(Result r, FILE* f);                          /* QS: */
                                                                      /* QS: */
// Allocate empty stack                                               /* QS: */
Stack* alloc_stack(void);                                             /* QS: */
                                                                      /* QS: */
// Deallocate stack                                                   /* QS: */
void free_stack(Stack* s);                                            /* QS: */
                                                                      /* QS: */
// push pair of values onto stack                                     /* QS: */
void push(Stack* s, int r, int c);                                    /* QS: */
                                                                      /* QS: */
// read and pop pair of values                                        /* QS: */
bool pop(Stack* s, int* r, int* c);                                   /* QS: */
                                                                      /* QS: */
// Initialise Game struct for a new game                              /* QS: */
void init_game(Game* g, bool pomanual,                                /* QS: */
        bool pxmanual, int height, int width) {                       /* QS: */
    init_grid(&g->board, height, width);                              /* QS: */
    g->playerType[0] = pomanual;                                 /* QS:a7f9n */
    g->playerType[1] = pxmanual;                                 /* QS:a8m1j */
    g->movers[0].mcount = 0;                                     /* QS:a9t3e */
    g->movers[0].mult = 9;                                       /* QS:a9a6z */
    g->movers[0].mod = 1000037;                                  /* QS:b0h8u */
    g->movers[0].add = 17;                                       /* QS:b1o0q */
    g->movers[0].height = height;                                /* QS:b2v2l */
    g->movers[0].width = width;                                  /* QS:b3c5g */
    g->movers[1].mcount = 0;                                     /* QS:b4j7b */
    g->movers[1].mult = 7;                                       /* QS:b4q9w */
   
    g->movers[1].mod = 1000231;                                  /* QS:b5x1s */
    g->movers[1].add = 81;                                       /* QS:b6e4n */
    printf("Add: %d\n", g->movers[1].add);
    g->movers[1].height = height;                                /* QS:b7l6i */
    g->movers[1].width = width;                                  /* QS:b8s8d */
    g->cPlayer = 0;                                                   /* QS: */
    g->playSym[0] = 'O';                                         /* QS:b8z0z */
    g->playSym[1] = 'X';                                         /* QS:b9g3u */
    g->winner = -1;                                                   /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// Release memory held by Game (does not free game itself)            /* QS: */
void clean_game(Game* g) {                                            /* QS: */
    clean_grid(&g->board);                                            /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// Get the next move from the Automove, does not check for validity   /* QS: */
void read_move(Automove* move, int* row, int* column) {               /* QS: */
    int m = move->height > move->width ? move->height : move->width;  /* QS: */
    int t = (move->mcount * move->mult + move->add) % move->mod; /* QS:c0n5p */
    *row = t / m % move->height;                                 /* QS:c1u7k */
    *column = t % move->width;                                   /* QS:c2b0g */
    move->mcount++;                                                   /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// read up to maxc chars (stop on \n but do not store it)             /* QS: */
// If maxc chars have been read but no \n, then gobble chars until    /* QS: */
// one is found.                                                      /* QS: */
// Warning: buffer must be at least [maxc+1] to allow for \0          /* QS: */
// return false if eof and no input stored                            /* QS: */
bool grab_line(FILE* f, char* buffer, int maxc) {                     /* QS: */
    int i;                                                            /* QS: */
    int c;                                                            /* QS: */
    for (i = 0; i < maxc; ++i) {                                      /* QS: */
        c = fgetc(f);                                                 /* QS: */
        if (c == EOF) {                                               /* QS: */
            buffer[i] = '\0';                                         /* QS: */
            return i > 0;                                             /* QS: */
        }                                                             /* QS: */
        if (c == '\n') {                                              /* QS: */
            break;                                                    /* QS: */
        }                                                             /* QS: */
        buffer[i] = c;                                           /* QS:c3i2b */
    }                                                                 /* QS: */
    buffer[i] = '\0';                                                 /* QS: */
    while ((c != '\n') && (c != EOF)) {                               /* QS: */
        c = fgetc(f);                                                 /* QS: */
    }                                                                 /* QS: */
    return true;                                                      /* QS: */
                                                                      /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// Reads previously saved game state into g.                          /* QS: */
// The boolean parameters indicate whether player O/X will be         /* QS: */
// manually controlled this time                                      /* QS: */
Result init_game_from_file(Game* g, const char* name, bool pomanual,  /* QS: */
        bool pxmanual) {                                              /* QS: */
    FILE* f = fopen(name, "r");                                  /* QS:c3p4w */
    if (!f) {                                                    /* QS:c4w6r */
        return BADREADOPEN;                                      /* QS:c5d9m */
    }                                                                 /* QS: */
    char buff[71];                                                    /* QS: */
    if (!grab_line(f, buff, 70)) {                                    /* QS: */
        return BADREADCONT;                                           /* QS: */
    }                                                                 /* QS: */
    int h, w, m0, m1;                                                 /* QS: */
    char t;                                                           /* QS: */
    char dummy;                                                       /* QS: */
    if (sscanf(buff, "%d,%d,%c,%d,%d%c", &h, &w, &t,             /* QS:c6k1i */
            &m0, &m1, &dummy) != 5) {                                 /* QS: */
        return BADREADCONT;                                      /* QS:c7r3d */
    }                                                                 /* QS: */
    if ((h < 1) || (h > 1000) || (w < 1) || (w > 1000) ||        /* QS:c7y5y */
            (t != '1' && t != '0') || (m0 < 0) || (m1 < 0)) {    /* QS:c8f8t */
        return BADREADCONT;                                           /* QS: */
    }                                                                 /* QS: */
    init_game(g, pomanual, pxmanual, h, w);                           /* QS: */
    g->cPlayer = (t == '0' ? 0 : 1);                                  /* QS: */
    int in;                                                           /* QS: */
    for (int i = 0; i < h; ++i) {                                     /* QS: */
        for (int j = 0; j < w; ++j) {                                 /* QS: */
            in = fgetc(f);                                            /* QS: */
            if ((in != g->playSym[0]) && (in != g->playSym[1])   /* QS:c9m0p */
                    && (in != BLANKCHAR)) {                      /* QS:d0t2k */
                return BADREADCONT;     // will catch EOF as well     /* QS: */
            }                                                         /* QS: */
            g->board.cells[i][j] = (char)in;                     /* QS:d1a5f */
        }                                                             /* QS: */
        in = fgetc(f);                                                /* QS: */
        if (in != '\n') {                                             /* QS: */
            return BADREADCONT;                                       /* QS: */
        }                                                             /* QS: */
    }                                                                 /* QS: */
    if (fgetc(f) != EOF) {                                            /* QS: */
        return BADREADCONT;                                           /* QS: */
    }                                                                 /* QS: */
    return OK;                                                        /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// Record game state to file                                          /* QS: */
void save_game(FILE* f, Game* g) {                                    /* QS: */
    fprintf(f,"%d,%d,%c,%d,%d\n",g->board.height,g->board.width, /* QS:d2h7a */
            (g->cPlayer ? '1' : '0'),                                 /* QS: */
            g->movers[0].mcount, g->movers[1].mcount);                /* QS: */
    print_grid(&g->board, f, false);                                  /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// Get and apply the next valid move                                  /* QS: */
// will return GAMEOVER, USEREOF or OK (if no other issues)           /* QS: */
Result next_move(Game* g) {                                           /* QS: */
    int r = 0, c = 0;                                                 /* QS: */
    if (!g->playerType[g->cPlayer]) {   // auto player                /* QS: */
        while (read_move(&g->movers[g->cPlayer], &r, &c),             /* QS: */
                g->board.cells[r][c] != BLANKCHAR) {             /* QS:d2o9v */
        }                                                             /* QS: */
        g->board.cells[r][c] = g->playSym[g->cPlayer];                /* QS: */
        printf("Player %c => %d %d\n",                           /* QS:d3v1r */
               g->playSym[g->cPlayer], r, c);                         /* QS: */
    } else {                                                          /* QS: */
        char buffer[71];                                              /* QS: */
        do {                                                          /* QS: */
            printf("Player %c] ", g->playSym[g->cPlayer]);       /* QS:d4c4m */
            if (!grab_line(stdin, buffer, 70)) {                      /* QS: */
                return USEREOF;                                       /* QS: */
            }                                                         /* QS: */
            if ((strlen(buffer) > 1) && (buffer[0] == 'w')) {         /* QS: */
                FILE* w = fopen(buffer + 1, "w");                     /* QS: */
                if (!w) {                                             /* QS: */
                    fprintf(stderr, "Unable to save game\n");    /* QS:d5j6h */
                    continue;                                         /* QS: */
                }                                                     /* QS: */
                save_game(w, g);                                      /* QS: */
                fclose(w);                                            /* QS: */
                continue;                                             /* QS: */
            }                                                         /* QS: */
            char b1, b2;                                              /* QS: */
            if (sscanf(buffer, "%d%c%d%c", &r, &b1, &c, &b2) != 3) {  /* QS: */
                continue;                                             /* QS: */
            }                                                         /* QS: */
            if ((b1 != ' ') || (r < 0) || (r >= g->board.height) /* QS:d6q8c */
                    || (c < 0) || (c >= g->board.width)) {       /* QS:d6x0y */
                continue;                                             /* QS: */
            }                                                         /* QS: */
            if (g->board.cells[r][c] == BLANKCHAR) {             /* QS:d7e3t */
                break;                                                /* QS: */
            }                                                         /* QS: */
        } while (true);                                               /* QS: */
        g->board.cells[r][c] = g->playSym[g->cPlayer];           /* QS:d8l5o */
    }                                                                 /* QS: */
    print_grid(&g->board, stdout, true);                              /* QS: */
    // now check for gameover                                         /* QS: */
    if (sides_connected(&g->board, g->cPlayer == 0, r, c)) {          /* QS: */
        g->winner = g->cPlayer;                                       /* QS: */
        return GAMEOVER;                                         /* QS:d9s7j */
    }                                                                 /* QS: */
    return OK;                                                        /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// Overall driver for game. Return OK if game finished normally       /* QS: */
Result play_game(Game* g) {                                           /* QS: */
    Result r;                                                         /* QS: */
    print_grid(&g->board, stdout, true);                         /* QS:e0z9e */
    while (r = next_move(g), (r == OK)) {                             /* QS: */
        g->cPlayer = (g->cPlayer + 1) % 2;                            /* QS: */
    }                                                                 /* QS: */
    return r;                                                         /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// initialise empty grid with specified dimensions                    /* QS: */
void init_grid(Grid* g, int height, int width) {                      /* QS: */
    g->height = height;                                               /* QS: */
    g->width = width;                                                 /* QS: */
    g->cells = malloc(sizeof(char*) * height);                        /* QS: */
    for (int i = 0; i < height; ++i) {                                /* QS: */
        g->cells[i] = malloc(sizeof(char) * width);              /* QS:e1g2a */
        memset(g->cells[i], '.', width);                         /* QS:e1n4v */
    }                                                                 /* QS: */
    g->stack = alloc_stack();                                         /* QS: */
    g->markstack = alloc_stack();                                     /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// cleanup memory held by g (does not free g itself)                  /* QS: */
void clean_grid(Grid* g) {                                            /* QS: */
    for (int i = 0; i < g->height; ++i) {                             /* QS: */
        free(g->cells[i]);                                            /* QS: */
    }                                                                 /* QS: */
    free(g->cells);                                                   /* QS: */
    free_stack(g->stack);                                             /* QS: */
    free_stack(g->markstack);                                         /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// Output grid to file.                                               /* QS: */
// If indent is true, cells will be spaced for player display.        /* QS: */
// If indent is false, do not indent display (used for saving).       /* QS: */
void print_grid(Grid* g, FILE* f, bool indent) {                      /* QS: */
    for (int i = 0; i < g->height; ++i) {                             /* QS: */
        if (indent) {                                                 /* QS: */
            for (int j = 0; j < g->height - i - 1; ++j) {        /* QS:e2u6q */
                fputc(' ', f);                                        /* QS: */
            }                                                         /* QS: */
        }                                                             /* QS: */
        for (int j = 0; j < g->width; ++j) {                     /* QS:e3b9l */
            fputc(g->cells[i][j], f);                                 /* QS: */
            if (indent) {                                             /* QS: */
                fputc(' ', f);                                        /* QS: */
            }                                                         /* QS: */
        }                                                             /* QS: */
        fputc('\n', f);                                               /* QS: */
    }                                                                 /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// If the specified cell is in bounds and contains <target>           /* QS: */
// then put <mark> in the cell and add it to both stacks              /* QS: */
void try_add(Grid* g, int r, int c, char target, char mark) {         /* QS: */
    if ((r >= 0) && (r < g->height) && (c >= 0) && (c < g->width)) {  /* QS: */
        if (g->cells[r][c] == target) {                               /* QS: */
            g->cells[r][c] = mark;                               /* QS:e4i1h */
            push(g->stack, r, c);                                     /* QS: */
            push(g->markstack, r, c);                                 /* QS: */
        }                                                             /* QS: */
    }                                                                 /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// r and c indicate the cell to start checking                        /* QS: */
// leftright is true checking for O left to right and                 /* QS: */
// false to check X top to bottom                                     /* QS: */
bool sides_connected(Grid* g, bool leftright, int r, int c) {         /* QS: */
    bool sides[4] = {false, false, false, false};     // NSEW         /* QS: */
    char search = leftright ? 'O' : 'X';                              /* QS: */
    char mark = leftright ? 'o' : 'x';                                /* QS: */
    if (g->cells[r][c] != search) {                                   /* QS: */
        return false;                                                 /* QS: */
    }                                                                 /* QS: */
    g->cells[r][c] = mark;                                            /* QS: */
    push(g->markstack, r, c);                                         /* QS: */
    bool result = false;                                              /* QS: */
    push(g->stack, r, c);                                             /* QS: */
    while (pop(g->stack, &r, &c)) {     // Add legal neighbours       /* QS: */
        try_add(g, r - 1, c - 1, search, mark);                  /* QS:e5p3c */
        try_add(g, r - 1, c, search, mark);                      /* QS:e5w5x */
        try_add(g, r, c - 1, search, mark);                      /* QS:e6d8s */
        try_add(g, r, c + 1, search, mark);                      /* QS:e7k0o */
        try_add(g, r + 1, c, search, mark);                      /* QS:e8r2j */
        try_add(g, r + 1, c + 1, search, mark);                  /* QS:e9y4e */
        if (r == 0) {                                                 /* QS: */
            sides[0] = true;                                          /* QS: */
        }                                                             /* QS: */
        if (r == g->height - 1) {                                     /* QS: */
            sides[1] = true;                                          /* QS: */
        }                                                             /* QS: */
        if (c == 0) {                                                 /* QS: */
            sides[2] = true;                                          /* QS: */
        }                                                             /* QS: */
        if (c == g->width - 1) {                                      /* QS: */
            sides[3] = true;                                          /* QS: */
        }                                                             /* QS: */
        if ((leftright && sides[2] && sides[3])                       /* QS: */
                || (!leftright && sides[0] && sides[1])) {            /* QS: */
            result = true;                                            /* QS: */
            break;                                                    /* QS: */
        }                                                             /* QS: */
    }                                                                 /* QS: */
        // now we know the result, we need to clean up                /* QS: */
    while (pop(g->stack, &r, &c));  // empty the stack                /* QS: */
    while (pop(g->markstack, &r, &c)) {                               /* QS: */
        g->cells[r][c] = search;                                      /* QS: */
    }                                                                 /* QS: */
    return result;                                                    /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// note that once argument checking is done, most of the work is done /* QS: */
// by another funcion (play_game)                                     /* QS: */
int main(int argc, char** argv) {                                     /* QS: */
    Result r = OK;                                                    /* QS: */
    char ptype[2];                                                    /* QS: */
    int dims[2];                                                      /* QS: */
    if (argc != 4 && argc != 5) {                                     /* QS: */
        r = BADARGC;                                                  /* QS: */
        print_res_message(r, stderr);                                 /* QS: */
        return r;                                                     /* QS: */
    }                                                                 /* QS: */
    if ((strlen(argv[1]) != 1) || (strlen(argv[2]) != 1) ||           /* QS: */
            (argv[1][0] != 'a' && argv[1][0] != 'm') ||               /* QS: */
            (argv[2][0] != 'a' && argv[2][0] != 'm')) {               /* QS: */
        r = BADPTYPE;                                                 /* QS: */
        print_res_message(r, stderr);                                 /* QS: */
        return r;                                                     /* QS: */
    }                                                                 /* QS: */
    ptype[0] = (argv[1][0] == 'm');                              /* QS:e9f7z */
    ptype[1] = (argv[2][0] == 'm');                              /* QS:f0m9u */
    Game g;                                                           /* QS: */
    if (argc == 4) {                                                  /* QS: */
        r = init_game_from_file(&g, argv[3], ptype[0], ptype[1]);     /* QS: */
        if (r != OK) {                                                /* QS: */
            print_res_message(r, stderr);                             /* QS: */
            return r;                                                 /* QS: */
        }                                                             /* QS: */
    } else {                                                          /* QS: */
        char* err;                                                    /* QS: */
        dims[0] = strtol(argv[3], &err, 10);                     /* QS:f1t1q */
        if ((*err != '\0') || (dims[0] < 1) || (dims[0] > 1000)) {    /* QS: */
            r = BADDIM;                                               /* QS: */
        }                                                             /* QS: */
        dims[1] = strtol(argv[4], &err, 10);                     /* QS:f2a4l */
        if ((*err != '\0') || (dims[1] < 1) || (dims[1] > 1000)) {    /* QS: */
            r = BADDIM;                                               /* QS: */
        }                                                             /* QS: */
        if (r != OK) {                                                /* QS: */
            print_res_message(r, stderr);                             /* QS: */
            return r;                                                 /* QS: */
        }                                                             /* QS: */
        init_game(&g, ptype[0], ptype[1], dims[0], dims[1]);          /* QS: */
    }                                                                 /* QS: */
    r = play_game(&g);                                                /* QS: */
    if (r == GAMEOVER) {                                              /* QS: */
        printf("Player %c wins\n", g.playSym[g.cPlayer]);             /* QS: */
        r = OK;                                                       /* QS: */
    }                                                                 /* QS: */
    clean_game(&g);                                                   /* QS: */
    return print_res_message(r, stderr);                              /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// output human readable message for the result code                  /* QS: */
// return the passed in value to allow chaining of ops                /* QS: */
Result print_res_message(Result r, FILE* f) {                         /* QS: */
    switch (r) {                                                      /* QS: */
        case OK:                                                      /* QS: */
            break;                                                    /* QS: */
        case BADARGC:                                                 /* QS: */
            fprintf(f,                                                /* QS: */
                    "Usage: bob p1type p2type [height "               /* QS: */
                    "width | filename]\n");                      /* QS:f3h6g */
            break;                                                    /* QS: */
        case BADPTYPE:                                                /* QS: */
            fprintf(f,"Invalid type\n");                         /* QS:f4o8b */
            break;                                                    /* QS: */
        case BADDIM:                                                  /* QS: */
            fprintf(f,"Sensible board dimensions please!\n");    /* QS:f4v0x */
            break;                                                    /* QS: */
        case BADREADOPEN:                                             /* QS: */
            fprintf(f,                                                /* QS: */
                "Could not start reading from savefile\n");      /* QS:f5c3s */
            break;                                                    /* QS: */
        case BADREADCONT:                                             /* QS: */
            fprintf(f,"Incorrect file contents\n");              /* QS:f6j5n */
            break;                                                    /* QS: */
        case USEREOF:                                                 /* QS: */
            fprintf(f,"EOF from user\n");                        /* QS:f7q7i */
            break;                                                    /* QS: */
        default:                                                      /* QS: */
            fprintf(f,"Unknown result\n");                       /* QS:f8x9d */
    }                                                                 /* QS: */
    return r;                                                         /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
                                                                      /* QS: */
// allocate empty stack                                               /* QS: */
Stack* alloc_stack(void) {                                            /* QS: */
    Stack* s = malloc(sizeof(Stack));                                 /* QS: */
    s->values = malloc(sizeof(unsigned int) * 20);                    /* QS: */
    s->size = 0;                                                      /* QS: */
    s->cap = 10;                                                      /* QS: */
    return s;                                                         /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// deallocate stack                                                   /* QS: */
void free_stack(Stack* s) {                                           /* QS: */
    if (s != 0) {                                                     /* QS: */
        free(s->values);                                              /* QS: */
        free(s);                                                      /* QS: */
    }                                                                 /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// push pair of values onto stack                                     /* QS: */
void push(Stack* s, int r, int c) {                                   /* QS: */
    if (s->size == (s->cap - 1)) {              // need to resize     /* QS: */
        s->cap = s->cap * 1.5;  // this is error prone if malloc fails/* QS: */
        int* temp = malloc(2 * sizeof(int) * s->cap);                 /* QS: */
                // but I'm not expecting you to deal with that case   /* QS: */
          // copy over the old contents                               /* QS: */
        for (unsigned int i = 0; i < s->size; ++i) {                  /* QS: */
            temp[2 * i] = s->values[2 * i];                           /* QS: */
            temp[2 * i + 1] = s->values[2 * i + 1];              /* QS:f8e2z */
        }                                                             /* QS: */
        free(s->values);                                              /* QS: */
        s->values = temp;                                             /* QS: */
    }                                                                 /* QS: */
    s->values[2 * s->size] = r;                                       /* QS: */
    s->values[2 * s->size + 1] = c;                                   /* QS: */
    s->size++;                                                        /* QS: */
}                                                                     /* QS: */
                                                                      /* QS: */
// pop pair of values from stack                                      /* QS: */
bool pop(Stack* s, int* r, int* c) {                                  /* QS: */
    if (s->size == 0) {                                               /* QS: */
        return false;                                                 /* QS: */
    }                                                                 /* QS: */
    unsigned int pos = s->size - 1;                                   /* QS: */
    *r = s->values[2 * pos];                                          /* QS: */
    *c = s->values[2 * pos + 1];                                      /* QS: */
    s->size--;                                                        /* QS: */
    return true;                                                      /* QS: */
}                                                                     /* QS: */
