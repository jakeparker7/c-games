
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#include "common.h"

/*
This is a global function used to handle messages from the bub. These messages are
here because they can be applied to all players. The messages include game over singals
as well as any other moves, loots or shorts.
*/
int other_players_move_handler(char* readString, CarriageDetails* carriageDetails, 
        GameStruct* gameStruct, PlayerPositions* playerPositions, 
        char actualPlayerID) {
    char playerID, targetID, move, orderedMove;
    char* gameOver = "game_over";
    char endString;
    if(sscanf(readString, "hmove%c%c%c", &playerID, &move, &endString) == 2) {
        if(playerID != actualPlayerID) {
            hmove_player(playerPositions, gameStruct, playerID, move);
            return 1;
        }
        return 1;
    }
    if(sscanf(readString, "driedout%c%c", &playerID, &endString) == 1) {
        playerPositions->hits[playerID - 65] = 0;
        fprintf(stderr, "%c dries off\n", playerID);
        return 1;
    }
    if(sscanf(readString, "ordered%c%c%c", &playerID, &orderedMove, &endString)
            == 2) {

        gameStruct->playerMoveArray[playerID - 65] = orderedMove;
        fprintf(stderr, "%c ordered %c\n", playerID, orderedMove);
        return 1;
    }
    if(sscanf(readString, "long%c%c%c", &playerID, &targetID, &endString) 
            == 2) {
        if(playerID != actualPlayerID) {
            player_longed(playerPositions, playerID, targetID, gameStruct);
            return 1;
        }
    }
    if(sscanf(readString, "short%c%c%c", &playerID, &targetID, &endString) 
            == 2) {
        if(playerID != actualPlayerID) {
            player_shorted(playerPositions, playerID, targetID, gameStruct,
                    carriageDetails);
            return 1;
        }
        return 1;
    }
    if(strcmp(readString, gameOver) == 0) {
        exit(0);
        return 1;
    }
    if (strcmp(readString, "round") == 0) {
        return 1;
    }
    return 0;
}

/*
This function initialises the game struct, it also takes in args to 
allocate seed, players and carriages as they are constant values passed
from the hub.
*/
GameStruct* player_init(int argc, char** argv) {
    GameStruct* gameStruct = malloc(sizeof(GameStruct*));
    gameStruct->playerMoveArray = (char*)malloc(sizeof(char) * 
            gameStruct->totalPlayers);
    gameStruct->totalPlayers = atoi(argv[1]);
    gameStruct->totalCarriages = atoi(argv[3]);
    gameStruct->seed = atoi(argv[4]);
    return gameStruct;
}

/*
This function initialises the CarriageDetails struct, this struct holds
an array of the top level and the bottom level values of loot. It 
also runs the loot formulae of the game and successfully allocates the
loot.
*/
CarriageDetails* loot_distributor(GameStruct* gameStruct) {
    //initialising Carriage Details
    CarriageDetails* carriageDetails = malloc(sizeof(CarriageDetails));
    carriageDetails->topLevel = (int*)malloc(sizeof(int) * 
            gameStruct->totalCarriages);
    carriageDetails->bottomLevel = (int*)malloc(sizeof(int) * 
            gameStruct->totalCarriages);

    for(int i = 0; i < gameStruct->totalCarriages; i++) {
    	carriageDetails->topLevel[i] = 0;
    	carriageDetails->bottomLevel[i] = 0;
    }
    int totalLoot = ((gameStruct->seed % 4) + 1) * gameStruct->totalCarriages;
    int lootCarriage = ceil(gameStruct->totalCarriages / 2.0);
    //Y = 0 here so bottom level
    int carriageLevel = 0;
    carriageDetails->bottomLevel[lootCarriage]++;
    totalLoot--;
    while(totalLoot != 0) {
        lootCarriage = (lootCarriage + (gameStruct->seed % 101)) 
            % gameStruct->totalCarriages;
        if((carriageLevel = (carriageLevel + (gameStruct->seed % 2)) % 2) 
                == 0) {
            carriageDetails->bottomLevel[lootCarriage]++;
        } else {
            carriageDetails->topLevel[lootCarriage]++;
        }
        totalLoot--;
    }
    return carriageDetails;
}

/*
This function initialises the player positions struct. The player postions struct
contains details about where each player is on the board. The amount of loot
each player has and the amount of hits each player has.
*/
PlayerPositions* player_position_init(GameStruct* gameStruct) {
    //Initialising Player positions
    int i;
    char** topLevel;
    char** bottomLevel;
    PlayerPositions* playerPositions = malloc(sizeof(PlayerPositions));
    playerPositions->topLevel = (char**)malloc(sizeof(char*) * 
        gameStruct->totalCarriages);
    topLevel = playerPositions->topLevel;
    for(i = 0; i < gameStruct->totalCarriages; i++) {
        topLevel[i] = (char*)malloc(sizeof(char) * 
                gameStruct->totalPlayers + 1);
        memset(topLevel[i], '\0', gameStruct->totalCarriages);
    }
    playerPositions->bottomLevel = (char**)malloc(sizeof(char*) * 
        gameStruct->totalCarriages);
    bottomLevel = playerPositions->bottomLevel;
    for(i = 0; i < gameStruct->totalCarriages; i++) {
        bottomLevel[i] = (char*)malloc(sizeof(char) * 
                gameStruct->totalPlayers + 1);
        memset(bottomLevel[i], '\0', gameStruct->totalCarriages);
    }
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        int carriage;
        char playerID = i + 65;
        carriage = i % gameStruct->totalCarriages;
        append(playerPositions->bottomLevel[carriage], playerID);
    }
    playerPositions->loot = (int*)malloc(sizeof(int) * 
            gameStruct->totalPlayers);
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        playerPositions->loot[i] = 0;
    }
    playerPositions->hits = (int*)malloc(sizeof(int) * 
            gameStruct->totalPlayers);
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        playerPositions->hits[i] = 0;
    }
    return playerPositions;
}

/*
Error Controller is a function thaht checks each players for any errors
with the passed args, player count, seed etc.
*/
void error_controller(int argc, char** argv) {
    int i, playerValue;
    if(argc != 5) {
        fprintf(stderr, "Usage: player pcount myid width seed\n");
        exit(1);
    }
    if (argv[1] && argv[1][0] != 0) {
        playerValue = atoi(argv[1]);
        for(i = 0; i < strlen(argv[1]); i++) {
            if(!isdigit(argv[1][i]) || playerValue < 2 || playerValue > 26) {
                fprintf(stderr, "Invalid player count\n");
                exit(2);
            }
        }
    } else {
        fprintf(stderr, "Invalid player count\n");
    }
    for(i = 0; i < strlen(argv[2]); i++) {
        if(!isdigit(argv[2][i]) || atoi(argv[1]) < atoi(argv[2]) 
                || atoi(argv[2]) < 0) {
            fprintf(stderr, "Invalid player ID\n");
            exit(3);
        }
    }
    for(i = 0; i < strlen(argv[3]); i++) {
        if(!isdigit(argv[3][i]) || atoi(argv[3]) < 3) {
            fprintf(stderr, "Invalid width\n");
            exit(4);
        }
    }
    for(i = 0; i < strlen(argv[4]); i++) {
        if(!isdigit(argv[4][i]) || atoi(argv[4]) < 0) {
            fprintf(stderr, "Invalid seed\n");
            exit(5);
        }
    }
    printf("!");
    fflush(stdout);
}

/*
This is a function to check that the hubs messages are appropriate
*/
void check_hub_message(char* readString, GameStruct* gameStruct) {
    char orderedMove, playerID, targetID, move;
    int totalPlayers = gameStruct->totalPlayers;
    if(sscanf(readString, "ordered%c%c", &playerID, &orderedMove) == 2) {
        if(orderedMove != 'v' && orderedMove != 'l' && orderedMove != 'h' 
            && orderedMove != 's' && orderedMove != '$') {
            fprintf(stderr, "Communication Error\n");
            exit(6);
        }
        if(0 > playerID - 65 || totalPlayers - 1 < playerID - 65) {
            fprintf(stderr, "Communication Error\n");
            exit(6);
        }
    }
    if(sscanf(readString, "long%c%c", &playerID, &targetID) == 2) {
        if(0 > playerID - 65 || totalPlayers < playerID - 65) {
            fprintf(stderr, "Communication Error player\n");
            exit(6);
        }
        if((0 > targetID - 65 || totalPlayers - 1 < targetID - 65) 
                && targetID != 45) {
            fprintf(stderr, "Communication Error IDD\n");
            exit(6);
        }
    }
    if(sscanf(readString, "hmove%c%c", &playerID, &move) == 2) {
        if(move != '+' && move != '-') {
            fprintf(stderr, "Communication Error\n");
            exit(6);
        }
        if(0 > playerID - 65 || totalPlayers - 1 < playerID - 65) {
            fprintf(stderr, "Communication Error\n");
            exit(6);
        }
    }
    if(sscanf(readString, "looted%c", &playerID) == 1) {
        if(0 > playerID - 65 || totalPlayers - 1 < playerID - 65) {
            fprintf(stderr, "Communication Error\n");
            exit(6);
        }
    }
    if(sscanf(readString, "vmove%c", &playerID) == 1) {
        if(0 > playerID - 65 || totalPlayers - 1 < playerID - 65) {
            fprintf(stderr, "Communication Error\n");
            exit(6);
        }
    }
    if(sscanf(readString, "short%c%c", &playerID, &targetID) == 2) {
        if(0 > playerID - 65 || totalPlayers - 1 < playerID - 65) {
            fprintf(stderr, "Communication Error\n");
            exit(6);
        }
        if((0 > targetID - 65 || totalPlayers - 1 < targetID - 65) 
                && targetID != 45) {
            fprintf(stderr, "Communication Error\n");
            exit(6);
        }
    }
}

/*
This function simply reads the string at standard in and returns the char* 
representation.
*/
char* read_standard_in(void) {
    int buffer = 2048;
    char* readString = (char*)malloc(buffer);
    memset(readString, 0, 2048);
    //Valid pipe should work
    if (fgets(readString, buffer, stdin) == NULL) { 
        fprintf(stderr, "Communication Error\n");
        exit(6);
    }
    readString[strlen(readString) - 1] = '\0'; 
    return readString;
}

/*
Append is a simple function that appends a character (appendedCharacter)
to a set string (appenedString).
*/
char* append(char* appendedString, char appendedCharacter) {
    int length = strlen(appendedString);
    appendedString[length] = appendedCharacter;
    appendedString[length + 1] = '\0';
    return appendedString;
}

/*
This function adjusts each playes playerPosition struct for each players move.
*/
void hmove_player(PlayerPositions* playerPositions, GameStruct* gameStruct,
        char playerID, char direction) {
    int i, j, found = 0;
    char level;
    char* newValue;
    for(i = 0; i < gameStruct->totalCarriages; i++) {
        for(j = 0; j < strlen(playerPositions->topLevel[i]); j++) {
            if(playerPositions->topLevel[i][j] == playerID) {
                newValue = recreate_string(playerPositions->topLevel[i], 
                        playerID, gameStruct);
                playerPositions->topLevel[i] = newValue;
                found = 1;
                level = 't';
                break;
            }
        }
        if(found == 1) {
            break;
        }
        for(j = 0; j < strlen(playerPositions->bottomLevel[i]); j++) {
            if(playerPositions->bottomLevel[i][j] == playerID) {
                newValue = recreate_string(playerPositions->bottomLevel[i], 
                        playerID, gameStruct);
                playerPositions->bottomLevel[i] = newValue;
                found = 1;
                level = 'b';
                break;
            }
        }
        if(found == 1) {
            break;
        }
    }
    if(direction == '+' && level == 'b') {

        playerPositions->bottomLevel[i + 1] 
                = append(playerPositions->bottomLevel[i + 1], playerID);
        fprintf(stderr, "%c moved to %d/%d\n", playerID, i + 1, 0);
    } 
    if(direction == '-' && level == 'b') {
        playerPositions->bottomLevel[i - 1] 
                = append(playerPositions->bottomLevel[i - 1], playerID);
        fprintf(stderr, "%c moved to %d/%d\n", playerID, i - 1, 0);
    }
    if(direction == '+' && level == 't') {

        playerPositions->topLevel[i + 1] 
                = append(playerPositions->topLevel[i + 1], playerID);
        fprintf(stderr, "%c moved to %d/%d\n", playerID, i + 1, 1);
    } 
    if(direction == '-' && level == 't') {
        playerPositions->topLevel[i - 1] 
                = append(playerPositions->topLevel[i - 1], playerID);
        fprintf(stderr, "%c moved to %d/%d\n", playerID, i - 1, 1);
    }
}

/*
Vmove player is a function that adjusts each the player positon structs
of each player to know where the knew player has moved to. 
*/
void vmove_player(PlayerPositions* playerPositions, GameStruct* gameStruct,
        char playerID) {
    int i, j;
    char* newValue;
    for(i = 0; i < gameStruct->totalCarriages; i++) {
        for(j = 0; j < strlen(playerPositions->topLevel[i]); j++) {
            if(playerPositions->topLevel[i][j] == playerID) {
                newValue = recreate_string(playerPositions->topLevel[i], 
                        playerID, gameStruct);
                playerPositions->topLevel[i] = newValue;
                playerPositions->bottomLevel[i] 
                        = append(playerPositions->bottomLevel[i], playerID);
                fprintf(stderr, "%c moved to %d/%d\n", playerID, i, 0);
                return;
            }
        }
    }
    for(i = 0; i < gameStruct->totalCarriages; i++) {
        for(j = 0; j < strlen(playerPositions->bottomLevel[i]); j++) {
            if(playerPositions->bottomLevel[i][j] == playerID) {
                newValue = recreate_string(playerPositions->bottomLevel[i], 
                        playerID, gameStruct);
                playerPositions->bottomLevel[i] = newValue;
                playerPositions->topLevel[i] 
                        = append(playerPositions->topLevel[i], playerID);
                fprintf(stderr, "%c moved to %d/%d\n", playerID, i, 1);
                return;
            }
        }
    }
}

/*
This function adjusts the amount of loot each player know each other have, aswell as
adjusts the CarriageDetails for the loot available on the board.
*/
void looted(PlayerPositions* playerPositions, GameStruct* gameStruct, 
        CarriageDetails* carriageDetails, char playerID) {
    int i, j;
    for(i = 0; i < gameStruct->totalCarriages; i++) {
        for(j = 0; j < strlen(playerPositions->topLevel[i]); j++) {
            if(playerPositions->topLevel[i][j] == playerID && 
                    carriageDetails->topLevel[i] > 0) {
                carriageDetails->topLevel[i]--;
                playerPositions->loot[playerID - 65]++;
                fprintf(stderr, "%c picks up loot (they now have %d)\n", playerID, 
                        playerPositions->loot[playerID - 65]);
                return;
            }
        }
    }
    for(i = 0; i < gameStruct->totalCarriages; i++) {
        for(j = 0; j < strlen(playerPositions->bottomLevel[i]); j++) {
            if(playerPositions->bottomLevel[i][j] == playerID && 
                    carriageDetails->bottomLevel[i] > 0) {
                carriageDetails->bottomLevel[i]--;
                playerPositions->loot[playerID - 65]++;
                fprintf(stderr, "%c picks up loot (they now have %d)\n", playerID, 
                        playerPositions->loot[playerID - 65]);
                return;
            }

        }
    }
    
    fprintf(stderr, "%c tries to pick up loot but there isn't any\n", playerID);
}

/*
This function is used to recreate a string. Often used when a player moves
from a position, the string is recreated to remove that playerID from the
current existing string.
*/
char* recreate_string(char* oldString, char playerID, GameStruct* gameStruct) {
    int i;
    char* newValue = (char*)malloc(sizeof(char) * gameStruct->totalPlayers 
            + 1);
    memset(newValue, '\0', gameStruct->totalPlayers + 1);
    for(i = 0; i < strlen(oldString); i++) {
        if(oldString[i] != playerID) {
            append(newValue, oldString[i]);
        }
    }
    return newValue;
}

/*
Remove player is a function that will remove the desired character from the 
current string, used for playerPositions adjustments, when another player 
moves.
*/
void remove_player(PlayerPositions* playerPositions, GameStruct* gameStruct,
        char playerID, int currentCariage, int carriageLevel) {
    char* oldString;
    char* newValue;
    if(carriageLevel == 1) {
        oldString = playerPositions->topLevel[currentCariage];
        newValue = recreate_string(oldString, playerID, gameStruct);
        playerPositions->topLevel[currentCariage] = newValue;
    } else if (carriageLevel == 0) {
        oldString = playerPositions->bottomLevel[currentCariage];
        newValue = recreate_string(oldString, playerID, gameStruct);
        playerPositions->bottomLevel[currentCariage] = newValue;
    }
}

/*
This function takes in a string and returns the highest character
*/
char highest_ID(char* chosenString, char playerID) {
    int i;
    char highestID;
    for(i = 0; i < strlen(chosenString); i++) {
        if(chosenString[0] != playerID) {
            highestID = chosenString[0];
        } else {
            highestID = chosenString[1];
        }
        if(chosenString[i] > highestID && chosenString[i] != playerID) {
            highestID = chosenString[i];
        }
    }
    return highestID;
}

/*
This function takes in a string and returns the lowest character.
*/
char lowest_ID(char* chosenString, char playerID ) {
    int i;
    char lowestID;
    for(i = 0; i < strlen(chosenString); i++) {
        if(chosenString[0] != playerID) {
            lowestID = chosenString[0];
        } else {
            lowestID = chosenString[1];
        }
        if(chosenString[i] < lowestID && chosenString[i] 
                != playerID) {
            lowestID = chosenString[i];
        }
    }
    return lowestID;
}


/*
This function simply checks if a long is available.
*/
void player_longed(PlayerPositions* playerPositions, char playerID, 
        char targetID, 
        GameStruct* gameStruct) {
    int playerCarriage;
    int targetCarriage;
    if(find_player_position_top(playerID, playerPositions, gameStruct) 
            != 100 &&
            find_player_position_top(targetID, playerPositions, gameStruct) 
                != 100) {
        playerPositions->hits[targetID - 65]++;
        fprintf(stderr, "%c targets %c who has %d hits\n", playerID, targetID,
                playerPositions->hits[targetID - 65]);
        return;
    }
    if(find_player_position_bot(playerID, playerPositions, gameStruct) 
            != 100 && 
            find_player_position_bot(targetID, playerPositions, gameStruct) 
                != 100) {
        playerCarriage = find_player_position_bot(playerID, playerPositions, 
                gameStruct);
        targetCarriage = find_player_position_bot(targetID, playerPositions, 
                gameStruct);
        if(playerCarriage - 1 == targetCarriage || playerCarriage + 1 == 
                targetCarriage) {
            playerPositions->hits[targetID - 65]++;
            fprintf(stderr, "%c targets %c who has %d hits\n", playerID, targetID, 
                    playerPositions->hits[targetID - 65]);
            return;
        }
    }
    fprintf(stderr, "%c has no target\n", playerID);
}

/*
This function handles if all children if a short happens
*/
void player_shorted(PlayerPositions* playerPositions, char playerID, 
        char targetID,
        GameStruct* gameStruct, CarriageDetails* carriageDetails) {
    int playerCarriage;
    if(find_player_position_top(playerID, playerPositions, gameStruct) != 
            100 &&
            find_player_position_top(targetID, playerPositions, gameStruct) 
                != 100) {
        if(find_player_position_top(playerID, playerPositions, gameStruct) ==
            find_player_position_top(targetID, playerPositions, gameStruct)) {
            playerCarriage = find_player_position_top(targetID, 
                    playerPositions, gameStruct);
            if(playerPositions->loot[targetID - 65] > 0) {
                playerPositions->loot[targetID - 65]--;
                carriageDetails->topLevel[playerCarriage]++;
                fprintf(stderr, "%c makes %c drop loot\n", playerID, targetID);
                return;
            } else {
                fprintf(stderr, 
                        "%c tries to make %c drop loot they don't have\n", 
                        playerID, targetID);
                return;
            }
        }
    }
    if(find_player_position_bot(playerID, playerPositions, gameStruct) 
            != 100 && 
            find_player_position_bot(targetID, playerPositions, gameStruct) 
                != 100) {
        if(find_player_position_bot(playerID, playerPositions, gameStruct) ==
                find_player_position_bot(targetID, playerPositions, 
                        gameStruct)) {
            playerCarriage = find_player_position_bot(targetID, 
                    playerPositions, gameStruct);
            if(playerPositions->loot[targetID - 65] > 0) {
                playerPositions->loot[targetID - 65]--;
                carriageDetails->bottomLevel[playerCarriage]++;
                fprintf(stderr, "%c makes %c drop loot\n", playerID, targetID);
                return;
            } else {
                fprintf(stderr, 
                        "%c tries to make %c drop loot they don't have\n", 
                        playerID, targetID);
                return;
            }
        }
    }
    fprintf(stderr, "%c has no target\n", playerID);
}

/*
This is a function to find a particular player with the top player
position array.
*/
int find_player_position_top(char playerID, PlayerPositions* playerPositions, 
        GameStruct* gameStruct) {
    int i, j;
    for(i = 0; i < gameStruct->totalCarriages; i++) {
        for(j = 0; j < strlen(playerPositions->topLevel[i]); j++) {
            if(playerPositions->topLevel[i][j] == playerID) {
                return i;
            }
        }
    }
    return 100;
}

/*
This is a function to find a particular player within the bottom player
positions array.
*/
int find_player_position_bot(char playerID, PlayerPositions* playerPositions,
        GameStruct* gameStruct) {
    int i, j;
    for(i = 0; i < gameStruct->totalCarriages; i++) {
        for(j = 0; j < strlen(playerPositions->bottomLevel[i]); j++) {
            if(playerPositions->bottomLevel[i][j] == playerID) {
                return i;
            }
        }
    }
    return 100;
}