#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <ctype.h>
#include "common.h"
#include <sys/wait.h>

typedef struct {
    char playerID;
    int loot;
    int currentCarriage;
    int carriageLevel;
    char move;
    char justShortOrLong;
} BanditStruct;


int move_horrizontal(CarriageDetails* carriageDetails, 
        BanditStruct* banditStruct, GameStruct* gameStruct, 
        PlayerPositions* playerPositions);
int move_loot(CarriageDetails* carriageDetails, BanditStruct* banditStruct, 
        PlayerPositions* playerPositions);
int move_vertical(BanditStruct* banditStruct, PlayerPositions* playerPositions,
        GameStruct* gameStruct);
void bandit_decider(char* readString, 
        CarriageDetails* carriageDetails, BanditStruct* banditStruct,
        GameStruct* gameStruct, PlayerPositions* playerPositions);
char move_controller(CarriageDetails* carriageDetails, GameStruct* gameStruct, 
        BanditStruct* banditStruct, PlayerPositions* playerPositions);
int long_check(PlayerPositions* playerPositions, BanditStruct* banditStruct, 
        GameStruct* gameStruct);
int move_short(PlayerPositions* playerPositions, BanditStruct* banditStruct, 
        CarriageDetails* carriageDetails);
int vertical_check(GameStruct* gameStruct, CarriageDetails* carriageDetails,
        BanditStruct* banditStruct);

/*
The main of the bandit controller checks for all errors, initialises all
required structs, it additionally uses a while true loop to constantly 
read from the parent until the game is over, player dries out or an
error has occured.
*/
int main(int argc, char** argv) {
    error_controller(argc, argv);
    BanditStruct* banditStruct = malloc(sizeof(BanditStruct));
    GameStruct* gameStruct = player_init(argc, argv);
    CarriageDetails* carriageDetails = loot_distributor(gameStruct);
    PlayerPositions* playerPositions = player_position_init(gameStruct);
    int playerInteger = atoi(argv[2]);
    banditStruct->playerID = playerInteger + 65;
    banditStruct->currentCarriage = playerInteger % gameStruct->totalCarriages;
    banditStruct->carriageLevel = 0;
    char* readString;
    while(1) {
        readString = read_standard_in();
        //fprintf(stderr, "CHILD (%c) Read string is: %s\n", banditStruct->playerID, readString);
        bandit_decider(readString, carriageDetails, banditStruct, gameStruct, 
                playerPositions);    
    }
}

/*
Bandit decider is a function that analyses the readString from the Hub and
performs the correct move.
*/
void bandit_decider(char* readString, 
        CarriageDetails* carriageDetails, BanditStruct* banditStruct,
        GameStruct* gameStruct, PlayerPositions* playerPositions) {
    char* yourTurn = "yourturn";
    char* horrizontalMessage = "h?";
    char* shortMessage = "s?";
    char* longMessage = "l?";
    char* execute = "execute";
    char playerID, move, endString;
    check_hub_message(readString, gameStruct);

    if(strcmp(readString, yourTurn) == 0) {
        //fprintf(stderr, "CHILD(%c) carriage level %d\n", banditStruct->playerID, 
        //    banditStruct->currentCarriage);
        move = move_controller(carriageDetails, gameStruct, banditStruct,
                playerPositions);
        banditStruct->move = move;
        printf("play%c\n", move);
        fflush(stdout);
        return;
    }
    if(strcmp(readString, execute) == 0) {
        return;
    }
    if(sscanf(readString, "looted%c%c", &playerID, &endString) == 1) {
        if(playerID != banditStruct->playerID) {
            looted(playerPositions, gameStruct, carriageDetails, playerID);
            return;
        }
        if(playerID == banditStruct->playerID) {
            move_loot(carriageDetails, banditStruct, playerPositions);
            return;
        }
    }
    if(sscanf(readString, "vmove%c%c", &playerID, &endString) == 1) {
        if(playerID != banditStruct->playerID) {
            vmove_player(playerPositions, gameStruct, playerID);
            return;
        }
        if(playerID == banditStruct->playerID) {
            move_vertical(banditStruct, playerPositions, gameStruct);
            return;
        }
    }
    if(strcmp(readString, horrizontalMessage) == 0) {
        move_horrizontal(carriageDetails, banditStruct,
                gameStruct, playerPositions);
        return;
    }
    if(strcmp(readString, shortMessage) == 0) {
        //fprintf(stderr, "Child(%c) Handling short\n", banditStruct->playerID);
        move_short(playerPositions, banditStruct, carriageDetails);
        //fprintf(stderr, "Child(%c) Loot in current carriage %d\n", 
        //        banditStruct->playerID,
        //        carriageDetails->bottomLevel[banditStruct->currentCarriage]);
        //fprintf(stderr, "Child(%c) Short ends\n", banditStruct->playerID);
        return;
    }
    if(strcmp(readString, longMessage) == 0) {
        fprintf(stderr, "Handling long\n");
        //move_long(playerPositions, banditStruct, carriageDetails);
        return;
    }
    if(other_players_move_handler(readString, carriageDetails, gameStruct,
            playerPositions, banditStruct->playerID) == 1) {
        return;
    }
    fprintf(stderr, "Communication Error\n");
    exit(0);
}

/*
Move controller is a function that returns the decided move
for the bandit. It follows the bandit's algorithim of preference
and returns the desired move character.
*/
char move_controller(CarriageDetails* carriageDetails, GameStruct* gameStruct, 
        BanditStruct* banditStruct, PlayerPositions* playerPositions) {
    int sumRight = 0, sumLeft = 0;
    fprintf(stderr, "CHILD(%c) Move controller %d\n", banditStruct->playerID,
            banditStruct->currentCarriage);
    if(banditStruct->carriageLevel == 0) {
        //fprintf(stderr, "Current Carriage: %d Loot available: %d\n", 
        //        banditStruct->currentCarriage, 
        //        carriageDetails->bottomLevel[banditStruct->currentCarriage]);
        //fprintf(stderr, "CHILD(%d) carriageLevel: %d\n", banditStruct->playerID,
        //        banditStruct->carriageLevel);
        if(carriageDetails->bottomLevel[banditStruct->currentCarriage] > 0) {
            banditStruct->justShortOrLong = 'n';
            return '$';
        }
        if(strlen(playerPositions->bottomLevel[banditStruct->currentCarriage]) 
            > 1
            && banditStruct->justShortOrLong == 'n') {
            banditStruct->justShortOrLong = 'y';
            return 's';
        }
        //fprintf(stderr, "CHILD(%d) Dealing with sums\n", banditStruct->playerID);
        if(banditStruct->currentCarriage != 0) {
            sumLeft = carriageDetails->bottomLevel[banditStruct->currentCarriage - 1];
        }
        sumRight = carriageDetails->bottomLevel[banditStruct->currentCarriage + 1];
    } else {
        if(carriageDetails->topLevel[banditStruct->currentCarriage] > 0) {
            banditStruct->justShortOrLong = 'n';
            return '$';
        }
        if(strlen(playerPositions->topLevel[banditStruct->currentCarriage]) > 1
                && banditStruct->justShortOrLong == 'n') {
            banditStruct->justShortOrLong = 'y';
            return 's';
        }
        if(banditStruct->currentCarriage != gameStruct->totalCarriages - 1) {
            sumRight = carriageDetails->topLevel[banditStruct->currentCarriage + 1];
        }
        sumLeft = carriageDetails->topLevel[banditStruct->currentCarriage - 1];
    }

    if(vertical_check(gameStruct, carriageDetails, banditStruct) == 1) {
        banditStruct->justShortOrLong = 'n';
        return 'v';
    }
    if(sumLeft != sumRight) {
        banditStruct->justShortOrLong = 'n';
        return 'h';
    }
    if(long_check(playerPositions, banditStruct, gameStruct) == 1 
            && banditStruct->justShortOrLong == 'n') {
            //fprintf(stderr, "handling long check\n");
            banditStruct -> justShortOrLong = 'y';
        return 'l';
    }

    //fprintf(stderr, "Default move\n");
    banditStruct->justShortOrLong = 'n';
    return 'v';
}

/*
Move horriszontal is a function that sums both the loot to the left and
the loot to the right of the player postion. It then decides to 
move the player towards the direction with the highest loot. 
*/
int move_horrizontal(CarriageDetails* carriageDetails, 
        BanditStruct* banditStruct, GameStruct* gameStruct, 
        PlayerPositions* playerPositions) {
    int i, sumRight = 0, sumLeft = 0;
    //Do bottom level
    if(banditStruct->carriageLevel == 0) {
        for(i = banditStruct->currentCarriage; i < 
                gameStruct->totalCarriages - 1; i++) {
            sumRight += carriageDetails->bottomLevel[i];
        }
        for(i = banditStruct->currentCarriage; i > -1; i--) {
            sumLeft += carriageDetails->bottomLevel[i];
        }
    } else {
        for(i = banditStruct->currentCarriage; i < 
                gameStruct->totalCarriages - 1; i++) {
            sumRight += carriageDetails->topLevel[i];
        }
        for(i = banditStruct->currentCarriage; i > -1; i--) {
            sumLeft += carriageDetails->topLevel[i];
        }
    }
    if(sumLeft > sumRight) {
        remove_player(playerPositions, gameStruct, banditStruct->playerID, 
                banditStruct->currentCarriage,
                banditStruct->carriageLevel);
        banditStruct->currentCarriage--;
        fprintf(stderr, "%c moved to %d,%d\n", banditStruct->playerID, 
                banditStruct->currentCarriage, 
                banditStruct->carriageLevel);
        fprintf(stdout, "sideways-\n");
        fflush(stdout);
    } else if (sumLeft < sumRight) {
        remove_player(playerPositions, gameStruct, banditStruct->playerID, 
                banditStruct->currentCarriage,
                banditStruct->carriageLevel);
        banditStruct->currentCarriage++;
        fprintf(stderr, "%c moved to %d,%d\n", banditStruct->playerID, 
                banditStruct->currentCarriage, 
                banditStruct->carriageLevel);
        fprintf(stdout, "sideways+\n");
        fflush(stdout);
    } else if (banditStruct->currentCarriage != 0) {
        remove_player(playerPositions, gameStruct, banditStruct->playerID, 
                banditStruct->currentCarriage,
                banditStruct->carriageLevel);
        banditStruct->currentCarriage--;
        fprintf(stderr, "%c moved to %d,%d\n", banditStruct->playerID, 
                banditStruct->currentCarriage, 
                banditStruct->carriageLevel);
        fprintf(stdout, "sideways-\n");
        fflush(stdout);
    } else {
        remove_player(playerPositions, gameStruct, banditStruct->playerID, 
                banditStruct->currentCarriage,
                banditStruct->carriageLevel);
        banditStruct->currentCarriage++;
        fprintf(stderr, "%c moved to %d,%d\n", banditStruct->playerID, 
                banditStruct->currentCarriage, 
                banditStruct->carriageLevel);
        fprintf(stdout, "sideways+\n");
        fflush(stdout);
    }
    if(banditStruct->carriageLevel == 0) {
        playerPositions->bottomLevel[banditStruct->currentCarriage] 
                = append(playerPositions->bottomLevel[banditStruct->currentCarriage],
                banditStruct->playerID);
    } else {
        playerPositions->topLevel[banditStruct->currentCarriage] 
                = append(playerPositions->topLevel[banditStruct->currentCarriage],
                banditStruct->playerID);
    }
    return 1;
}

/*
Move vertical is a function that moves the bandit to the opposite carriage.
*/
int move_vertical(BanditStruct* banditStruct, PlayerPositions* playerPositions, 
        GameStruct* gameStruct) {
    fprintf(stderr, "Changing current carriage posn %d\n", banditStruct->currentCarriage);
    if(banditStruct->carriageLevel == 1) {
        //fprintf(stderr, "Child(%d) Moving bottom\n", banditStruct->playerID);
        remove_player(playerPositions, gameStruct, banditStruct->playerID, banditStruct->currentCarriage,
                banditStruct->carriageLevel);
        banditStruct->carriageLevel = 0;
        playerPositions->bottomLevel[banditStruct->currentCarriage] 
                = append(playerPositions->bottomLevel[banditStruct->currentCarriage],
                banditStruct->playerID);
        //fprintf(stderr, "CHILD(%c) %c moved to %d/%d\n", banditStruct->playerID, banditStruct->playerID, banditStruct->currentCarriage,
        //        banditStruct->carriageLevel);
        return 1;
    } else {
        //fprintf(stderr, "Child(%d) Moving top\n", banditStruct->playerID);
        remove_player(playerPositions, gameStruct, banditStruct->playerID, banditStruct->currentCarriage,
                banditStruct->carriageLevel);
        banditStruct->carriageLevel = 1;
        playerPositions->topLevel[banditStruct->currentCarriage] 
                = append(playerPositions->topLevel[
                    banditStruct->currentCarriage],
                    banditStruct->playerID);
        //fprintf(stderr, "CHILD(%c) %c moved to %d/%d\n", banditStruct->playerID, banditStruct->playerID, banditStruct->currentCarriage,
        //       banditStruct->carriageLevel);
        return 1;
    }
}

/*
This function simply picks up loot if it is available, it then adjusts all corresponding
arrays to reflect that loot has been taken from the current carriage.
*/
int move_loot(CarriageDetails* carriageDetails, BanditStruct* banditStruct, 
        PlayerPositions* playerPositions) {
    if(banditStruct->carriageLevel == 1) {
        if(carriageDetails->topLevel[banditStruct->currentCarriage] > 0) {
            playerPositions->loot[banditStruct->playerID - 65]++;
            carriageDetails->topLevel[banditStruct->currentCarriage] -= 1;
            fprintf(stderr, "%c picks up loot (they now have %d)\n", 
                    banditStruct->playerID,
                    playerPositions->loot[banditStruct->playerID - 65]);
            return 1;
        } 
    } else if (banditStruct->carriageLevel == 0) {
        if(carriageDetails->bottomLevel[banditStruct->currentCarriage] > 0) {
            playerPositions->loot[banditStruct->playerID - 65]++;
            carriageDetails->bottomLevel[banditStruct->currentCarriage] -= 1;
            fprintf(stderr, "%c picks up loot (they now have %d)\n", 
                    banditStruct->playerID,
                    playerPositions->loot[banditStruct->playerID - 65]);
            return 1;
        }
    }
    fprintf(stderr, "%c tries to pick up loot but there isn't any\n", 
            banditStruct->playerID);
    return 0;
}

/*
The move short function checks if there is a player currently at the
same position as the current player moving. If so it will select
the player with the highest ID and make them drop loot, if they
have any loot to drop.
*/
int move_short(PlayerPositions* playerPositions, BanditStruct* banditStruct, 
        CarriageDetails* carriageDetails) {
    char* positionString;
    char shortedPlayer;
    //fprintf(stderr, "Hit short\n");
    if(banditStruct->carriageLevel == 0) {
        positionString = 
                playerPositions->bottomLevel[banditStruct->currentCarriage];
        //fprintf(stderr, "Posn: %s\n", positionString);
        //bottom level
        if(strlen(positionString) > 1) {
            shortedPlayer = highest_ID(positionString, banditStruct->playerID);
            if(playerPositions->loot[shortedPlayer - 65] == 0) {
                fprintf(stderr, 
                        "%c tries to make %c drop loot they dont have\n", 
                        banditStruct->playerID, shortedPlayer);
            } else {
                fprintf(stderr, "%c makes %c drop loot\n", 
                        banditStruct->playerID, shortedPlayer);
                carriageDetails->bottomLevel[banditStruct->currentCarriage]++;
                playerPositions->loot[shortedPlayer - 65]--;
            }
            printf("target.short%c\n", shortedPlayer);
            fflush(stdout);
            return 1;
        } else {
            fprintf(stderr, "%c has no target\n", banditStruct->playerID);
            printf("target.short-\n");
            fflush(stdout);
        }
    } else {
        positionString = 
                playerPositions->topLevel[banditStruct->currentCarriage];
        //top level
        if(strlen(positionString) > 1) {
            //dosomethig
            shortedPlayer = highest_ID(positionString, banditStruct->playerID);
            if(playerPositions->loot[shortedPlayer - 65] == 0) {
                fprintf(stderr, 
                        "%c tries to make %c drop loot they dont have\n", 
                        banditStruct->playerID, shortedPlayer);
            } else {
                fprintf(stderr, "%c makes %c drop loot\n", 
                        banditStruct->playerID, shortedPlayer);
                carriageDetails->topLevel[banditStruct->currentCarriage]++;
                playerPositions->loot[shortedPlayer - 65]--;
            }
            printf("target.short%c\n", shortedPlayer);
            fflush(stdout);
            return 1;
        } else {
            fprintf(stderr, "%c has no target\n", banditStruct->playerID);
            printf("target.short-\n");
            fflush(stdout);
        }
    }
    return 0;
}

/*
The move long function searches if there are players to the left or
right carriage to the current position, if the current position is
on the bottom level. Other wise if on the top level it will look
accross the entire level and find the closest player, with the 
highest ID, if there is one. Once a player is found they will be
shot and allocated a hit.
*/
void move_long(PlayerPositions* playerPositions, BanditStruct* banditStruct, 
        GameStruct* gameStruct) {
    char* carriageLeft;
    char* carriageRight;
    char* emptyString = "";
    int i, j;
    char targetID, targetIDLeft, targetIDRight;
    if(banditStruct->carriageLevel == 0) {
        if(banditStruct->currentCarriage == 0) {
            carriageRight = playerPositions->bottomLevel[
                    banditStruct->currentCarriage + 1];
            if(carriageRight != emptyString) {
                targetID = lowest_ID(carriageRight, banditStruct->playerID);
                playerPositions->hits[banditStruct->playerID - 65]++;
                fprintf(stderr, "%c targets %c who has %d hits\n",
                        banditStruct->playerID, targetID, 
                        playerPositions->hits[targetID - 65]);
                printf("target.long%c\n", targetID);
            }
        }
        if(banditStruct->currentCarriage == gameStruct->totalCarriages - 1) {
            carriageLeft = 
                playerPositions->bottomLevel[banditStruct->currentCarriage - 1];
            if(carriageLeft != emptyString) {
                targetID = lowest_ID(carriageLeft, banditStruct->playerID);
                playerPositions->hits[banditStruct->playerID - 65]++;
                fprintf(stderr, "%c targets %c who has %d hits\n",
                        banditStruct->playerID, targetID, playerPositions->hits[targetID - 65]);
                printf("target.long%c\n", targetID);
            }
        }
        carriageRight = playerPositions->bottomLevel[banditStruct->currentCarriage + 1];
        carriageLeft = playerPositions->bottomLevel[banditStruct->currentCarriage - 1];
        if(carriageLeft != emptyString) {
            targetIDLeft = lowest_ID(carriageLeft, banditStruct->playerID);
        }
        if(carriageRight != emptyString) {
            targetIDRight = lowest_ID(carriageRight, banditStruct->playerID);
        }
        if(carriageLeft == emptyString && carriageRight != emptyString) {
            playerPositions->hits[banditStruct->playerID - 65]++;
            fprintf(stderr, "%c targets %c who has %d hits\n",
                    banditStruct->playerID, targetIDRight, playerPositions->hits[targetIDRight - 65]);
            printf("target.long%c\n", targetIDRight);
        }
        if(carriageRight == emptyString && carriageLeft != emptyString) {
            playerPositions->hits[banditStruct->playerID - 65]++;
            fprintf(stderr, "%c targets %c who has %d hits\n",
                    banditStruct->playerID, targetIDLeft, playerPositions->hits[targetIDLeft - 65]);
            printf("target.long%c\n", targetIDLeft);
        }
        if(carriageLeft != emptyString && carriageRight != emptyString) {
            if(targetIDLeft < targetIDRight) {
                targetID = targetIDLeft;
            } else {
                targetID = targetIDRight;
            }
            playerPositions->hits[banditStruct->playerID - 65]++;
            fprintf(stderr, "%c targets %c who has %d hits\n",
                    banditStruct->playerID, targetID, playerPositions->hits[targetID - 65]);
            printf("target.long%c\n", targetID);
        }
        if(carriageLeft == emptyString && carriageRight == emptyString) {
            playerPositions->hits[banditStruct->playerID - 65]++;
            fprintf(stderr, "%c has no target\n", banditStruct->playerID);
            printf("target.long-\n");
        }
    } else {
        targetIDLeft = '0';
        targetIDRight = '0';
        for(i = banditStruct->currentCarriage + 1; i < gameStruct->totalCarriages - 1; i++) {
            if(playerPositions->topLevel[i] != emptyString) {
                targetIDRight = lowest_ID(playerPositions->topLevel[i], banditStruct->playerID);
                break;
            }
        }
        for(j = banditStruct->currentCarriage - 1; j > -1; j--) {
            if(playerPositions->topLevel[j] != emptyString) {
                targetIDLeft = lowest_ID(playerPositions->topLevel[j], banditStruct->playerID);
                break;
            }
        }
        if(targetIDLeft != '0' && targetIDRight == '0') {
            playerPositions->hits[banditStruct->playerID - 65]++;
            fprintf(stderr, "%c targets %c who has %d hits\n",
                    banditStruct->playerID, targetIDLeft, playerPositions->hits[targetIDLeft - 65]);
            printf("target.long%c\n", targetIDLeft);
        }
        if(targetIDLeft == '0' && targetIDRight != '0') {
            playerPositions->hits[banditStruct->playerID - 65]++;
            fprintf(stderr, "%c targets %c who has %d hits\n",
                    banditStruct->playerID, targetIDRight, playerPositions->hits[targetIDRight - 65]);
            printf("target.long%c\n", targetIDRight);
        }
        if(targetIDLeft != '0' && targetIDRight != '0') {
            if(abs(banditStruct->currentCarriage - i) > abs(banditStruct->currentCarriage - j)) {
                targetID = targetIDLeft;
            } 
            if(abs(banditStruct->currentCarriage - i) < abs(banditStruct->currentCarriage - j)) {
                targetID = targetIDRight;
            }
            if(abs(banditStruct->currentCarriage - i) == abs(banditStruct->currentCarriage - j)) {
                if(targetIDLeft > targetIDRight) {
                    targetID = targetIDRight;
                } else {
                    targetID = targetIDLeft;
                }
            }
            playerPositions->hits[banditStruct->playerID - 65]++;
            fprintf(stderr, "%c targets %c who has %d hits\n",
                    banditStruct->playerID, targetID, playerPositions->hits[targetID - 65]);
            printf("target.long%c\n", targetID);
        }
        if(targetIDLeft == '0' && targetIDRight == '0') {
            fprintf(stderr, "%c has no target\n", banditStruct->playerID);
            printf("target.long-\n");
        }
    }
}

/*
This function simply checks if a long is available.
*/
int long_check(PlayerPositions* playerPositions, BanditStruct* banditStruct, 
        GameStruct* gameStruct) {
    char* carriageLeft;
    char* carriageRight;
    char* emptyString = "";
    int i, j;
    if(banditStruct->carriageLevel == 0) {
        if(banditStruct->currentCarriage == 0) {
            carriageRight = playerPositions->bottomLevel[banditStruct->currentCarriage + 1];
            if(carriageRight != emptyString) {
                return 1;
            }
        }
        if(banditStruct->currentCarriage == gameStruct->totalCarriages - 1) {
            carriageLeft = playerPositions->bottomLevel[banditStruct->currentCarriage - 1];
            if(carriageLeft != emptyString) {
                return 1;
            }
        }
        carriageRight = playerPositions->bottomLevel[banditStruct->currentCarriage + 1];
        carriageLeft = playerPositions->bottomLevel[banditStruct->currentCarriage - 1];
        if(carriageLeft != emptyString) {
            return 1;
        }
        if(carriageRight != emptyString) {
            return 1;
        }
    } else {
        for(i = banditStruct->currentCarriage + 1; i < gameStruct->totalCarriages - 1; i++) {
            if(playerPositions->topLevel[i] != emptyString) {
                return 1;
            }
        }
        for(j = banditStruct->currentCarriage - 1; j > -1; j--) {
            if(playerPositions->topLevel[j] != emptyString) {
                return 1;
            }
        }
    }
    return 0;
}

int vertical_check(GameStruct* gameStruct, CarriageDetails* carriageDetails,
        BanditStruct* banditStruct) {
    fprintf(stderr, "Certial func\n");
    int bottomSum = 0, topSum = 0, i;
    for(i = 0; i < gameStruct->totalCarriages; i++) {
        topSum += carriageDetails->topLevel[i];
        bottomSum += carriageDetails->bottomLevel[i];
    }
    //topSum += carriageDetails->topLevel[banditStruct->carriageLevel];
    //bottomSum += carriageDetails->bottomLevel[banditStruct->carriageLevel];
    //TODO
    //fprintf(stderr, "CHILD(%c) TopSum: %d Bottom Sum: %d\n", banditStruct->playerID, topSum, bottomSum);
    //fprintf(stderr, "CHILD (%c) carriage level is: %d\n", banditStruct->playerID,
    //    banditStruct->carriageLevel);
    if(banditStruct->carriageLevel == 0) {
        if(topSum > bottomSum) {
            return 1;
        }
    } else if(banditStruct->carriageLevel == 1){
        if(bottomSum > topSum) {
            return 1;
        }
    }
    return 0;
}