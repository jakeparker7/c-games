#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "common.h"


typedef struct {
    char playerID;
    int loot;
    int currentCarriage;
    int carriageLevel;
    char move;
    char moveDirection;
} AcrophobeStruct;

int move_horrizontal(CarriageDetails* carriageDetails, 
        AcrophobeStruct* acrophobeStruct,
        GameStruct* gameStruct, PlayerPositions* playerPositions);
void acrophobe_decider(char* readString, 
        CarriageDetails* carriageDetails, AcrophobeStruct* acrophobeStruct,
        GameStruct* gameStruct, PlayerPositions* playerPositions);
char move_controller(CarriageDetails* carriageDetails, GameStruct* gameStruct, 
        AcrophobeStruct* acrophobeStruct,
        PlayerPositions* playerPositions);
int move_loot(CarriageDetails* carriageDetails, 
        AcrophobeStruct* acrophobeStruct,
        PlayerPositions* playerPositions);
int move_horrizontal(CarriageDetails* carriageDetails, 
        AcrophobeStruct* acrophobeStruct,
        GameStruct* gameStruct, PlayerPositions* playerPositions);

/*
The main function of the acrohpobe child, handling everything in a constant
while true loop until it errors, dires out or receives game.over.
*/
int main(int argc, char** argv) {
    error_controller(argc, argv);
    AcrophobeStruct* acrophobeStruct = malloc(sizeof(AcrophobeStruct));
    GameStruct* gameStruct = player_init(argc, argv);
    CarriageDetails* carriageDetails = loot_distributor(gameStruct);
    PlayerPositions* playerPositions = player_position_init(gameStruct);
    int playerInteger = atoi(argv[2]);
    acrophobeStruct->playerID = playerInteger + 65;
    acrophobeStruct->currentCarriage = playerInteger % 
            gameStruct->totalCarriages;
    acrophobeStruct->carriageLevel = 0;
    char* readString;
    acrophobeStruct->moveDirection = '-';
    while(1) {
        readString = read_standard_in();
        acrophobe_decider(readString, carriageDetails, acrophobeStruct, 
                gameStruct,
                playerPositions);
    }
}

/*
Acrophobe Decider is a function that reads the incoming string from the
parent and handles it correctly. It takes in both the string and various
required structs.
*/
void acrophobe_decider(char* readString, 
        CarriageDetails* carriageDetails, AcrophobeStruct* acrophobeStruct,
        GameStruct* gameStruct, PlayerPositions* playerPositions) {
    char* yourTurn = "yourturn";
    char* horrizontalMessage = "h?";
    char* execute = "execute";
    char endString;
    char playerID, move;
    check_hub_message(readString, gameStruct);
    if(strcmp(readString, yourTurn) == 0) {
        move = move_controller(carriageDetails, gameStruct, acrophobeStruct,
                playerPositions);
        acrophobeStruct->move = move;
        printf("play%c\n", move);
        fflush(stdout);
        return;
    }
    if(sscanf(readString, "looted%c%c", &playerID, &endString) == 1) {
        if(playerID != acrophobeStruct->playerID) {
            looted(playerPositions, gameStruct, carriageDetails, playerID);
            return;
        }
        if(playerID == acrophobeStruct->playerID) {
            move_loot(carriageDetails, acrophobeStruct, playerPositions);
        }
        return;
    }
    if(sscanf(readString, "vmove%c%c", &playerID, &endString) == 1) {
        if(playerID != acrophobeStruct->playerID) {
            vmove_player(playerPositions, gameStruct, playerID);
            return;
        }
        return;
    }
    if(strcmp(readString, execute) == 0) {
        return;
    }
    if(strcmp(readString, horrizontalMessage) == 0) {
        move_horrizontal(carriageDetails, acrophobeStruct,
                gameStruct, playerPositions);
        return;
    }
    if(other_players_move_handler(readString, carriageDetails, gameStruct,
            playerPositions, acrophobeStruct->playerID) == 1) {
        return;
    } 
    
    
    fprintf(stderr, "Communication Error\n");
    exit(6);
    
}

/*
Move Controller is a function that determines the next move played by the
acrophobe player. The acrophobe player follows the requirements of loot,
then move horrizontally.
*/
char move_controller(CarriageDetails* carriageDetails, GameStruct* gameStruct, 
        AcrophobeStruct* acrophobeStruct,
        PlayerPositions* playerPositions) {
    if(acrophobeStruct->carriageLevel == 0) {
        //Check for Loot
        if(carriageDetails->bottomLevel[
                acrophobeStruct->currentCarriage] > 0) {
            return '$';
        }
        return 'h';
    } else {
        //Check for Loot
        if(carriageDetails->topLevel[acrophobeStruct->currentCarriage] > 0) {
            return '$';
        }
        return 'h';
    }
}

/*
Move loot is a function that simply picks up loot if it is available. It then
adjusts all other dependent arrays so that there a
*/
int move_loot(CarriageDetails* carriageDetails, 
        AcrophobeStruct* acrophobeStruct,
        PlayerPositions* playerPositions) {
    if(acrophobeStruct->carriageLevel == 1) {
        if(carriageDetails->topLevel[acrophobeStruct->currentCarriage] > 0) {
            playerPositions->loot[acrophobeStruct->playerID - 65]++;
            carriageDetails->topLevel[acrophobeStruct->currentCarriage] -= 1;
            fprintf(stderr, "%c picks up loot (they now have %d)\n", 
                    acrophobeStruct->playerID,
                    playerPositions->loot[acrophobeStruct->playerID - 65]);
            return 1;
        } 
    } 
    if (acrophobeStruct->carriageLevel == 0) {
        if(carriageDetails->bottomLevel[
                acrophobeStruct->currentCarriage] > 0) {
            playerPositions->loot[acrophobeStruct->playerID - 65]++;
            carriageDetails->bottomLevel[acrophobeStruct->currentCarriage] 
                    -= 1;
            fprintf(stderr, "%c picks up loot (they now have %d)\n", 
                    acrophobeStruct->playerID,
                    playerPositions->loot[acrophobeStruct->playerID - 65]);
            return 1;
        }
    }
    fprintf(stderr, "%c tries to pick up loot but there isn't any\n", 
            acrophobeStruct->playerID);
    return 0;
}

/*
Move horrizontal function follows the acrophobe algorithim with regards to
 moving
the correct horrizontal direection. It then prints this direction out to the 
parent.
*/
int move_horrizontal(CarriageDetails* carriageDetails, 
        AcrophobeStruct* acrophobeStruct,
        GameStruct* gameStruct, PlayerPositions* playerPositions) {
    if(acrophobeStruct->currentCarriage == 0) {
        acrophobeStruct->moveDirection = '+';
    }
    if(acrophobeStruct->currentCarriage == gameStruct->totalCarriages - 1) {
        acrophobeStruct->moveDirection = '-';
    }
    if(acrophobeStruct->moveDirection == '-') {
        remove_player(playerPositions, gameStruct, acrophobeStruct->playerID, 
                acrophobeStruct->currentCarriage, 
                acrophobeStruct->carriageLevel);
        acrophobeStruct->currentCarriage--;
        playerPositions->bottomLevel[acrophobeStruct->currentCarriage] = 
                append(playerPositions->bottomLevel[
                acrophobeStruct->currentCarriage], 
                acrophobeStruct->playerID);
        fprintf(stderr, "%c moved to %d/%d\n", acrophobeStruct->playerID, 
                acrophobeStruct->currentCarriage, 
                acrophobeStruct->carriageLevel);
        fprintf(stdout, "sideways-\n");
        fflush(stdout);
        return 1;
    } else {
        remove_player(playerPositions, gameStruct, acrophobeStruct->playerID, 
                acrophobeStruct->currentCarriage, 
                acrophobeStruct->carriageLevel);
        acrophobeStruct->currentCarriage++;
        playerPositions->bottomLevel[acrophobeStruct->currentCarriage] = 
                append(playerPositions->bottomLevel[
                acrophobeStruct->currentCarriage], acrophobeStruct->playerID);
        fprintf(stderr, "%c moved to %d/%d\n", acrophobeStruct->playerID, 
                acrophobeStruct->currentCarriage, 
                acrophobeStruct->carriageLevel);
        fprintf(stdout, "sideways+\n");
        fflush(stdout);
        return 1;
    }
}

