#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <ctype.h>
#include <signal.h>
#include <sys/wait.h>

#define READ 0 //Reading end of pipe
#define WRITE 1 //Writing end of pipe
#define GAMEOVER 10 //Amount for game.over write
#define ROUND 6 //Amount for round write
#define YOURTURN 9 //Amount for yourTurn write

typedef enum {
    gameOver,
    roundStart,
    yourTurn,
    execute,
    horrizontalMove,
    shortTarget,
    longTarget,
    hmove,
    vmove,
    longed,
    shorted,
    looted
} HubMessage;


typedef struct {
    int* topLevel;
    int* bottomLevel;
} CarriageDetails;

typedef struct {
    int seed;
    int carriages;
    int totalPlayers;
    int roundNumber;
    char** playerMoveArray;
    int** childInputArray;
} GameStruct;


typedef struct {
    int playerID;
    int currentCarriage;
    int carriageLevel;
    int loot;
    int hits;
    int pid;
} PlayerStruct;

void error_controller(int argc, char** argv);
GameStruct* game_struct_init(char* seed, char* carriages, char* totalPlayers);
CarriageDetails* loot_distributor(GameStruct* gameStruct);
PlayerStruct* player_init(GameStruct* gameStruct);
void write_to_child(HubMessage hubMessage, GameStruct* gameStruct, PlayerStruct* playerStruct, 
        CarriageDetails* carriageDetails, int position);
void end_of_round_summary(PlayerStruct* playerStruct, 
        CarriageDetails* carriageDetails, GameStruct* gameStruct);
char read_move_character(GameStruct* gameStruct, PlayerStruct* playerStruct, 
        int* childInput);
void read_error(char move, int errorCase, GameStruct* gameStruct, 
        PlayerStruct* playerStruct);
int read_player_decision(GameStruct* gameStruct, PlayerStruct* playerStruct, 
        CarriageDetails* carriageDetails, int* childInput);
void move_handler(PlayerStruct* playerStruct, GameStruct* gameStruct, 
        CarriageDetails* carriageDetails);
int loot_check(PlayerStruct* playerStruct, CarriageDetails* carriageDetails);
void signal_handler(int singal);
void find_winner(GameStruct* gameStruct, PlayerStruct* playerStruct);
char* append(char* appendedString, char appendedCharacter);

//These are two global structs, containing all game and player details.
//These were made global for handling signal calls
GameStruct* gameStruct;
PlayerStruct* playerStruct;

/*
The main of the Hub consists of all forking, aswell as the child's
entire gameplay, including write order, a round while and
required reads.
*/
int main(int argc, char** argv) {
    signal(SIGINT, signal_handler);
    signal(SIGPIPE, signal_handler);
    error_controller(argc, argv);
    int pid = 0, i, status;
    char totalPlayers[3], playerID[3];
    sprintf(totalPlayers, "%d", argc - 3);
    char* carriages = argv[2];
    char* seed = argv[1];
    GameStruct* gameStruct = game_struct_init(seed, carriages, totalPlayers);
    PlayerStruct* playerStruct = player_init(gameStruct);
    CarriageDetails* carriageDetails = loot_distributor(gameStruct);
    for(i = 0; i < atoi(totalPlayers); i++) {
        int childInput[2];
        int childOutput[2];
        if(pipe(childInput) == -1 || pipe(childOutput) == -1) {
            fprintf(stderr, "Bad start\n");
            exit(3);
        }
        if((pid = fork()) == -1) {
            fprintf(stderr, "Bad start\n");
            exit(3);
        }
        if(pid == 0) {
            sprintf(playerID, "%d", i);
            dup2(childInput[READ], STDIN_FILENO); 
            close(childInput[WRITE]);
            dup2(childOutput[WRITE], STDOUT_FILENO);
            close(childOutput[READ]);
            char* args[] = {argv[i + 3], totalPlayers, playerID, carriages, seed, NULL};
            close(STDERR_FILENO);
            execvp(args[0], args);
            fprintf(stderr, "Bad start");
            exit(3);
        }
        char buffer[1024];
        close(childInput[READ]);
        close(childOutput[WRITE]); /* Close the unused end */
        playerStruct[i].playerID = i;
        gameStruct->childInputArray[i][WRITE] = childInput[WRITE];
        gameStruct->childInputArray[i][READ] = childOutput[READ];
        playerStruct[i].pid = pid;

        memset(buffer, '\0', 1024);
        read(gameStruct->childInputArray[i][READ], buffer, 1);
        if(buffer[0] != '!' && strlen(buffer) != 1) {
            fprintf(stderr, "Bad start\n");
            exit(3);
        }
    }
    gameStruct->roundNumber = 15;
    while(gameStruct->roundNumber != 0) {
        write_to_child(roundStart, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(yourTurn, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(execute, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(hmove, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(vmove, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(longed, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(shorted, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(looted, gameStruct, playerStruct, carriageDetails, 0);
        end_of_round_summary(playerStruct, carriageDetails, gameStruct);
        gameStruct->roundNumber--;
    }
    write_to_child(gameOver, gameStruct, playerStruct, carriageDetails, 0);

    for(i = 0; i < gameStruct->totalPlayers; i++) {
        waitpid(playerStruct[i].pid, &status, 0);
        if(status != 0) {
            fprintf(stderr, "Player %c ended with status %d\n", 
                    playerStruct[i].playerID, status);
        }
    }
    find_winner(gameStruct, playerStruct);
    return 0;
}

/*
This function reads the move selected by each player. It will then
carry out this move. Some moves occur instantly, while others
will result in a reply from the hub.
*/
void move_handler(PlayerStruct* playerStruct, GameStruct* gameStruct, 
        CarriageDetails* carriageDetails) {
    int i;
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        if(gameStruct->playerMoveArray[i][0] == '$') {
            if(loot_check(&playerStruct[i], carriageDetails) == 1) {
                continue;
            }
            if(playerStruct[i].carriageLevel == 1) {
                carriageDetails->topLevel[playerStruct[i].currentCarriage] 
                	-= 1;
            } else {
                carriageDetails->bottomLevel[playerStruct[i].currentCarriage] 
                	-= 1;
            }
            playerStruct[i].loot++;
        }
        if(gameStruct->playerMoveArray[i][0] == 'v') {
            playerStruct[i].carriageLevel = (playerStruct[i].carriageLevel + 1)
            	 % 2;
            write_to_child(vmove, gameStruct, playerStruct, carriageDetails, 
            		0);
        }
        if(gameStruct->playerMoveArray[i][0] == 'h') {
            write_to_child(horrizontalMove, gameStruct, playerStruct, 
            	carriageDetails, i);
        }
        if(gameStruct->playerMoveArray[i][0] == 's') {
            write_to_child(shortTarget, gameStruct, playerStruct, 
            	carriageDetails, i);
        }
        if(gameStruct->playerMoveArray[i][0] == 'l') {
            write_to_child(longTarget, gameStruct, playerStruct, 
            	carriageDetails, i);
        }
    }
}

/*
Write to child is a large enumerated switch statement that controls the
majority of the Hub's writes to the child. 
*/
void write_to_child(HubMessage hubMessage, GameStruct* gameStruct, 
		PlayerStruct* playerStruct,
        CarriageDetails* carriageDetails, int position) {
    int i, j;
    char move;
    switch(hubMessage) {
        case gameOver:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                write(gameStruct->childInputArray[i][WRITE], 
                		"game_over\n", GAMEOVER);
            }
            break;
        case roundStart:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                write(gameStruct->childInputArray[i][WRITE], 
                		"round\n", ROUND);
            }
            break;
        case yourTurn:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
            	if(playerStruct[i].playerID > 3) {
            		char buffer1[8];
                    memset(buffer1, '\0', 8);
                    snprintf(buffer1, 8, "driedout%c\n", 
                    		playerStruct[i].playerID + 65);
                    write(gameStruct->childInputArray[i][WRITE], 
                    		buffer1, strlen(buffer1));
                    continue;
            	}
                write(gameStruct->childInputArray[i][WRITE], 
                		"yourturn\n", YOURTURN);
                move = read_move_character(gameStruct, &(playerStruct[i]), 
                        gameStruct->childInputArray[i]);
                gameStruct->playerMoveArray[i][0] = move;
                for(int j = 0; j < gameStruct->totalPlayers; j++) {
                    char buffer[11];
                    memset(buffer, '\0', 11);
                    snprintf(buffer, 11, "ordered%c%c\n", 
                    		playerStruct[i].playerID + 65, 
                            gameStruct->playerMoveArray[i][0]);
                    write(gameStruct->childInputArray[j][WRITE], 
                    		buffer, strlen(buffer));
                }
            }
            break;
        case execute:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                write(gameStruct->childInputArray[i][WRITE], 
                		"execute\n", strlen("execute\n"));
            }
            move_handler(playerStruct, gameStruct, carriageDetails);
            break;
        case horrizontalMove:
            write(gameStruct->childInputArray[position][WRITE], "h?\n", 3);
            read_player_decision(gameStruct, &playerStruct[position], 
            		carriageDetails,
                    gameStruct->childInputArray[position]);
            break;
        case shortTarget:
            write(gameStruct->childInputArray[position][WRITE], "s?\n", 3);
            read_player_decision(gameStruct, &playerStruct[position], 
            		carriageDetails,
                    gameStruct->childInputArray[position]);
            break;
        case longTarget:
            write(gameStruct->childInputArray[position][WRITE], "l?\n", 3);
            read_player_decision(gameStruct, &playerStruct[position], 
            		carriageDetails,
                    gameStruct->childInputArray[position]);
            break;
        case hmove:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                if(gameStruct->playerMoveArray[i][0] == 'h') {
                    for(j = 0; j < gameStruct->totalPlayers; j++) {
                        char buffer[9];
                        memset(buffer, '\0', 9);
                        snprintf(buffer, 9, "hmove%c%c\n", 
                        		playerStruct[i].playerID + 65,
                                gameStruct->playerMoveArray[i][1]);
                        write(gameStruct->childInputArray[j][WRITE], buffer, 
                        		strlen(buffer));
                    }
                }
            }
            break;
        case vmove:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                if(gameStruct->playerMoveArray[i][0] == 'v') {
                    for(j = 0; j < gameStruct->totalPlayers; j++) {
                        char buffer[9];
                        memset(buffer, '\0', 9);
                        snprintf(buffer, 9, "vmove%c\n", 
                        		playerStruct[i].playerID + 65);
                        write(gameStruct->childInputArray[j][WRITE], 
                        		buffer, strlen(buffer));
                    }
                }
            }
            break;
        case longed:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                if(gameStruct->playerMoveArray[i][0] == 'l') {
                    for(j = 0; j < gameStruct->totalPlayers; j++) {
                        char buffer[8];
                        memset(buffer, '\0', 8);
                        snprintf(buffer, 8, "long%c%c\n", playerStruct[i].playerID + 65,
                                gameStruct->playerMoveArray[i][1]);
                        write(gameStruct->childInputArray[j][WRITE], buffer, strlen(buffer));

                    }
                }
            }
        case shorted:
        	for(i = 0; i < gameStruct->totalPlayers; i++) {
                if(gameStruct->playerMoveArray[i][0] == 's') {
                    for(j = 0; j < gameStruct->totalPlayers; j++) {
                    	fprintf(stderr, "Sending short short%c%c\n", playerStruct[i].playerID + 65,
                                gameStruct->playerMoveArray[i][1]);
                        char buffer[9];
                        memset(buffer, '\0', 9);
                        snprintf(buffer, 9, "short%c%c\n", playerStruct[i].playerID + 65,
                                gameStruct->playerMoveArray[i][1]);
                        write(gameStruct->childInputArray[j][WRITE], buffer, strlen(buffer));
                    }
                }
            }
            break;
        case looted:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                if(gameStruct->playerMoveArray[i][0] == '$') {
                    for(j = 0; j < gameStruct->totalPlayers; j++) {
                        char buffer[9];
                        memset(buffer, '\0', 9);
                        snprintf(buffer, 9, "looted%c\n", playerStruct[i].playerID + 65);
                        write(gameStruct->childInputArray[j][WRITE], buffer, strlen(buffer));
                    }
                }
            }
            break;
        default:
            fprintf(stderr, "Cant get here\n");
    }
}

/*
This function prints the end of round summary. Showing all players
ID's, hits and loot. Followed by the loot available at each
carriage.
*/    
void end_of_round_summary(PlayerStruct* playerStruct, 
        CarriageDetails* carriageDetails, GameStruct* gameStruct) {
    int i;
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        char playerID = playerStruct[i].playerID + 65;
        int currentCarriage = playerStruct[i].currentCarriage;
        int carriageLevel = playerStruct[i].carriageLevel;
        int loot = playerStruct[i].loot;
        int hits = playerStruct[i].hits;
        fprintf(stdout, "%c@(%d,%d): $=%d hits=%d\n", playerID, 
                currentCarriage, carriageLevel, loot, hits);
    }
    for(i = 0; i < gameStruct->carriages; i++) {
    	fprintf(stdout, "Carriage %d: $=%d : $=%d\n", i, 
                carriageDetails->bottomLevel[i], carriageDetails->topLevel[i]);
        fflush(stdout);
    }
}

/*
This function reads the players selected move. It stores this move and 
checks the move for any specific errorbad*/
char read_move_character(GameStruct* gameStruct, PlayerStruct* playerStruct, 
        int* childInput) {
    char buffer[100], move;
    memset(buffer, '\0', 100);
    int readBytes = read(childInput[READ], buffer, sizeof(buffer) - 1);
    //fprintf(stderr, "Move buffer %s\n", buffer);
    if(readBytes == 0) {
    	fprintf(stderr, "Client disconnected\n");
    	exit(4);
    }
    if(readBytes != 0 && sscanf(buffer, "play%c", &move) == 1) {
        read_error(move, 0, gameStruct, playerStruct);
        return move;
    }
    return 0;
}

/*
Read player decision is a function that reads the players reply once
askedh?, l? or s?. It reads the players reply and adjust all relevant
structs accordingly.
*/
int read_player_decision(GameStruct* gameStruct, PlayerStruct* playerStruct, 
        CarriageDetails* carriageDetails, int* childInput) {
    char buffer[100], move;
    int playerID, readBytes;
    memset(buffer, '\0', 100);
   readBytes = read(childInput[READ], buffer, sizeof(buffer) - 1);
   //fprintf(stderr, "Player decision buffer %s\n", buffer);
   if(readBytes == 0) {
    	fprintf(stderr, "Client disconnected\n");
    	exit(4);
    }
    if(sscanf(buffer, "sideways%c", &move) == 1) {
        read_error(move, 1, gameStruct, playerStruct);
        gameStruct->playerMoveArray[playerStruct->playerID][1] = move;
        if(move == '+') {
            playerStruct->currentCarriage++;
        } else {
            playerStruct->currentCarriage--;
        }
        return 1;
    }
    if(sscanf(buffer, "target.short%c", &move) == 1) {
    	//fprintf(stdout, "Before Targeting player %d they have %d loot\n", 2, playerStruct[2].loot);
     
        read_error(move, 2, gameStruct, playerStruct);
        //fprintf(stdout, "After Targeting player %d they have %d loot\n", 2, playerStruct[2].loot);
     
        //fprintf(stderr, "In target short aftter read err\n");
        gameStruct->playerMoveArray[playerStruct->playerID][1] = move;
        if(move != '-') {
        	//fprintf(stderr, "Withing target short if\n");
            //playerID = move - 65;
            //fprintf(stderr, "playerId %d", playerID);
            //fprintf(stderr, "Before Loot for player %d\n", playerStruct[playerID].loot);
            //fprintf(stdout, "Targeting player %d they have %d loot\n", playerStruct->playerID, playerStruct->loot);
            //fflush(stdout);
            if (playerStruct->loot > 0) {
	            playerStruct->loot--;
	            //fprintf(stderr, "After Loot for player %d\n", playerStruct[playerID].loot);

	            if(playerStruct->carriageLevel == 1) {
	            	//fprintf(stderr, "Before CarriageDetails T %d\n", carriageDetails->topLevel[playerStruct->currentCarriage]);
	                carriageDetails->topLevel[playerStruct->currentCarriage]++;
	          	} else {
	            	//fprintf(stderr, "Before CarriageDetails B %d\n", carriageDetails->bottomLevel[playerStruct->currentCarriage]);
	                carriageDetails->bottomLevel[playerStruct->currentCarriage]++;
	            }
	        }
            return 1;
        }
    }
    if(sscanf(buffer, "target.long%c", &move) == 1) {
        read_error(move, 2, gameStruct, playerStruct);
        gameStruct->playerMoveArray[playerStruct->playerID][1] = move;
        if(move != '-') {
            playerID = move - 65;
            playerStruct[playerID].hits++;
        }
        return 1;
    }
    return 0;
}

/*
This function checks if the child (client) has replied with 
appropriate move commands. If it has not it will prent to 
standard error and exit with a specified status.
*/
void read_error(char move, int errorCase, GameStruct* gameStruct, 
        PlayerStruct* playerStruct) {
    int moveInteger = move - 65;
    switch(errorCase) {
        case 0:
            if(move != 'v' && move != 'l' && move != 'h' && move != 's' 
                    && move != '$') {
                fprintf(stderr, "Protocol error by client\n");
                exit(6);
            }
            break;
        case 1:
            if(move != '+' && move != '-') {
                fprintf(stderr, "Protocol error by client\n");
                exit(5);
            }
            break;
        case 2:
            if(moveInteger < 0 || moveInteger > gameStruct->totalPlayers - 1) {
                fprintf(stderr, "Protocol error by client\n");
                exit(5);
            }
            break;
        default:
            break;
    }
}

/*
This function initialises the CarriageDetails struct, this struct holds
an array of the top level and the bottom level values of loot. It 
also runs the loot formulae of the game and successfully allocates the
loot.
*/
CarriageDetails* loot_distributor(GameStruct* gameStruct) {
    //initialising Carriage Details
    CarriageDetails* carriageDetails = malloc(sizeof(CarriageDetails));
    carriageDetails->topLevel = (int*)malloc(sizeof(int) * gameStruct->carriages);
    carriageDetails->bottomLevel = (int*)malloc(sizeof(int) * gameStruct->carriages);

    for(int i = 0; i < gameStruct->carriages; i++) {
    	carriageDetails->topLevel[i] = 0;
    	carriageDetails->bottomLevel[i] = 0;
    }
    int totalLoot = ((gameStruct->seed % 4) + 1) * gameStruct->carriages;
    int lootCarriage = ceil(gameStruct->carriages / 2.0);
    //Y = 0 here so bottom level
    int carriageLevel = 0;
    carriageDetails->bottomLevel[lootCarriage]++;
    totalLoot--;
    while(totalLoot != 0) {
        lootCarriage = (lootCarriage + (gameStruct->seed % 101)) % gameStruct->carriages;
        if((carriageLevel = (carriageLevel + (gameStruct->seed % 2)) % 2) == 0) {
            carriageDetails->bottomLevel[lootCarriage]++;
        } else {
            carriageDetails->topLevel[lootCarriage]++;
        }
        totalLoot--;
    }
    return carriageDetails;
}

/*
Function to initalise the game structure, making use of variables globally easier
*/
GameStruct* game_struct_init(char* seed, char* carriages, char* totalPlayers) {
    int i;
    char** playerMoveArray;
    int** childInputArray;
    GameStruct* gameStruct = malloc(sizeof(GameStruct));
    gameStruct->seed = atoi(seed);
    gameStruct->carriages = atoi(carriages);
    gameStruct->totalPlayers = atoi(totalPlayers);
    gameStruct->playerMoveArray = (char**)malloc(sizeof(char*) * gameStruct->totalPlayers);
    playerMoveArray = gameStruct->playerMoveArray;
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        playerMoveArray[i] = (char*)malloc(sizeof(char) * gameStruct->totalPlayers);
    }
    gameStruct->childInputArray = (int**)malloc(sizeof(int*) * gameStruct->totalPlayers);
    childInputArray = gameStruct->childInputArray;
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        childInputArray[i] = (int*)malloc(sizeof(int) * gameStruct->totalPlayers);
    }
    return gameStruct;
}

/*
This functinon initialises the player struct and sets all of the players to
their allocated positions.
*/
PlayerStruct* player_init(GameStruct* gameStruct) {
    PlayerStruct* playerStruct 
            = (PlayerStruct*)malloc(sizeof(PlayerStruct) * gameStruct->totalPlayers);
    int i;
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        playerStruct[i].carriageLevel = 0;
        playerStruct[i].currentCarriage = i % gameStruct->carriages;
    }
    return playerStruct;
}

/*
This function is the error handler for the hub, it checks for issues and
if found prints the appropriate statement to stderr and exits with a
specified status.
*/
void error_controller(int argc, char** argv) {
    int i;
    if(argc < 5) {
        fprintf(stderr, "Usage: hub seed width player player [player ...]\n");
        exit(1);
    }
    for(i = 0; i < strlen(argv[1]); i++) {
        if(isdigit(argv[1][i]) == 0) {
            fprintf(stderr, "Bad argument\n");
            exit(2);
        }
    }
    for(i = 0; i < strlen(argv[2]); i++) {
        if(isdigit(argv[2][i]) == 0) {
            fprintf(stderr, "Bad argument\n");
            exit(2);
        }
    }
    if(atoi(argv[2]) < 3) {
        fprintf(stderr, "Bad argument\n");
        exit(2);
    }
}

/*
This function checks if there is loot available at the current player position.
*/
int loot_check(PlayerStruct* playerStruct, CarriageDetails* carriageDetails) {
    if(playerStruct->carriageLevel == 1) {
        if(carriageDetails->topLevel[playerStruct->currentCarriage] == 0) {
            return 1;
        }
    } else {
        if(carriageDetails->bottomLevel[playerStruct->currentCarriage] == 0) {
            return 1;
        }
    }
    return 0;
}

/*
This is a signal handler function to handle a potential SIGINT
*/
void signal_handler(int signal) {
    int i;
    if(signal == SIGINT) {
        for(i = 0; i < gameStruct->totalPlayers; i++) {
            kill(playerStruct[i].playerID, SIGINT);
        }
        fprintf(stderr, "SIGINT caught\n");
        exit(9);
    }
    if (signal == SIGPIPE) {
    	// Ignore
    }
    if(signal == SIGCHLD) {
    	int status;
    	int pid = waitpid(-1, &status, WNOHANG);
    	for(i = 0; i < gameStruct->totalPlayers; i++) {
    		if(pid == playerStruct[i].pid) {
    			fprintf(stderr, 
    					"Player %c shutdown after receiving singal %d\n", 
    					playerStruct[i].playerID, status);
    		}
    	}
    	
    }
}

/*
This function is to find who has won the game and return the winner.
*/
void find_winner(GameStruct* gameStruct, PlayerStruct* playerStruct) {
	int i;
	int max;
	char* winnersString;
	winnersString = (char*)calloc(sizeof(char) , gameStruct->totalPlayers);
	for(i = 0; i < gameStruct->totalPlayers; i++) {
		max = playerStruct[0].loot;
		if(playerStruct[i].loot > max) {
			max = playerStruct[i].loot;
		}
	}
	for(i = 0; i < gameStruct->totalPlayers; i++) {
		if(playerStruct[i].loot == max) {
			winnersString = append(winnersString, playerStruct[i].playerID + 65);
		}
	}
	printf("Winner(s):%s\n", winnersString);
}

/*
Append is a simple function that appends a character (appendedCharacter)
to a set string (appenedString).
*/
char* append(char* appendedString, char appendedCharacter) {
    int length = strlen(appendedString);
    appendedString[length] = appendedCharacter;
    appendedString[length + 1] = '\0';
    return appendedString;
}