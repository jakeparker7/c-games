typedef struct {
    int seed;
    int totalCarriages;
    int totalPlayers;
    int loot;
    char* playerMoveArray;
} GameStruct;

typedef struct {
    int* topLevel;
    int* bottomLevel;
} CarriageDetails;

typedef struct {
    char** topLevel;
    char** bottomLevel;
    int* loot;
    int* hits;
} PlayerPositions;

void error_controller(int argc, char** argv);
char* read_standard_in(void);
void err_digit_check(int argvValue, char* exitString, int exitStatus, char** argv);
GameStruct* player_init(int argc, char** argv);
CarriageDetails* loot_distributor(GameStruct* gameStruct);
GameStruct* player_init(int argc, char** argv);
PlayerPositions* player_position_init(GameStruct* gameStruct);
char* append(char* appendedString, char appendedCharacter);
void hmove_player(PlayerPositions* playerPositions, GameStruct* gameStruct,
    char playerID, char direction);
char* recreate_string(char* oldString, char playerID, GameStruct* gameStruct);
void remove_player(PlayerPositions* playerPositions, GameStruct* gameStruct,
        char playerID, int currentCariage, int carriageLevel);
char lowest_ID(char* chosenString, char playerID);
char highest_ID(char* chosenString, char playerID);
void vmove_player(PlayerPositions* playerPositions, GameStruct* gameStruct,
        char playerID);
void looted(PlayerPositions* playerPositions, GameStruct* gameStruct, 
            CarriageDetails* carriageDetails, char playerID);
int other_players_move_handler(char* readString, CarriageDetails* carriageDetails, 
        GameStruct* gameStruct, PlayerPositions* playerPositions, char actualPlayerID);
void check_hub_message(char* readString, GameStruct* gameStruct);
int find_player_position_top(char playerID, PlayerPositions* playerPositions, 
        GameStruct* gameStruct);
int find_player_position_bot(char playerID, PlayerPositions* playerPositions,
        GameStruct* gameStruct);
void player_longed(PlayerPositions* playerPositions, char playerID, char targetID, 
        GameStruct* gameStruct);
void player_shorted(PlayerPositions* playerPositions, char playerID, char targetID,
        GameStruct* gameStruct, CarriageDetails* carriageDetails);

