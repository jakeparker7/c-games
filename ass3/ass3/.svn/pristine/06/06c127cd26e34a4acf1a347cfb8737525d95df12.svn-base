#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <ctype.h>
#include <signal.h>
#include <sys/wait.h>

#define READ 0 //Reading end of pipe
#define WRITE 1 //Writing end of pipe
#define GAMEOVER 10 //Amount for game.over write
#define ROUND 6 //Amount for round write
#define YOURTURN 9 //Amount for yourTurn write

typedef enum {
    gameOver,
    roundStart,
    yourTurn,
    execute,
    horrizontalMove,
    shortTarget,
    longTarget,
    hmove,
    vmove,
    longed,
    shorted,
    looted,
    driedOut
} HubMessage;


typedef struct {
    int* topLevel;
    int* bottomLevel;
} CarriageDetails;

typedef struct {
    int seed;
    int carriages;
    int totalPlayers;
    int roundNumber;
    char** playerMoveArray;
    int** childInputArray;
} GameStruct;


typedef struct {
    int playerID;
    int currentCarriage;
    int carriageLevel;
    int loot;
    int hits;
    int pid;
} PlayerStruct;

void error_controller(int argc, char** argv);
GameStruct* game_struct_init(char* seed, char* carriages, char* totalPlayers);
CarriageDetails* loot_distributor(GameStruct* gameStruct);
PlayerStruct* player_init(GameStruct* gameStruct);
void write_to_child(HubMessage hubMessage, GameStruct* gameStruct, PlayerStruct* playerStruct, 
        CarriageDetails* carriageDetails, int position);
void end_of_round_summary(PlayerStruct* playerStruct, 
        CarriageDetails* carriageDetails, GameStruct* gameStruct);
char read_move_character(GameStruct* gameStruct, PlayerStruct* playerStruct, 
        int* childInput);
void read_error(char move, int errorCase, GameStruct* gameStruct, 
        PlayerStruct* playerStruct);
int read_player_decision(GameStruct* gameStruct, PlayerStruct* playerStruct, 
        CarriageDetails* carriageDetails, int* childInput);
void move_handler(PlayerStruct* playerStruct, GameStruct* gameStruct, 
        CarriageDetails* carriageDetails);
int loot_check(PlayerStruct* playerStruct, CarriageDetails* carriageDetails);
void signal_handler(int singal);

GameStruct* gameStruct;
PlayerStruct* playerStruct;

/*
The main of the Hub consists of all forking, aswell as the child's
entire gameplay, including write order, a round while and
required reads.
*/
int main(int argc, char** argv) {
    signal(SIGINT, signal_handler);
    error_controller(argc, argv);
    int pid = 0, i, status;
    char totalPlayers[3], playerID[3];
    sprintf(totalPlayers, "%d", argc - 3);
    char* carriages = argv[2];
    char* seed = argv[1];
    //Initialising the game struct
    GameStruct* gameStruct = game_struct_init(seed, carriages, totalPlayers);

    //Creating array of structs
    PlayerStruct* playerStruct = player_init(gameStruct);
    //Allocating loot
    CarriageDetails* carriageDetails = loot_distributor(gameStruct);
    for(i = 0; i < atoi(totalPlayers); i++) {
        int childInput[2];
        int childOutput[2];
        if(pipe(childInput) == -1 || pipe(childOutput) == -1) {
            fprintf(stderr, "Bad start\n");
            exit(3);
        }
        if((pid = fork()) == -1) {
            fprintf(stderr, "Bad start\n");
            exit(3);
        }
        if(pid == 0) {
            sprintf(playerID, "%d", i);
            dup2(childInput[READ], STDIN_FILENO); 
            close(childInput[WRITE]);
            dup2(childOutput[WRITE], STDOUT_FILENO);
            close(childOutput[READ]);
            char* args[] = {argv[i + 3], totalPlayers, playerID, carriages, seed, NULL};
            execvp(args[0], args);
            fprintf(stderr, "Bad start");
            exit(3);
            //Kill kids
        }
        char buffer[1024];
        close(childInput[READ]);
        close(childOutput[WRITE]); /* Close the unused end */
        playerStruct[i].playerID = i;
        gameStruct->childInputArray[i][WRITE] = childInput[WRITE];
        gameStruct->childInputArray[i][READ] = childOutput[READ];
        playerStruct[i].pid = pid;

        memset(buffer, '\0', 1024);
        read(gameStruct->childInputArray[i][READ], buffer, 1023);
        fprintf(stderr, "First read buffer is: %s\n", buffer);
        if(buffer[0] != '!' && strlen(buffer) != 1) {
            fprintf(stderr, "Bad start\n");
            exit(3);
        }
        
    }
    fflush(stdout);
    gameStruct->roundNumber = 10;
    fprintf(stderr, "FINISHED ALL CHILD SET UP GET READY FOR GAME\n");
    while(gameStruct->roundNumber != 0) {
        write_to_child(roundStart, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(yourTurn, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(execute, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(hmove, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(vmove, gameStruct, playerStruct, carriageDetails, 0);
        write_to_child(looted, gameStruct, playerStruct, carriageDetails, 0);

        //write_to_child(yourTurn, gameStruct, playerStruct, carriageDetails);
        //write_to_child(gameOver, gameStruct->childInputArray[i], gameStruct, playerStruct);

        //need to ensure the number writen to child is not negative
        end_of_round_summary(playerStruct, carriageDetails, gameStruct);
        gameStruct->roundNumber--;
        //fprintf(stderr, "Round is now %d\n", gameStruct->roundNumber);
    }
    write_to_child(gameOver, gameStruct, playerStruct, carriageDetails, 0);

    for(i = 0; i < gameStruct->totalPlayers; i++) {
        waitpid(playerStruct[i].pid, &status, 0);
        if(status != 0) {
            fprintf(stderr, "Player %c ended with status %d\n", 
                    playerStruct[i].playerID, status);
        }
    }

    //fprintf(stderr, "PARENT: Main ending");
    //fprintf(stdout, "PARENT: Main ending");
    fflush(stdout);
    //end_of_round_summary(playerStruct, carriageDetails, gameStruct);
    return 0;
}

/*
This function reads the move selected by each player. It will then
carry out this move. Some moves occur instantly, while others
will result in a reply from the hub.
*/
void move_handler(PlayerStruct* playerStruct, GameStruct* gameStruct, 
        CarriageDetails* carriageDetails) {
    int i;
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        if(gameStruct->playerMoveArray[i][0] == '$') {
            if(loot_check(&playerStruct[i], carriageDetails) == 1) {
                continue;
            }
            if(playerStruct[i].carriageLevel == 1) {
                carriageDetails->topLevel[playerStruct[i].currentCarriage] -= 1;
            } else {
                carriageDetails->bottomLevel[playerStruct[i].currentCarriage] -= 1;
            }
            playerStruct[i].loot++;
        }
        if(gameStruct->playerMoveArray[i][0] == 'v') {
            playerStruct[i].carriageLevel = (playerStruct[i].carriageLevel + 1) % 2;
            write_to_child(vmove, gameStruct, playerStruct, carriageDetails, 0);
        }
        if(gameStruct->playerMoveArray[i][0] == 'h') {
            write_to_child(horrizontalMove, gameStruct, playerStruct, carriageDetails, i);
        }
        if(gameStruct->playerMoveArray[i][0] == 's') {
            write_to_child(shortTarget, gameStruct, playerStruct, carriageDetails, i);
        }
        if(gameStruct->playerMoveArray[i][0] == 'l') {
            printf("A LONG HAPPENED IN MOVE_HANDLER\n");
        }
    }
}

/*
Write to child is a large enumerated switch statement that controls the
majority of the Hub's writes to the child. 
*/
void write_to_child(HubMessage hubMessage, GameStruct* gameStruct, PlayerStruct* playerStruct,
        CarriageDetails* carriageDetails, int position) {
    int i, j;
    char move;
    switch(hubMessage) {
        case gameOver:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                write(gameStruct->childInputArray[i][WRITE], "game_over\n", GAMEOVER);
            }
            break;
        case roundStart:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                write(gameStruct->childInputArray[i][WRITE], "round\n", ROUND);
            }
            break;
        case yourTurn:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                write(gameStruct->childInputArray[i][WRITE], "yourTurn\n", YOURTURN);
                move = read_move_character(gameStruct, &(playerStruct[i]), 
                        gameStruct->childInputArray[i]);
                gameStruct->playerMoveArray[i][0] = move;
                fprintf(stderr, "PARENT: move is %c\n", move);
                for(int j = 0; j < gameStruct->totalPlayers; j++) {
                    char buffer[11];
                    memset(buffer, '\0', 11);
                    snprintf(buffer, 11, "ordered%c%c\n", playerStruct[i].playerID + 65, 
                            gameStruct->playerMoveArray[i][0]);
                    write(gameStruct->childInputArray[j][WRITE], buffer, strlen(buffer));
                }
            }
            break;
        case execute:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                write(gameStruct->childInputArray[i][WRITE], "execute\n", strlen("execute\n"));
            }
            move_handler(playerStruct, gameStruct, carriageDetails);
            break;
        case horrizontalMove:
            write(gameStruct->childInputArray[position][WRITE], "h?\n", 3);
            read_player_decision(gameStruct, &playerStruct[position], carriageDetails,
                    gameStruct->childInputArray[position]);
            break;
        case shortTarget:
            write(gameStruct->childInputArray[position][WRITE], "s?\n", 3);
            read_player_decision(gameStruct, &playerStruct[position], carriageDetails,
                    gameStruct->childInputArray[position]);
            break;
        case longTarget:
            //write(childInput[WRITE], "1?\n", 3);
            break;
        case hmove:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                if(gameStruct->playerMoveArray[i][0] == 'h') {
                    for(j = 0; j < gameStruct->totalPlayers; j++) {
                        char buffer[9];
                        memset(buffer, '\0', 9);
                        snprintf(buffer, 9, "hmove%c%c\n", playerStruct[i].playerID + 65,
                                gameStruct->playerMoveArray[i][1]);
                        write(gameStruct->childInputArray[j][WRITE], buffer, strlen(buffer));
                    }
                }
            }
            break;
        case vmove:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                if(gameStruct->playerMoveArray[i][0] == 'v') {
                    for(j = 0; j < gameStruct->totalPlayers; j++) {
                        char buffer[9];
                        memset(buffer, '\0', 9);
                        snprintf(buffer, 9, "vmove%c\n", playerStruct[i].playerID + 65);
                        write(gameStruct->childInputArray[j][WRITE], buffer, strlen(buffer));
                    }
                }
            }
            break;
        case longed:
            //do shit here

        case shorted:

           // char* shortedString = "short";
            break;
        case looted:
            for(i = 0; i < gameStruct->totalPlayers; i++) {
                if(gameStruct->playerMoveArray[i][0] == '$') {
                    for(j = 0; j < gameStruct->totalPlayers; j++) {
                        char buffer[9];
                        memset(buffer, '\0', 9);
                        snprintf(buffer, 9, "looted%c\n", playerStruct[i].playerID + 65);
                        write(gameStruct->childInputArray[j][WRITE], buffer, strlen(buffer));
                    }
                }
            }
            break;
        case driedOut:
            //char* driedOutString = "driedOut";
            //print to stdin
            break;
        default:
            fprintf(stderr, "Cant get here\n");
    }
}

/*
This function prints the end of round summary. Showing all players
ID's, hits and loot. Followed by the loot available at each
carriage.
*/    
void end_of_round_summary(PlayerStruct* playerStruct, 
        CarriageDetails* carriageDetails, GameStruct* gameStruct) {
    int i;
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        char playerID = playerStruct[i].playerID + 65;
        printf("PlayerID: %c i: %d\n", playerID, i);
        int currentCarriage = playerStruct[i].currentCarriage;
        int carriageLevel = playerStruct[i].carriageLevel;
        int loot = playerStruct[i].loot;
        int hits = playerStruct[i].hits;
        printf("%c(%d,%d): $=%d hits=%d\n", playerID, 
                currentCarriage, carriageLevel, loot, hits);
    }
    for(i = 0; i < gameStruct->carriages; i++) {
        printf("Carriage %d: $=%d : $=%d\n", i, 
                carriageDetails->bottomLevel[i], carriageDetails->topLevel[i]);
        fflush(stdout);
    }
}

/*
This function reads the players selected move. It stores this move and 
checks the move for any specific error.
*/
char read_move_character(GameStruct* gameStruct, PlayerStruct* playerStruct, 
        int* childInput) {
    char buffer[100], move;
    memset(buffer, '\0', 100);
    int readBytes = read(childInput[READ], buffer, sizeof(buffer) - 1);
    fprintf(stderr, "bytes %d \"%s\"\n", readBytes, buffer);
    if(readBytes != 0 && sscanf(buffer, "play%c", &move) == 1) {
        //fprintf(stderr, "Move is: %c\n", move);
        read_error(move, 0, gameStruct, playerStruct);
        return move;
    }
    return 0;
}

/*
Read player decision is a function that reads the players reply once
askedh?, l? or s?. It reads the players reply and adjust all relevant
structs accordingly.
*/
int read_player_decision(GameStruct* gameStruct, PlayerStruct* playerStruct, 
        CarriageDetails* carriageDetails, int* childInput) {
    char buffer[100], move;
    int playerID;
    memset(buffer, '\0', 100);
    //fprintf(stderr, "PARENT: In read_player_decision\n");
    read(childInput[READ], buffer, sizeof(buffer) - 1);
    //fprintf(stderr, "PARENT: read_player_decision read%s\n", buffer);
    if(sscanf(buffer, "sideways%c", &move) == 1) {
        read_error(move, 1, gameStruct, playerStruct);
        gameStruct->playerMoveArray[playerStruct->playerID][1] = move;
        if(move == '+') {
            playerStruct->currentCarriage++;
        } else {
            playerStruct->currentCarriage--;
        }
        return 1;
    }
    if(sscanf(buffer, "target.short%c", &move) == 1) {
        read_error(move, 2, gameStruct, playerStruct);
        gameStruct->playerMoveArray[playerStruct->playerID][1] = move;
        if(move != '-') {
            playerID = move - 65;
            playerStruct[playerID].loot--;
            if(playerStruct[playerID].carriageLevel == 1) {
                carriageDetails->topLevel[playerStruct[playerID].currentCarriage]++;
            } else {
                carriageDetails->bottomLevel[playerStruct[playerID].currentCarriage]++;
            }
            return 2;
        }
    }
    if(sscanf(buffer, "target.long%c", &move) == 1) {
        read_error(move, 2, gameStruct, playerStruct);
        gameStruct->playerMoveArray[playerStruct->playerID][1] = move;
        if(move != '-') {
            return 3;
        }
    }
    return 0;
}

/*
This function checks if the child (client) has replied with 
appropriate move commands. If it has not it will prent to 
standard error and exit with a specified status.
*/
void read_error(char move, int errorCase, GameStruct* gameStruct, 
        PlayerStruct* playerStruct) {
    int moveInteger = move + '0';
    switch(errorCase) {
        case 0:
            if(move != 'v' && move != 'l' && move != 'h' && move != 's' 
                    && move != '$') {
                fprintf(stderr, "Protocol error by client\n");
                exit(6);
            }
            break;
        case 1:
            if(move != '+' && move != '-') {
                fprintf(stderr, "Protocol error by client\n");
                exit(5);
            }
            break;
        case 2:
            if(moveInteger < 0 || moveInteger > gameStruct->totalPlayers) {
                fprintf(stderr, "Protocol error by client\n");
                exit(5);
            }
            break;
        default:
            break;
    }
}

/*
This function initialises the carriage details for the hub.
It also allocates loot to each specific carriage, following
the games specific algorithim.
*/
CarriageDetails* loot_distributor(GameStruct* gameStruct) {
    //initialising Carriage Details
    CarriageDetails* carriageDetails = malloc(sizeof(CarriageDetails));
    carriageDetails->topLevel = (int*)malloc(sizeof(int) * gameStruct->totalPlayers);
    carriageDetails->bottomLevel = (int*)malloc(sizeof(int) * gameStruct->totalPlayers);

    int totalLoot = ((gameStruct->seed % 4) + 1) * gameStruct->carriages;
    int lootCarriage = ceil(gameStruct->carriages / 2);
    //Y = 0 here so bottom level
    int carriageLevel = 0;
    carriageDetails->bottomLevel[lootCarriage]++;
    totalLoot--;
    while(totalLoot != 0) {
        lootCarriage = (lootCarriage + (gameStruct->seed % 101)) % gameStruct->carriages;
        if((carriageLevel + (gameStruct->seed % 2)) % 2 == 0) {
            carriageDetails->bottomLevel[lootCarriage]++;
        } else {
            carriageDetails->topLevel[lootCarriage]++;
        }
        totalLoot--;
    }
    int i;
    for(i = 0; i < gameStruct->carriages; i++) {
        printf("%d", carriageDetails->topLevel[i]);
    }
    printf("\n");
    for(i = 0; i < gameStruct->carriages; i++) {
        printf("%d", carriageDetails->bottomLevel[i]);
    }
    printf("\n");
    return carriageDetails;
}

/*
Function to initalise the game structure, making use of variables globally easier
*/
GameStruct* game_struct_init(char* seed, char* carriages, char* totalPlayers) {
    int i;
    char** playerMoveArray;
    int** childInputArray;
    GameStruct* gameStruct = malloc(sizeof(GameStruct));
    gameStruct->seed = atoi(seed);
    gameStruct->carriages = atoi(carriages);
    gameStruct->totalPlayers = atoi(totalPlayers);
    gameStruct->playerMoveArray = (char**)malloc(sizeof(char*) * gameStruct->totalPlayers);
    playerMoveArray = gameStruct->playerMoveArray;
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        playerMoveArray[i] = (char*)malloc(sizeof(char) * gameStruct->totalPlayers);
    }
    gameStruct->childInputArray = (int**)malloc(sizeof(int*) * gameStruct->totalPlayers);
    childInputArray = gameStruct->childInputArray;
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        childInputArray[i] = (int*)malloc(sizeof(int) * gameStruct->totalPlayers);
    }
    return gameStruct;
}

/*
This functinon initialises the player struct and sets all of the players to
their allocated positions.
*/
PlayerStruct* player_init(GameStruct* gameStruct) {
    PlayerStruct* playerStruct 
            = (PlayerStruct*)malloc(sizeof(PlayerStruct) * gameStruct->totalPlayers);
    int i;
    for(i = 0; i < gameStruct->totalPlayers; i++) {
        playerStruct[i].carriageLevel = 0;
        playerStruct[i].currentCarriage = i % gameStruct->carriages;
    }
    return playerStruct;
}

/*
This function is the error handler for the hub, it checks for issues and
if found prints the appropriate statement to stderr and exits with a
specified status.
*/
void error_controller(int argc, char** argv) {
    int i;
    for(i = 0; i < argc; i++) {
        printf("Argv on %d : %s\n", i, argv[i]);
    }
    if(argc < 5) {
        fprintf(stderr, "Usage: hub seed width player player [player ...}\n");
        exit(1);
    }
    for(i = 0; i < strlen(argv[1]); i++) {
        if(isdigit(argv[1][i]) == 0) {
            fprintf(stderr, "Bad argument\n");
            exit(2);
        }
    }
    for(i = 0; i < strlen(argv[2]); i++) {
        if(isdigit(argv[2][i]) == 0) {
            fprintf(stderr, "Bad argument\n");
            exit(2);
        }
    }
    if(atoi(argv[2]) < 3) {
        fprintf(stderr, "Bad argument\n");
        exit(2);
    }
    for(int i = 3; i < argc; i++) {
        if(!strcmp(argv[i], "./bandit") == 0 && 
                (!strcmp(argv[i], "./acrophobe") == 0) &&
                (!strcmp(argv[i], "./spoiler") == 0)) {
            fprintf(stderr, "Bad start\n");
            exit(3);
        }
    }
}

/*
This function checks if there is loot available at the current player position.
*/
int loot_check(PlayerStruct* playerStruct, CarriageDetails* carriageDetails) {
    if(playerStruct->carriageLevel == 1) {
        if(carriageDetails->topLevel[playerStruct->currentCarriage] == 0) {
            return 1;
        }
    } else {
        if(carriageDetails->bottomLevel[playerStruct->currentCarriage] == 0) {
            return 1;
        }
    }
    return 0;
}


void signal_handler(int signal) {
    int i;
    if(signal == SIGINT) {
        for(i = 0; i < gameStruct->totalPlayers; i++) {
            kill(playerStruct[i].playerID, SIGINT);
        }
        fprintf(stderr, "SIGINT caught\n");
        exit(9);
    }
}