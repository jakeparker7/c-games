#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <ctype.h>
#include "common.h"

typedef struct {
    char playerID;
    int loot;
    int currentCarriage;
    int carriageLevel;
    char move;
    char justShortOrLong;
} SpoilerStruct;

void spoiler_decider(char* readString, 
        CarriageDetails* carriageDetails, SpoilerStruct* spoilerStruct,
        GameStruct* gameStruct, PlayerPositions* playerPositions);

int main(int argc, char** argv) {
    error_controller(argc, argv);
    SpoilerStruct* spoilerStruct = malloc(sizeof(SpoilerStruct));
    GameStruct* gameStruct = player_init(argc, argv);
    CarriageDetails* carriageDetails = loot_distributor(gameStruct);
    PlayerPositions* playerPositions = player_position_init(gameStruct);
    int playerInteger = atoi(argv[2]);
    banditStruct->playerID = playerInteger + 65;
    banditStruct->currentCarriage = playerInteger % gameStruct->totalCarriages;
    banditStruct->carriageLevel = 0;
    char* readString;
    while(1) {
        readString = read_standard_in();
    }
}



void spoiler_decider(char* readString, 
        CarriageDetails* carriageDetails, SpoilerStruct* spoilerStruct,
        GameStruct* gameStruct, PlayerPositions* playerPositions) {
    char* yourTurn = "yourturn";
    char* horrizontalMessage = "h?";
    char* shortMessage = "s?";
    char* longMessage = "l?";
    char* execute = "execute";
    char playerID, move, targetID;
     if(strcmp(readString, yourTurn) == 0) {
        move = move_controller(carriageDetails, gameStruct, spoilerStruct,
                playerPositions);
        spoilerStruct->move = move;
        printf("play%c\n", move);
        fflush(stdout);
    }
    if(strcmp(readString, execute) == 0) {
        return;
    }
    if(sscanf(readString, "looted%c", &playerID) == 1) {
        if(playerID != spoilerStruct->playerID) {
            looted(playerPositions, gameStruct, carriageDetails, playerID);
            return;
        }
        if(playerID == spoilerStruct->playerID) {
            //move_loot(carriageDetails, banditStruct, playerPositions);
        }
    }
    if(sscanf(readString, "vmove%c", &playerID) == 1) {
        if(playerID != spoilerStruct->playerID) {
            vmove_player(playerPositions, gameStruct, playerID);
            return;
        }
        if(playerID == spoilerStruct->playerID) {
            //move_vertical(banditStruct, playerPositions, gameStruct);
        }
    }
    if(strcmp(readString, horrizontalMessage) == 0) {
        //move_horrizontal(carriageDetails, banditStruct,
        //        gameStruct, playerPositions);
    }
    if(strcmp(readString, shortMessage) == 0) {
        //move_short(playerPositions, banditStruct, carriageDetails);
    }
    if(strcmp(readString, longMessage) == 0) {
        //move_long
    }
    if(other_players_move_handler(readString, carriageDetails, gameStruct,
            playerPositions, spoilerStruct->playerID) == 1) {
        return;
    }
    fprintf(stderr, "Communication Error\n");
    exit(0);
}

char move_controller(CarriageDetails* carriageDetails, GameStruct* gameStruct, 
        SpoilerStruct* spoilerStruct, PlayerPositions* playerPositions) {
    int targetCarriaage
    if(spoilerStruct->carriageLevel == 0) {
        if(strlen(playerPositions->bottomLevel[spoilerStruct->currentCarriage]) > 1
            && spoilerStruct->justShortOrLong == 'n') {
            spoilerStruct->justShortOrLong = 'y';
            return 's';
        }
    if(spoilerStruct->carriageLevel == 1) {
        if(strlen(playerPositions->topLevel[spoilerStruct->currentCarriage]) > 1
                && spoilerStruct->justShortOrLong == 'n') {
            spoilerStruct->justShortOrLong = 'y';
            return 's';
        }
    }
    //LONG CHECK && SPOILERSTRUCT->JUSTSHORTORLONG == 'n'
    if((carriageLevel + 1) % 2 == 0 {
        if(strlen(playerPositions->bottomLevel[spoilerStruct->currentCarriage]) > 0) {
            return 'v';
        }
    } else {
        if(strlen(playerPositions->topLevel[spoilerStruct->currentCarriage]) > 0) {
            return 'v';
        }
    }


 }